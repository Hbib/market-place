import React, { useEffect, useState } from "react"

import PropTypes from "prop-types"
import { AvForm, AvField } from "availity-reactstrap-validation"
import { useHistory } from "react-router-dom"
import { Col, Container, Row, Collapse, Card, CardBody } from "reactstrap"
import { useParams } from "react-router-dom"
import { del, get, post, put } from "../../helpers/api_helper"
import { useSelector } from "react-redux"
const NewUser = ({ location }) => {
  const { id } = useParams()
  useEffect(() => {
    if (id) {
      document.title = "Editer utilisateur"
    } else {
      document.title = "Nouveau utilisateur"
    }
  }, [])
  let history = useHistory()
  const { user } = useSelector(state => {
    return state.user
  })

  const route = location.pathname
  const [role, setRole] = useState([])
  const [loadedData, setloadedData] = useState(null)
  const [cin, setCin] = useState("")
  const token = localStorage.getItem("authToken")

  function getUser() {
    get(`/api/users/${id}`)
      .then(res => {
        console.log(res)
        setloadedData(res.data)
        setRole(res.data.roles)
      })
      .catch(err => console.log(err))
  }
  useEffect(() => {
    if (id) {
      getUser()
    }
  }, [])
  function submit(e, v) {
    v.roles = role
    v.company = user.company
    if (!id) {
      post("/api/users/register", v, {
        headers: { Authorization: `Bearer ${token}` },
      })
        .then(res => {
          console.log(res.data)
          history.push("/users")
        })
        .catch(err => {
          console.log(err.response)
        })
    } else {
      put("/api/users/update/" + id, v, {
        headers: { Authorization: `Bearer ${token}` },
      })
        .then(res => {
          console.log(res.data)
          history.push("/users")
        })
        .catch(err => {
          console.log(err.response)
        })
    }
  }
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid={true}>
          <Card>
            <CardBody>
              <AvForm onValidSubmit={submit}>
                <Row>
                  <Col lg={4}>
                    <div className="mb-3">
                      <label>
                        Nom d'utilisateur{" "}
                        <small className="text-danger">
                          Champs obligatoire
                        </small>
                      </label>
                      <AvField
                        required
                        name="username"
                        type="text"
                        value={loadedData && loadedData.username}
                        className="form-control"
                      />
                    </div>{" "}
                  </Col>
                </Row>
                <Row>
                  <Col lg={4}>
                    <div className="mb-3">
                      <label>Nom et prenom</label>
                      <AvField
                        name="fullName"
                        value={loadedData && loadedData.fullName}
                        type="text"
                        className="form-control"
                      />
                    </div>{" "}
                  </Col>
                </Row>
                {/* <Row>
                  <Col lg={4}>
                    <div className="mb-3">
                      <label>Prénom</label>
                      <AvField
                        name="firstname"
                        value={loadedData && loadedData.firstname}
                        type="text"
                        className="form-control"
                      />
                    </div>{" "}
                  </Col>
                </Row> */}
                <Row>
                  <Col lg={4}>
                    <div className="mb-3">
                      <label>CIN</label>
                      <AvField
                        name="cin"
                        value={loadedData && loadedData.cin}
                        onInput={e => setCin(e.currentTarget.value)}
                        type="number"
                        invalid={cin && cin.length != 8}
                        className="form-control"
                      />
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col lg={4}>
                    <div className="mb-3">
                      <label>
                        E-mail{" "}
                        <small className="text-danger">
                          Champs obligatoire
                        </small>
                      </label>
                      <AvField
                        required
                        name="email"
                        value={loadedData && loadedData.email}
                        type="email"
                        className="form-control"
                      />
                    </div>{" "}
                  </Col>
                </Row>

                <hr />

                <h2>Roles</h2>
                {/* <div className="p-2 d-flex"> */}
                <label>Super Admin</label>
                <div className="form-check form-switch form-switch-md rounded mx-3">
                  <input
                    type="checkbox"
                    className="form-check-input"
                    id="superAdmin"
                    checked={role.includes("ROLE_SUPER_ADMIN")}
                    onClick={() => {
                      setRole(roles => [...roles, "ROLE_SUPER_ADMIN"])
                    }}
                  />
                </div>
                {/* <label>Racine</label>
                  <div className="form-check form-switch form-switch-md rounded mx-3">
                    <input
                      type="checkbox"
                      className="form-check-input"
                      id="racine"
                      name="racine"
                      checked={role.includes("ROLE_ROOT")}
                      onClick={
                        () => {
                          setRole(roles => [...roles, "ROLE_ROOT"])
                        }
                        // setRacine(true),
                        // setSuperAdmin(!racine),
                        // setAdvancedTrador(!racine),
                        // setTrador(!racine),
                        // setTradorBO(!racine),
                        // setSTBAdmin(!racine)
                      }
                      // defaultChecked={racine}
                    />
                  </div>
                </div>

                <Row>
                  <Col lg={6}>
                    <Row>
                      <Col lg={12}>
                        <div
                          className=" rounded-3 p-3 cursor-pointer d-flex mb-3"
                          style={{ background: "#05386b" }}
                        >
                          <span
                            className="fs-5 col-6 text-white"
                            onClick={() => setCollapse0(!collapse0)}
                          >
                            STB ADMIN{" "}
                            <i
                              className={
                                collapse0
                                  ? "fas fa-chevron-up mx-3"
                                  : "fas fa-chevron-down mx-3"
                              }
                            />
                          </span>

                          <div
                            className="form-check form-switch form-switch-lg rounded col-6"
                            // style={{ border: "1px solid #556ee6" }}
                          >
                            <input
                              type="checkbox"
                              className="form-check-input"
                              checked={role.includes("ROLE_STB_ADMIN")}
                              onClick={() => {
                                setRole(roles => [...roles, "ROLE_STB_ADMIN"])
                              }}
                              id="Autorisé"
                            />
                          </div>
                        </div>
                        <Collapse isOpen={collapse0} className="p-2">
                          <Row className="p-2">
                            <Col lg={9}>
                              <label>Creation course</label>
                            </Col>
                          </Row>

                          <Row className="p-2">
                            <hr />
                            <Col lg={9}>
                              <label>Générateur ticket</label>
                            </Col>
                          </Row>
                          <Row className="p-2">
                            <hr />
                            <Col lg={9}>
                              <label>Creation ordre</label>
                            </Col>
                          </Row>
                        </Collapse>
                      </Col>
                      <Col lg={12}>
                        <div
                          className=" rounded-3 p-3 cursor-pointer d-flex mb-3"
                          style={{ background: "#05386b" }}
                        >
                          <span
                            className="fs-5 col-6 text-white"
                            onClick={() => setCollapse1(!collapse1)}
                          >
                            TRADER{" "}
                            <i
                              className={
                                collapse1
                                  ? "fas fa-chevron-up mx-3"
                                  : "fas fa-chevron-down mx-3"
                              }
                            />
                          </span>

                          <div
                            className="form-check form-switch form-switch-lg rounded col-6"
                            // style={{ border: "1px solid #556ee6" }}
                          >
                            <input
                              type="checkbox"
                              className="form-check-input"
                              checked={role.includes("ROLE_TRADER")}
                              onClick={() => {
                                setRole(roles => [...roles, "ROLE_TRADER"])
                              }}
                              id="Autorisé"
                            />
                          </div>
                        </div>
                        <Collapse isOpen={collapse1} className="p-2">
                          <Row className="p-2">
                            <Col lg={9}>
                              <label>Creation ordre</label>
                            </Col>
                          </Row>

                          <Row className="p-2">
                            <hr />
                            <Col lg={9}>
                              <label>Confirmation ordre</label>
                            </Col>
                            <Col lg={3}></Col>
                          </Row>
                        </Collapse>
                      </Col>
                    </Row>
                  </Col>

                  <Col lg={6}>
                    <Row>
                      <Col lg={12}>
                        <div
                          className=" rounded-3 p-3 cursor-pointer mb-3 d-flex"
                          style={{ background: "#05386b" }}
                        >
                          <span
                            className="fs-5 text-white col-6"
                            onClick={() => setCollapse4(!collapse4)}
                          >
                            Advanced TRADER
                            <i
                              className={
                                collapse4
                                  ? "fas fa-chevron-up mx-3"
                                  : "fas fa-chevron-down mx-3"
                              }
                            />
                          </span>

                          <div
                            className="form-check form-switch form-switch-lg rounded col-6"
                            // style={{ border: "1px solid #556ee6" }}
                          >
                            <input
                              type="checkbox"
                              className="form-check-input"
                              checked={role.includes("ROLE_ADVANCED_TRADER")}
                              onClick={() => {
                                setRole(roles => [
                                  ...roles,
                                  "ROLE_ADVANCED_TRADER",
                                ])
                              }}
                              id="Autorisé"
                            />
                          </div>
                        </div>
                        <Collapse isOpen={collapse4}>
                          <Row className="p-2">
                            <Col lg={9}>
                              <label>TRADER</label>
                            </Col>
                          </Row>
                          <Row className="p-2">
                            <hr />
                            <Col lg={9}>
                              <label>Création course </label>
                            </Col>
                          </Row>
                        </Collapse>
                      </Col>
                      <Col lg={12}>
                        <div
                          className=" rounded-3 p-3 cursor-pointer mb-3 d-flex"
                          style={{ background: "#05386b" }}
                        >
                          <span
                            className="fs-5 text-white col-6"
                            onClick={() => setCollapse2(!collapse2)}
                          >
                            TRADER BO
                            <i
                              className={
                                collapse2
                                  ? "fas fa-chevron-up mx-3"
                                  : "fas fa-chevron-down mx-3"
                              }
                            />
                          </span>

                          <div
                            className="form-check form-switch form-switch-lg rounded col-6"
                            // style={{ border: "1px solid #556ee6" }}
                          >
                            <input
                              type="checkbox"
                              className="form-check-input"
                              checked={role.includes("ROLE_TRADER_BO")}
                              onClick={() => {
                                setRole(roles => [...roles, "ROLE_TRADER_BO"])
                              }}
                              id="Autorisé"
                            />
                          </div>
                        </div>
                        <Collapse isOpen={collapse2}>
                          <Row className="p-2">
                            <Col lg={9}>
                              <label>ADVENCED TRADER </label>
                            </Col>
                          </Row>
                          <Row className="p-2">
                            <hr />
                            <Col lg={9}>
                              <label>Générateur ticket </label>
                            </Col>
                          </Row>
                        </Collapse>
                      </Col>
                    </Row>
                  </Col>
                </Row> */}
                <button type="submit" className="btn btn-primary mt-3">
                  Valider
                </button>
              </AvForm>
            </CardBody>
          </Card>
        </Container>
      </div>
    </React.Fragment>
  )
}

NewUser.propTypes = {
  location: PropTypes.object,
}

export default NewUser
