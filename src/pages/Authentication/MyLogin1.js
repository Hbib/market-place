import PropTypes from "prop-types"
import MetaTags from "react-meta-tags"
import React, { useEffect, useState } from "react"
import $ from "jquery"
import loginBg1 from "assets/images/44.jpg"

import { Row, Col, CardBody, Card, Alert, Container } from "reactstrap"

//redux
import { useSelector, useDispatch } from "react-redux"

import { withRouter, Link, useHistory } from "react-router-dom"

// availity-reactstrap-validation
import { AvForm, AvField } from "availity-reactstrap-validation"

// actions
import { loginUser, apiError, socialLogin, saveUser } from "../../store/actions"

// import images
import hiveLogo from "assets/images/exsys_logo.png"
import { post } from "helpers/api_helper"

const Login = props => {
  const dispatch = useDispatch()
  const history = useHistory()

  const [error, setError] = useState("")
  const [loading, setLoading] = useState(false)

  // handleValidSubmit
  const handleValidSubmit = (event, values) => {
    setLoading(true)
    post("/api/login_check", values, {
      withCredentials: true,
    })
      .then(res => {
        dispatch(saveUser(res.data))
        localStorage.setItem("authToken", res.data.token)
        localStorage.setItem("stayOnline", JSON.stringify(values.stayOnline))
        console.log(res.data)
        history.push("/home")
        setLoading(false)
      })
      .catch(err => {
        console.log(err)
        if (err.response.status == 401) {
          setError("Nom d'utilisateur ou mot de passe incorrecte")
        } else {
          setError("Erreur serveur")
        }
        setLoading(false)
      })

    localStorage.setItem("userEmail", values.email)
  }

  return (
    <React.Fragment>
      <MetaTags>
        <title>Login | Exsys</title>
      </MetaTags>
      <div
        className="p-5 bg-image"
        style={{
          backgroundImage: `url(${loginBg1})`,
          //   background:
          //     "linear-gradient(180deg, rgba(10,20,110,1) 0%, rgba(9,9,182,1) 48%, rgba(0,87,199,0.9023984593837535) 100%)",
          width: "100%",
          backgroundSize: "cover",
          position: "absolute",
          height: "35vh",
        }}
      ></div>

      <div
        className="d-flex justify-content-center"
        style={{ alignItems: "center", height: "100vh" }}
      >
        <div
          className="p-5 shadow-5"
          style={{
            borderRadius: 10,
            boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
            background: "rgb(255 255 255 / 72%)",
            backdropFilter: "blur(5px)",
            width: 500,
          }}
        >
          <div className="text-primary text-center mb-5">
            <img
              className="img-fluid"
              src={hiveLogo}
              style={{
                width: "30%",
              }}
            />
          </div>
          <AvForm
            className="form-horizontal mt-3"
            onValidSubmit={handleValidSubmit}
          >
            {error ? <Alert color="danger">{error}</Alert> : null}

            <div className="mb-3">
              <AvField
                errorMessage="Invalide"
                name="username"
                label="Nom utilisateur"
                className="form-control"
                placeholder="Nom utilisateur"
                type="text"
                required
              />
            </div>

            <div className="mb-3">
              <div className="d-flex align-items-start">
                <div className="flex-grow-1">
                  <label className="form-label">Mot de passe</label>
                </div>
                {/* <div className="flex-shrink-0">
                      <div>
                        <Link to="/forgot-password" className="text-muted">
                          <i className="mdi mdi-lock me-1" />
                          Mot de passe oublié?
                        </Link>
                      </div>
                    </div> */}
              </div>
              <AvField
                errorMessage="Invalide"
                name="password"
                type="password"
                required
                placeholder="Mot de passe"
              />
            </div>

            <AvField
              errorMessage="Invalide"
              type="checkbox"
              className="form-check-input"
              id="stayOnline"
              name="stayOnline"
              checked={true}
              defaultChecked
              label="Rester connecté"
            />

            <div className="mt-3 d-grid">
              <button
                disabled={loading}
                className="btn btn-primary btn-block"
                type="submit"
              >
                {loading ? (
                  <div
                    className="spinner-border text-light"
                    style={{ width: "1.5rem", height: "1.5rem" }}
                    role="status"
                  >
                    <span className="sr-only">Loading...</span>
                  </div>
                ) : (
                  <span>Log In</span>
                )}
              </button>
            </div>

            {/* <div className="mt-4 text-center">
                  <Link to="/forgot-password" className="text-muted">
                    <i className="mdi mdi-lock me-1" />
                    Mot de passe oublié?
                  </Link>
                </div> */}
          </AvForm>
        </div>
      </div>
    </React.Fragment>
  )
}

export default withRouter(Login)

Login.propTypes = {
  history: PropTypes.object,
}
