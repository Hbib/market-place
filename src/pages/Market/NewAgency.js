import React, { useEffect, useState } from "react"

import SweetAlert from "react-bootstrap-sweetalert"
import {
  Card,
  Col,
  Container,
  Row,
  Label,
  Form,
  Input,
  CardBody,
} from "reactstrap"
import { useSelector, useDispatch } from "react-redux"
import { useHistory, useParams } from "react-router-dom"
import { AvField, AvForm } from "availity-reactstrap-validation"
import { get, post, put } from "helpers/api_helper"
import { ToastContainer, toast } from "react-toastify"
const NewAgency = () => {
  const route = location.pathname
  const { id } = useParams()
  const [loadedData, setLoadedData] = useState(null)
  const [regions, setregions] = useState([])

  useEffect(() => {
    getRegions()
    if (id) {
      getData(id)
      document.title = "Edite agence"
    } else {
      document.title = "Nouvelle agence"
    }
  }, [])
  let history = useHistory()

  async function getRegions() {
    const { data } = await get("/api/zone")
    setregions(data)
  }

  async function handleSubmit(e, v) {
    try {
      if (!id) {
        v.company = user.company
        await post("/api/agence/add", v)
      } else {
        await put(`/api/agence/update/${id}`, v)
      }

      history.push("/agencies")
    } catch (e) {
      toast["error"](
        "Veuillez patienter svp, une erreur système s'est produite"
      )
      console.log(e.response)
    }
  }
  const { user } = useSelector(state => {
    return state.user
  })
  async function getData(id) {
    try {
      const res = await get(`/api/agence/${id}`)

      setLoadedData(res.data)
    } catch (e) {
      console.log(e)
    }
  }
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid={true}>
          <Card>
            <CardBody>
              <AvForm onValidSubmit={handleSubmit}>
                <Row>
                  <Col md={4}>
                    <div className="mb-3">
                      <AvField
                        type="text"
                        name="name"
                        label="Nom"
                        value={loadedData && loadedData.name}
                        required
                      />
                    </div>
                  </Col>
                </Row>{" "}
                <Row>
                  <Col md={4}>
                    <div className="mb-3">
                      <AvField
                        type="text"
                        name="codeBCT"
                        label="Code BCT"
                        value={loadedData && loadedData.codeBCT}
                        required
                      />
                    </div>
                  </Col>
                </Row>{" "}
                <Row>
                  <Col md={4}>
                    <div className="mb-3">
                      <AvField
                        type="number"
                        name="phone"
                        label="Téléphone"
                        value={loadedData && loadedData.phone}
                        required
                      />
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col md={4}>
                    <div className="mb-3">
                      <AvField
                        type="text"
                        name="address"
                        label="Adresse"
                        value={loadedData && loadedData.address}
                        required
                      />
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col md={4}>
                    <div className="mb-3">
                      <AvField
                        type="text"
                        name="city"
                        label="Ville"
                        value={loadedData && loadedData.city}
                        required
                      ></AvField>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col md={4}>
                    <div className="mb-3">
                      <AvField
                        type="select"
                        name="zone"
                        label="Région"
                        className="form-select"
                        value={loadedData && loadedData.zone}
                        required
                      >
                        {regions.map(e => (
                          <option key={e.id} value={e.id}>
                            {e.name}
                          </option>
                        ))}
                      </AvField>
                    </div>
                  </Col>
                </Row>
                <Row>
                  <Col md={4}>
                    <div className="mb-3">
                      <AvField
                        type="text"
                        name="postCode"
                        label="Code postal"
                        value={loadedData && loadedData.postCode}
                        required
                      />
                    </div>
                  </Col>
                </Row>
                <button type="submit" className="btn btn-primary mt-3">
                  Valider
                </button>
              </AvForm>
            </CardBody>
          </Card>
        </Container>
      </div>

      <ToastContainer autoClose={5000} />
    </React.Fragment>
  )
}

export default NewAgency
