import { call, put, fork, takeLatest, takeEvery } from "redux-saga/effects"

// Login Redux States
import { SAVE_NOTIF } from "./actionTypes"
import { saveNotif } from "./actions"

function* save({ payload: notif }) {
  yield put(saveNotif(notif))
}

export function* watchNotif() {
  yield takeEvery(SAVE_NOTIF, save)
}

function* notifSaga() {
  yield all([fork(watchNotif)])
}

export default notifSaga
