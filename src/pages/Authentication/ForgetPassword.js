import PropTypes from "prop-types"
import MetaTags from "react-meta-tags"
import React, { useState } from "react"
import { Row, Col, Alert, Card, CardBody, Container } from "reactstrap"

//redux
import { useSelector, useDispatch } from "react-redux"

import { withRouter, Link } from "react-router-dom"

// availity-reactstrap-validation
import { AvForm, AvField } from "availity-reactstrap-validation"

// action
import { userForgetPassword } from "../../store/actions"

// import images

import forgetPassword from "assets/images/forgotPassword.jpg"
const ForgetPasswordPage = props => {
  const dispatch = useDispatch()

  const { forgetError, forgetSuccessMsg } = useSelector(state => ({
    forgetError: state.ForgetPassword.forgetError,
    forgetSuccessMsg: state.ForgetPassword.forgetSuccessMsg,
  }))

  function handleValidSubmit(event, values) {
    dispatch(userForgetPassword(values, props.history))
  }

  return (
    <React.Fragment>
      <MetaTags>
        <title>
          Forget Password | Skote - React Admin & Dashboard Template
        </title>
      </MetaTags>

      <div style={{ backgroundColor: "white", height: "100vh" }}>
        <Container>
          <Row>
            <Col>
              <Card className="overflow-hidden bg-white">
                <div className="text-center">
                  <img src={forgetPassword} alt="" className="img-fluid w-50" />
                </div>
                <CardBody className="pt-0">
                  <div className="p-2">
                    {forgetError && forgetError ? (
                      <Alert color="danger" style={{ marginTop: "13px" }}>
                        {forgetError}
                      </Alert>
                    ) : null}
                    {forgetSuccessMsg ? (
                      <Alert color="success" style={{ marginTop: "13px" }}>
                        {forgetSuccessMsg}
                      </Alert>
                    ) : null}

                    <AvForm
                      className="form-horizontal w-50 mx-auto"
                      onValidSubmit={(e, v) => handleValidSubmit(e, v)}
                    >
                      <div className="mb-3">
                        <AvField
                          name="email"
                          label="Email"
                          className="form-control"
                          placeholder="Saisissez l'émail principal"
                          type="email"
                          required
                        />
                      </div>
                      <div className="mb-3">
                        <AvField
                          name="userName"
                          label="Nom utilisateur"
                          className="form-control"
                          placeholder="Saisissez votre nom utilisateur"
                          type="text"
                          required
                        />
                      </div>
                      <Row className="mb-3">
                        <Col className="text-end">
                          <Link to="/mail-sent" className="text-muted">
                            <button
                              className="btn btn-primary w-md "
                              type="submit"
                            >
                              Reset
                            </button>
                          </Link>
                        </Col>
                      </Row>
                    </AvForm>
                  </div>
                  {/* {confirm_alert ? (
                    <SweetAlert
                      title="Etes vous sur?"
                      warning
                      showCancel
                      confirmButtonText="Yes, delete it!"
                      confirmBtnBsStyle="success"
                      cancelBtnBsStyle="danger"
                      onConfirm={() => {
                        setconfirm_alert(false)
                        setsuccess_dlg(true)
                        setdynamic_title(" Mot de passe réinitialisé")
                        setdynamic_description("")
                      }}
                      onCancel={() => setconfirm_alert(false)}
                    >
                      Vous ne pouvez plus revenir en arrière!
                    </SweetAlert>
                  ) : null}
                  {success_dlg ? (
                    <SweetAlert
                      success
                      title={dynamic_title}
                      onConfirm={() => {
                        setsuccess_dlg(false)
                      }}
                    >
                      {dynamic_description}
                    </SweetAlert>
                  ) : null}

                  {error_dlg ? (
                    <SweetAlert
                      error
                      title={dynamic_title}
                      onConfirm={() => {
                        seterror_dlg(false)
                      }}
                    >
                      {dynamic_description}
                    </SweetAlert>
                  ) : null} */}
                </CardBody>
              </Card>
              <div className=" text-center">
                <h4>
                  Go back to{" "}
                  <Link to="login" className="font-weight-medium text-primary">
                    Login
                  </Link>{" "}
                </h4>
                <p className="mt-4">
                  © {new Date().getFullYear()} Hive +. Crafted with{" "}
                  <i className="mdi mdi-heart text-danger" /> by INSGHT PLUS
                </p>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}

ForgetPasswordPage.propTypes = {
  history: PropTypes.object,
}

export default withRouter(ForgetPasswordPage)
