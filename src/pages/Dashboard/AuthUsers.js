import React, { useEffect, useState } from "react"

import PropTypes from "prop-types"
import { Link } from "react-router-dom"

import {
  Container,
  Button,
  Table,
  Modal,
  ModalBody,
  ModalFooter,
} from "reactstrap"

const AuthUsers = ({ location }) => {
  // const route = location.pathname

  useEffect(() => {
    document.title = "Autorisation"
  })
  const [rows, setRows] = useState([
    { username: "Hbib Bekir", limited: false, auth: false },
  ])
  const [modal, setModal] = useState(false)
  const route = location.pathname
  useEffect(() => {
    document.onkeydown = function (evt) {
      evt = evt || window.event
      var isEscape = false
      if ("key" in evt) {
        isEscape = evt.key === "Escape" || evt.key === "Esc"
      } else {
        isEscape = evt.keyCode === 27
      }
      if (isEscape) {
        setRows(rows => {
          let newRows = rows.map(ele => ele)
          newRows[modalId].modal = false
          return newRows
        })
        setShow({ isOpen: false })
      }
    }
  }, [])
  return (
    <React.Fragment>
      <div className="page-content">
        <Modal
          contentClassName="card-bg-gray"
          isOpen={modal}
          size="md"
          id="modal-auth"
          centered={true}
        >
          <div className="d-flex justify-content-between mb-3">
            <h4 className="mt-2">Création autorisation</h4>
            <i
              onClick={() => {
                setRows(rows => {
                  let newRows = rows.map(ele => ele)
                  newRows[modalId].modal = false
                  return newRows
                })
                setShow({ isOpen: false })
              }}
              className="btn pt-0 pb-0 bx bx-x-circle fs-2"
            />
          </div>
          <ModalBody>
            <div className="d-flex">
              <span className="d-flex">Montant supérieur à</span>
              <input min={0} type="number" className="form-control" />
            </div>
          </ModalBody>
          <ModalFooter>
            <Button onClick={() => setModal(false)} color="primary">
              Enregistrer
            </Button>{" "}
            <Button onClick={() => setModal(false)}>Annuler</Button>
          </ModalFooter>
        </Modal>
        <Container fluid={true}>
          <Link to="/auth/new" className="btn btn-primary mb-4">
            Nouveau
          </Link>
          <div className="table-responsive">
            <Table className="table table-bordered mb-0">
              <thead>
                <tr>
                  <th>Libélé</th>
                  <th>Régle</th>
                  <th>Action </th>
                </tr>
              </thead>
              <tbody>
                {rows.map((e, i) => (
                  <tr key={e.username}>
                    <td>Régle ticket</td>
                    <td>"inférieur 5000"</td>
                    <td>
                      <div className="d-flex">
                        <button className="btn btn-sm btn-primary mx-1">
                          edit
                        </button>
                        <button className="btn btn-sm btn-danger mx-1">
                          Rejeter
                        </button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        </Container>
      </div>
    </React.Fragment>
  )
}

AuthUsers.propTypes = {
  location: PropTypes.object,
}

export default AuthUsers
