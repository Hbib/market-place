import React, { useEffect, useState } from "react"

import PropTypes from "prop-types"
import SweetAlert from "react-bootstrap-sweetalert"

import { Card, Col, Container, Row } from "reactstrap"

import { useHistory } from "react-router-dom"
const NewAuthregle = ({ location }) => {
  let history = useHistory()
  const route = location.pathname

  useEffect(() => {
    document.title = "Nouvelle régle"
  })
  const [success_msg, setsuccess_msg] = useState(false)
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid={true}>
          <Row>
            <Col lg={4}>
              <div className="mb-3">
                <label>Label </label>
                <input type="text" className="form-control" />
              </div>{" "}
            </Col>
          </Row>
          <Row>
            <Col lg={4}>
              <div className="mb-3">
                <label>Montant suppérieur a</label>
                <input type="number" className="form-control" />
              </div>{" "}
            </Col>
          </Row>

          <div className="d-flex mt-2">
            <button
              onClick={() => {
                setsuccess_msg(true)
              }}
              type="button"
              className="btn btn-primary w-md"
              style={{ marginTop: "2%" }}
            >
              Valider
            </button>
          </div>
          {success_msg && (
            <SweetAlert
              title="Validé!"
              success
              confirmBtnBsStyle="success"
              cancelBtnBsStyle="danger"
              onConfirm={() => {
                setsuccess_msg(false)
              }}
              onCancel={() => {
                setsuccess_msg(false)
              }}
            >
              Régle créée avec succée!
            </SweetAlert>
          )}
        </Container>
      </div>
    </React.Fragment>
  )
}

NewAuthregle.propTypes = {
  location: PropTypes.object,
}

export default NewAuthregle
