import React, { useEffect, useState } from "react"
import { Row, Col } from "reactstrap"

import ReactApexChart from "react-apexcharts"

//actions
import { getEarningChartsData } from "../../store/actions"

//redux
import { useSelector, useDispatch } from "react-redux"

function Earning2(props) {
  const dispatch = useDispatch()

  const { earningChartData } = useSelector(state => ({
    earningChartData: state.DashboardSaas.earningChartData,
  }))

  const options = {
    chart: {
      toolbar: "false",
      dropShadow: {
        enabled: 0,
        color: "#000",
        top: 18,
        left: 7,
        blur: 8,
        opacity: 0.2,
      },
    },
    dataLabels: {
      enabled: !1,
    },
    colors: ["#556ee6"],
    stroke: {
      curve: "straight",
      width: 3,
    },
    xaxis: {
      categories: [
        "00h",
        "1h",
        "2h",
        "3h",
        "4h",
        "5h",
        "6h",
        "7h",
        "8h",
        "9h",
        "10h",
        "11h",
        "12h",
        "13h",
        "14h",
        "15h",
        "16h",
        "17h",
        "18h",
        "19h",
        "20h",
        "21h",
        "22h",
        "23h",
      ],
    },
  }

  const series = [
    {
      name: "Series 1",
      data: [
        1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1500, 1400, 1400, 1400,
        2000, 2000, 2000, 2100, 2100, 2100, 1800, 1800, 1800, 1800, 1800, 1800,
      ],
    },
  ]

  /*
  call api action to receive data
  */
  useEffect(() => {
    dispatch(getEarningChartsData("jan"))
  }, [dispatch])

  const [seletedMonth, setSeletedMonth] = useState("jan")
  const onChangeMonth = value => {
    setSeletedMonth(value)
    dispatch(getEarningChartsData(value))
  }

  return (
    <React.Fragment>
      <div className="clearfix">
        <div className="float-end">
          <div className="input-group input-group-sm"></div>
        </div>
      </div>

      <Row>
        <Col lg="12">
          <div id="line-chart" dir="ltr">
            <ReactApexChart
              series={series}
              options={options}
              type="line"
              height={260}
              className="apex-charts border bg-white border-primary rounded"
            />
          </div>
        </Col>
      </Row>
    </React.Fragment>
  )
}

export default Earning2
