import React, { useEffect, useState } from "react"

import PropTypes, { object } from "prop-types"
import { useSelector, useDispatch } from "react-redux"
import { Card, Col, Container, Row, CardBody } from "reactstrap"
import { AvForm, AvField } from "availity-reactstrap-validation"
import { useHistory } from "react-router-dom"
import { get, del, post, put } from "../../helpers/api_helper"
import { triggerAlert } from "../../store/actions"
import { toast } from "react-toastify"

const changePassword = ({ location }) => {
  const dispatch = useDispatch()
  const { user } = useSelector(state => {
    return state.user
  })

  useEffect(() => {
    document.title = "Modifier mot de passe"
  }, [])
  let history = useHistory()

  async function handleValidSubmit(e, v) {
    if (v.new_password == v.new_password_confirm) {
      let url = "/api/users/update-password/" + user.id
      let body = {
        old_password: v.old_password,
        new_password: v.new_password,
      }
      console.log(body, url)
      try {
        await put(url, body)
      } catch (e) {}
    } else {
      window.alert("Please confirm your new password")
    }
  }

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid={true}>
          <Card>
            <CardBody>
              <Row>
                <Col lg="4">
                  <AvForm
                    className="form-horizontal mt-3"
                    onValidSubmit={handleValidSubmit}
                  >
                    <div className="mb-3">
                      <AvField
                        name="old_password"
                        label="Ancien mot de passe"
                        className="form-control"
                        type="text"
                        required
                      />
                    </div>
                    <div className="mb-3">
                      <AvField
                        name="new_password"
                        label="Nouveau mot de passe"
                        className="form-control"
                        type="text"
                        required
                      />
                    </div>
                    <div className="mb-3">
                      <AvField
                        name="new_password_confirm"
                        label="Confirmer nouveau mot de passe"
                        className="form-control"
                        type="text"
                        required
                      />
                    </div>

                    <button type="submit" className="btn btn-primary mt-3">
                      Valider
                    </button>
                  </AvForm>
                </Col>
              </Row>
            </CardBody>
          </Card>
        </Container>
      </div>
    </React.Fragment>
  )
}

changePassword.propTypes = {
  location: PropTypes.object,
}

export default changePassword
