import React, { useState, useRef, useEffect } from "react"
import PropTypes from "prop-types"
import { Table, Spinner } from "reactstrap"
import { useLocation } from "react-router-dom/cjs/react-router-dom.min"

const TableCustom = ({ items, cols, loading, tableClass, tableStyle }) => {
  const { pathname } = useLocation()
  return (
    <>
      {loading ? (
        <div
          style={{
            height: 300,
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
          }}
          className="border border-2 d-flex mt-3"
        >
          <Spinner
            style={{
              height: "3rem",
              width: "3rem",
              borderWidth: "5px",
            }}
            color="primary"
          >
            Loading...
          </Spinner>
        </div>
      ) : items && items.length && cols && cols.length ? (
        <Table
          style={{ ...tableStyle }}
          className={tableClass + " mt-3"}
          bordered
          responsive
        >
          <thead>
            <tr>
              {cols.map((h, i) => (
                <th className="text-nowrap" style={h.style} key={i}>
                  {h.text}
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {items.map((e, i) => (
              <tr key={i}>
                {cols.map((c, idx) => (
                  <td
                    style={
                      pathname == "/confirm"
                        ? e.statusCode == 2 || e.statusCode == 5
                          ? { backgroundColor: "rgb(173, 216, 230)" }
                          : {}
                        : pathname == "/draft"
                        ? e.statusCode == 1
                          ? { backgroundColor: "rgb(173, 216, 230)" }
                          : {}
                        : {}
                    }
                    className="text-nowrap"
                    key={idx}
                  >
                    {e[c.dataField]}
                  </td>
                ))}
              </tr>
            ))}
          </tbody>
        </Table>
      ) : (
        <div
          style={{
            height: 300,
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
          }}
          className="border border-2 d-flex mt-3"
        >
          <h3>Aucun résultat</h3>
        </div>
      )}
    </>
  )
}

TableCustom.propTypes = {
  cols: PropTypes.array,
  items: PropTypes.array,
}

export default TableCustom
