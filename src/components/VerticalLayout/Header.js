import PropTypes from "prop-types"
import React, { useState, useEffect } from "react"

import { connect } from "react-redux"
import { Row, Col } from "reactstrap"
import ReactDrawer from "react-drawer"
import "react-drawer/lib/react-drawer.css"
import { Link, useHistory } from "react-router-dom"
import { Breadcrumbs, Button, Typography } from "@mui/material"
import DarkModeToggle from "react-dark-mode-toggle"

// Reactstrap
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap"

// Import menuDropdown
import LanguageDropdown from "../CommonForBoth/TopbarDropdown/LanguageDropdown"
import NotificationDropdown from "../CommonForBoth/TopbarDropdown/NotificationDropdown"
import RatesDropdown from "../CommonForBoth/TopbarDropdown/RatesDropdown"
import ProfileMenu from "../CommonForBoth/TopbarDropdown/ProfileMenu"
import RightSidebar from "../CommonForBoth/RightSidebar"
import megamenuImg from "../../assets/images/megamenu-img.png"

// import images
import github from "../../assets/images/brands/github.png"
import bitbucket from "../../assets/images/brands/bitbucket.png"
import dribbble from "../../assets/images/brands/dribbble.png"
import dropbox from "../../assets/images/brands/dropbox.png"
import mail_chimp from "../../assets/images/brands/mail_chimp.png"
import slack from "../../assets/images/brands/slack.png"
import { del, get, post, put } from "../../helpers/api_helper"

import logo from "../../assets/images/logo.png"
import logoLight from "../../assets/images/logo-white.png"

import { useSelector, useDispatch } from "react-redux"
//i18n
import { withTranslation } from "react-i18next"

// Redux Store
import {
  showRightSidebarAction,
  toggleLeftmenu,
  changeSidebarType,
} from "../../store/actions"
import CashboxDropdown from "components/CommonForBoth/TopbarDropdown/CashboxDropdown"
import moment from "moment"

const Header = props => {
  const [search, setsearch] = useState(false)
  const [megaMenu, setmegaMenu] = useState(false)
  const [socialDrp, setsocialDrp] = useState(false)
  const [modal, setModal] = useState(false)
  const [time, settime] = useState("")
  const [race, setrace] = useState(null)
  const [offer, setoffer] = useState(null)
  const { user } = useSelector(state => {
    return state.user
  })

  useEffect(() => {
    const interval = setInterval(() => {
      settime(moment().format("H:mm:ss"))
    }, 1000)

    return () => clearInterval(interval)
  }, [])

  const history = useHistory()

  const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent)

  const [position, setPosition] = useState()
  const [open, setOpen] = useState(false)

  const toggleTopDrawer = () => {
    setPosition("right")
    setOpen(!open)
  }

  const onDrawerClose = () => {
    setOpen(false)
  }

  function toggleFullscreen() {
    if (
      !document.fullscreenElement &&
      /* alternative standard method */ !document.mozFullScreenElement &&
      !document.webkitFullscreenElement
    ) {
      // current working methods
      if (document.documentElement.requestFullscreen) {
        document.documentElement.requestFullscreen()
      } else if (document.documentElement.mozRequestFullScreen) {
        document.documentElement.mozRequestFullScreen()
      } else if (document.documentElement.webkitRequestFullscreen) {
        document.documentElement.webkitRequestFullscreen(
          Element.ALLOW_KEYBOARD_INPUT
        )
      }
    } else {
      if (document.cancelFullScreen) {
        document.cancelFullScreen()
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen()
      } else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen()
      }
    }
  }

  function tToggle() {
    var body = document.body
    if (window.screen.width <= 998) {
      body.classList.toggle("sidebar-enable")
    } else {
      body.classList.toggle("vertical-collpsed")
      body.classList.toggle("sidebar-enable")
    }
  }

  function logout() {
    localStorage.removeItem("authToken")
    history.push("/login")
  }

  // async function getCounts() {
  //   try {
  //     const promise = await Promise.all([
  //       get("/api/course/get/count"),
  //       get("/api/offers/get/count"),
  //     ])
  //     const { count_course } = promise[0].data.data
  //     const { count_offers } = promise[1].data.data
  //     setrace(count_course)
  //     setoffer(count_offers)
  //   } catch (e) {
  //     console.log(e)
  //   }
  // }

  useEffect(() => {
    // getCounts()
  }, [])

  return (
    <React.Fragment>
      <header
        id="page-topbar"
        style={{
          borderBottom: "1px solid rgb(197, 197, 197)",
          position: "relative",
          left: 0,
        }}
      >
        <Modal
          contentClassName="card-bg-gray"
          isOpen={modal}
          size="md"
          id="modal-logout"
          centered={true}
        >
          <ModalBody>
            <h4>
              <i className="fas fa-exclamation-circle text-danger" />
              Déconnexion
            </h4>
            <hr />
            <span className="d-flex mx-2">
              Vous allez quitter la session,
              <br /> merci de confirmer.
            </span>
          </ModalBody>
          <ModalFooter>
            <button onClick={logout} className="btn btn-primary">
              Déconnexion
            </button>{" "}
            <button
              className="btn btn-secondary"
              onClick={() => setModal(false)}
            >
              Annuler
            </button>
          </ModalFooter>
        </Modal>
        <div
          className="navbar-header"
          style={{ background: "#FAF9F8", height: "40px" }}
        >
          <div className="d-flex">
            {/* <div className="navbar-brand-box d-lg-none d-md-block">
              <Link to="/" className="logo logo-dark">
                <span className="logo-sm">
                  <img src={logo} alt="" style={{ width: "140px" }} />
                </span>
              </Link>

              <Link to="/" className="logo logo-light">
                <span className="logo-sm">
                  <img src={logoLight} alt="" style={{ width: "140px" }} />
                </span>
              </Link>
            </div> */}

            <button
              type="button"
              onClick={() => {
                tToggle()
              }}
              className="btn px-3 font-size-16 header-item menu-toggle"
            >
              <i className="fa fa-fw fa-bars" />
            </button>
            <div className="mx-5 header-username">
              <div className="text-nowrap" style={{ textOverflow: "ellipsis" }}>
                ID{" "}
                <span
                  className="text-primary fw-bold"
                  style={{ marginRight: 5 }}
                >
                  {"{" + user?.company + "}"}{" "}
                </span>
                <span
                  className="text-primary fw-bold"
                  style={{ marginRight: 5 }}
                >
                  {" "}
                  {"{" + user?.companyName + "}"}
                </span>{" "}
                User{" "}
                <span className="text-primary fw-bold">
                  {"{" + user?.username + "}"}
                </span>{" "}
              </div>
            </div>
          </div>

          <div className="d-flex" style={{ height: 50 }}>
            {/* <div className="pt-2 mx-2">
              {props.darkmode ? (
                <button
                  onClick={() => props.setdarkmode(false)}
                  className="btn"
                >
                  <i className="fas fa-sun fs-4" />
                </button>
              ) : (
                <button className="btn">
                  <i
                    onClick={() => props.setdarkmode(true)}
                    className="fas fa-moon fs-4"
                  />
                </button>
              )}
            </div> */}

            <NotificationDropdown />
            {/* <ProfileMenu setModal={setModal} /> */}
            <div className="pt-1 mx-2">
              <button onClick={() => setModal(!modal)} className="btn">
                <i className="bx bx-power-off fs-3 align-middle me-1 text-danger" />
                {/* <span>{props.t("Déconnexion")}</span> */}
              </button>
            </div>
            {/* <div
              onClick={toggleTopDrawer}
              disabled={open}
              className="dropdown d-inline-block"
            >
              <button
                type="button"
                className="btn header-item noti-icon right-bar-toggle "
              >
                <i className="bx bx-cog bx-spin" />
              </button>
            </div> */}
          </div>
        </div>

        <div
          style={{
            borderTop: "1px solid rgb(197, 197, 197)",
            borderBottom: "2px solid #ff7600",
            height: 50,
          }}
          className="px-5 d-flex justify-content-between align-items-center lower-top-bar"
        >
          {/* <DarkModeToggle
            onChange={props.setdarkmode}
            checked={props.darkmode}
            size={60}
            className="mx-4 mt-2" dummy comment
          /> */}
          <div>
            <span className="">Référence utilisateur : </span>
            <span className="mx-1 fw-bold">{user?.id}</span>
            <Button
              style={{
                background: "#ff7600",
                maxHeight: 35,
                textTransform: "unset",
              }}
              className="mx-3"
              onClick={() => history.push("/home")}
            >
              <h5
                className="mt-0 mb-0 text-center text-white"
                style={{
                  borderRadius: "5px",
                  padding: "5px",
                }}
              >
                Mainscreen
              </h5>
            </Button>
          </div>
          <div className="fw-bold">{time}</div>
        </div>
      </header>

      {/* <ReactDrawer open={open} position={position} onClose={onDrawerClose}>
        <RightSidebar onClose={onDrawerClose} />
      </ReactDrawer> */}
    </React.Fragment>
  )
}

Header.propTypes = {
  changeSidebarType: PropTypes.func,
  leftMenu: PropTypes.any,
  leftSideBarType: PropTypes.any,
  showRightSidebar: PropTypes.any,
  showRightSidebarAction: PropTypes.func,
  t: PropTypes.any,
  toggleLeftmenu: PropTypes.func,
}

const mapStatetoProps = state => {
  const { layoutType, showRightSidebar, leftMenu, leftSideBarType } =
    state.Layout
  return { layoutType, showRightSidebar, leftMenu, leftSideBarType }
}

export default connect(mapStatetoProps, {
  showRightSidebarAction,
  toggleLeftmenu,
  changeSidebarType,
})(withTranslation()(Header))
