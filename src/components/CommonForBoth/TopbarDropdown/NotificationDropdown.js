import React, { useState, useEffect, useRef } from "react"
import PropTypes from "prop-types"
import { Link, useHistory } from "react-router-dom"
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  Row,
  Col,
  Alert,
} from "reactstrap"
import SimpleBar from "simplebar-react"
import { del, get, post, put } from "../../../helpers/api_helper"
import { useSelector, useDispatch } from "react-redux"

//Import images
import avatar3 from "../../../assets/images/users/avatar-3.jpg"
import avatar4 from "../../../assets/images/users/avatar-4.jpg"

//i18n
import { withTranslation } from "react-i18next"

const NotificationDropdown = props => {
  // Declare a new state variable, which we'll call "menu"
  const history = useHistory()
  const dispatch = useDispatch()
  const { user } = useSelector(state => {
    return state.user
  })
  const [menu, setMenu] = useState(false)
  const [notifs, setnotifs] = useState([])

  async function getChannels() {
    try {
      const { data } = await get(
        `/api/notifications/subscriptions/${user.user}`
      )

      console.log(data, "channels")
      data.data.forEach(e => {
        let url = `${process.env.REACT_APP_MERCURE}/.well-known/mercure?topic=${e.channel.path}`
        if (e.key != "void") {
          url += `/${e.key}`
        }

        let eventSource = new EventSource(url, {})
        eventSource.onmessage = event => {
          const data = JSON.parse(event.data)
          console.log(data, "notif")

          setnotifs(arr => [...arr, data])

          dispatch(
            triggerAlert({
              type: "info",
              message: "Vous avez une nouvelle notification",
            })
          )
          dispatch(
            triggerAlert({
              type: "",
              message: "",
            })
          )
        }
      })
      const res = await get(`/api/notifications/${user.user}`)

      console.log(res.data)
      setnotifs(res.data.data)
    } catch (e) {
      console.log(e)
    }
  }

  async function readNotif() {
    console.log(user)
    if (!menu && notifs.length) {
      try {
        await put("/api/notifications/read", {
          user: user.user,
          notifications: notifs.map(e => e.uuid),
        })

        // history.push(notif.url)
      } catch (e) {
        console.log(e.response)
      }
    }
  }

  function handleClick(e) {
    if (e.url) {
      history.push(e.url)
    }
  }

  useEffect(() => {
    // getChannels()
  }, [])

  return (
    <React.Fragment>
      <Dropdown
        isOpen={menu}
        toggle={() => {
          setMenu(!menu)
          readNotif()
        }}
        className="dropdown d-inline-block"
        tag="li"
      >
        <DropdownToggle
          className={
            menu
              ? "btn header-item noti-icon "
              : "btn header-item noti-icon  pt-3"
          }
          tag="button"
          id="page-header-notifications-dropdown"
        >
          <i
            className={
              menu
                ? "bx bx-bell text-white bg-primary rounded pb-3 p-2"
                : "bx bx-bell"
            }
          />
          {notifs.length ? (
            <span className="badge bg-danger rounded-pill">
              {notifs.length}
            </span>
          ) : null}
        </DropdownToggle>

        <DropdownMenu
          style={{ border: "2px solid" }}
          className="border-primary rounded dropdown-menu dropdown-menu-lg p-0 dropdown-menu-end"
        >
          <div className="p-3">
            <Row className="align-items-center">
              <Col>
                <h6 className="m-0"> {props.t("Notifications")} </h6>
              </Col>
              {/* <div className="col-auto">
                <a href="#!" className="small">
                  {" "}
                  View All
                </a>
              </div> */}
            </Row>
          </div>
          <SimpleBar style={{ height: "230px" }}>
            {/* {!notifs.length && (
              <p className="text-center">Aucune nouvelle notification</p>
            )} */}
            <Alert color="danger text-center">Coming soon!</Alert>
            {notifs.map(e => (
              <div
                id={e.uuid}
                key={e.uuid}
                onClick={() => handleClick(e)}
                className="text-reset notification-item pointer"
              >
                <div className="media">
                  <div className="avatar-xs me-3">
                    <span className="avatar-title bg-primary rounded-circle font-size-12">
                      <i className="fas fa-exclamation" />
                    </span>
                  </div>
                  <div className="media-body">
                    <h6 className="mt-0 mb-1">{e.content}</h6>
                    <div className="font-size-12 text-muted">
                      {/* <p className="mb-1">{e.content}</p> */}
                      <p className="mb-0">
                        <i className="mdi mdi-clock-outline" />{" "}
                        {props.t("3 min ago")}{" "}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </SimpleBar>
          {/* <div className="p-2 border-top d-grid">
            <Link
              className="btn btn-sm btn-link font-size-14 btn-block text-center"
              to="#"
            >
              <i className="mdi mdi-arrow-right-circle me-1"></i>{" "}
              {props.t("View all")}{" "}
            </Link>
          </div> */}
        </DropdownMenu>
      </Dropdown>
    </React.Fragment>
  )
}

export default withTranslation()(NotificationDropdown)

NotificationDropdown.propTypes = {
  t: PropTypes.any,
}
