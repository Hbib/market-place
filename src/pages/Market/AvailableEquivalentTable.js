import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { withRouter, Link } from "react-router-dom"
import { isEmpty } from "lodash"
import BootstrapTable from "react-bootstrap-table-next"
import paginationFactory, {
  PaginationListStandalone,
  PaginationProvider,
} from "react-bootstrap-table2-paginator"
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit"
import Filter from "../../components/Common/Filter"
import { del, get, post, put } from "../../helpers/api_helper"

import { Col, Row } from "reactstrap"

import currency from "../../assets/currency.json"
import $ from "jquery"
import moment from "moment"

//redux

import { useSelector, useDispatch } from "react-redux"

const AvailableEquivalentTable = props => {
  const [list, setList] = useState([])

  const { user } = useSelector(state => {
    return state.user
  })

  function getList() {
    get("/api/orders/status/" + user.company + "?status[]=9")
      .then(res => {
        console.log(res.data)
        setList(
          res.data.map(e => ({
            id: e.ref,
            domicialiation: e.bank,
            iduser: e.user.id,
            user: e.user.username,
            produit: e.productType,
            currency: e.currency.isoCode,
            sens: "Achat",
            position: e.sessionId,
            bayers: e.user.company && e.user.company.id,
            purchasename: e.user.company && e.user.company.name,
            amount: Number(e.amount).toFixed(e.currency.pattern),
            cible: Number(e.negotationRate).toFixed(3) || "",
            marge_platform:
              e.profit && Number(e.profit.platformShare).toFixed(3),
            profitI: e.profit && e.profit.insightSharePercent,
            profitP: e.profit && e.profit.partnerSharePercent,
            marge_transaction:
              e.profit && Number(e.profit.platformShareValue).toFixed(3),
            net: e.profit && parseFlaot(e.profit.finalPrice).toFixed(3),
            cvnet: e.profit && Number(e.profit.finalEquivelent).toFixed(3),
            margeI: e.profit && Number(e.profit.insightShare).toFixed(3),
            margeP: e.profit && Number(e.profit.partnerShare).toFixed(3),
            // cvcible: 15629,
            ticketnumber: e.ticket && e.ticket.id,
            ticket: e.ticket && (
              <a target="_blank" rel="noreferrer" href={e.ticket.file}>
                ticket pdf
              </a>
            ),
            synd: e.syndMember.map(d => d.percentage),
            price: Number(e.originalRate).toFixed(3),
            contract: e.contrat,
            brute: Number(e.rate).toFixed(3),
            cvbrut: Number(e.equivalent).toFixed(3),
            contrevalue: Number(
              (e.amount * e.originalRate) / e.currency?.unit
            ).toFixed(3),
            agencyChef: e.leaderBoxInfo.ex_op && e.leaderBoxInfo.ex_op.district,
            benefet: e.name,
            dateorder: moment(e.createdAt).format("L"),
            hour: moment(e.createdAt).format("LT"),
            chef: e.leaderBoxInfo.ex_op.id,
            valuedate: "12:00",
            catseller: "A+",
            range: "A",
            typeposition:
              e.syndMember.length == 1 ? "Single position" : "Syndication",
            agency: e.leaderBoxInfo.ex_op.box_agency,

            cvcible: e.negotationRate
              ? Number(
                  (e.negotationRate * e.amount) / e.currency?.unit
                ).toFixed(3)
              : "",
            state: e.status.name,
            Type: e.type == "order" ? "Marché" : "Course",
            contractB: "contrat",
            contractS: "contrat",
            cheffile: e.leaderBoxInfo.id,
          }))
        )
      })
      .catch(err => console.log(err.response))
  }

  useEffect(() => {
    getList()
    $(function () {
      $(".wrapper1").scroll(function () {
        $(".wrapper2").scrollLeft($(".wrapper1").scrollLeft())
      })
      $(".wrapper2").scroll(function () {
        $(".wrapper1").scrollLeft($(".wrapper2").scrollLeft())
      })
    })
    document.title = "Mise à disposition contre valeur"
  }, [])
  const selectRow = {
    hideSelectColumn: true,
  }

  const [selectedCurr, setSelectedCurr] = useState("TND")
  const [filtertype, setFiltertype] = useState("market")
  //pagination customization
  const pageOptions = {
    sizePerPage: 10,
    totalSize: list.length, // replace later with size(orders),
    custom: true,
  }
  const { SearchBar } = Search

  const EcommerceOrderColumns = [
    {
      dataField: "id",
      text: "ID order",
      sort: true,
    },
    {
      dataField: "action",
      text: "Action",
      sort: true,
    },
    {
      dataField: "dateorder",
      text: "Date ",
      sort: true,
    },
    {
      dataField: "hour",
      text: "Heure ",
      sort: true,
    },
    {
      dataField: "Type",
      text: "Type",
      sort: true,
    },
    {
      dataField: "state",
      text: "Statut",
      sort: true,
    },
    {
      dataField: "bayers",
      text: "ID acheteur",
      sort: true,
    },
    {
      dataField: "purchasename",
      text: "Nom acheterur",
      sort: true,
    },
    {
      dataField: "iduser",
      text: "Id utilisateur acheteur",
      sort: true,
    },
    {
      dataField: "user",
      text: "Utilisateur acheteur",
      sort: true,
    },
    {
      dataField: "currency",
      text: "Devise",
      sort: true,
    },
    {
      dataField: "produit",
      text: "Produit",
      sort: true,
    },
    {
      dataField: "typeposition",
      text: "Type position",
      sort: true,
    },

    {
      dataField: "cheffile",
      text: "Chef file",
      sort: true,
    },

    {
      dataField: "synd",
      text: "Exposition",
      sort: true,
    },
    {
      dataField: "agencyChef",
      text: "Agence chef lieu bureau",
      sort: true,
    },
    {
      dataField: "agency",
      text: "Agence bureau ",
      sort: true,
    },

    {
      text: "Contrat de vente",
      dataField: "contract",
      sort: true,
    },
    {
      dataField: "catseller",
      text: "Catégorie vendeur",
      sort: true,
    },
    {
      dataField: "range",
      text: "Catégorie acheteur",
      sort: true,
    },

    {
      dataField: "domicialiation",
      text: "Domiciliation",
      sort: true,
    },
    {
      dataField: "price",
      text: "Prix",
      sort: true,
    },
    {
      dataField: "cible",
      text: "Prix négociation",
      sort: true,
    },
    {
      dataField: "brute",
      text: "Prix brut",
      sort: true,
    },
    {
      dataField: "amount",
      text: "Montant",
      sort: true,
    },

    {
      dataField: "contrevalue",
      text: "Contre valeur",
      sort: true,
    },
    {
      dataField: "cvcible",
      text: "Contre valeur cible",
      sort: true,
    },
    {
      dataField: "cvbrut",
      text: "Contre valeur brute",
      sort: true,
    },
    {
      dataField: "ticketnumber",
      text: "Numéro tickt",
      sort: true,
    },
    {
      dataField: "marge_platform",
      text: "Marge platefotm",
      sort: true,
    },
    {
      dataField: "marge_transaction",
      text: "Marge sur transaction",
      sort: true,
    },
    {
      dataField: "net",
      text: "Prix net",
      sort: true,
    },
    {
      dataField: "cvnet",
      text: "Contre valeur net",
      sort: true,
    },
    {
      dataField: "profitI",
      text: "Part  Insight",
      sort: true,
    },
    {
      dataField: "profitP",
      text: "Part  partenaire",
      sort: true,
    },
    // {
    //   dataField: "profitTransaction",
    //   text: "Marge sur transaction",
    //   sort: true,
    // },
    {
      dataField: "margeI",
      text: "Marge sur transaction Insight",
      sort: true,
    },
    {
      dataField: "margeP",
      text: "Marge sur transaction Partenaire",
      sort: true,
    },
    {
      dataField: "ticket",
      text: "Ticket",
      sort: true,
    },
  ]

  const defaultSorted = [
    {
      dataField: "orderId",
      order: "desc",
    },
  ]

  return (
    <React.Fragment>
      <PaginationProvider
        pagination={paginationFactory(pageOptions)}
        keyField="id"
        columns={EcommerceOrderColumns}
        data={list}
      >
        {({ paginationProps, paginationTableProps }) => (
          <ToolkitProvider
            keyField="id"
            data={list}
            columns={EcommerceOrderColumns}
            bootstrap4
            search
          >
            {toolkitProps => (
              <React.Fragment>
                {" "}
                <Row className="mb-2 justify-content-between">
                  <Col md="3" className="d-flex">
                    {/* <Filter
                      filters={[
                        { label: "Date début", type: "date" },
                        { label: "Date fin", type: "date" },
                        {
                          label: "Devise",
                          type: "select",
                          options: currency,
                        },
                        {
                          label: "Référence",
                          type: "text",
                          options: ["Choisissez"],
                        },
                        {
                          label: "Utilisateur",
                          type: "select",
                          options: ["Tout", "Hbib", "Amal"],
                        },
                      ]}
                    /> */}
                  </Col>
                  <Col md="7"></Col>
                </Row>
                <Row>
                  <Col xl="12">
                    <div className="wrapper2">
                      <div className="div2">
                        <BootstrapTable
                          keyField="id"
                          responsive
                          bordered={true}
                          striped={true}
                          defaultSorted={defaultSorted}
                          selectRow={selectRow}
                          classes={"table align-middle table-nowrap"}
                          headerWrapperClasses={"table-light"}
                          {...toolkitProps.baseProps}
                          {...paginationTableProps}
                        />
                      </div>
                    </div>
                  </Col>
                </Row>
                <div className="pagination justify-content-end">
                  <PaginationListStandalone {...paginationProps} />
                </div>
              </React.Fragment>
            )}
          </ToolkitProvider>
        )}
      </PaginationProvider>
    </React.Fragment>
  )
}

AvailableEquivalentTable.propTypes = {
  onGetOrders: PropTypes.func,
}

export default withRouter(AvailableEquivalentTable)
