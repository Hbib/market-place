import { SAVE_USER } from "./actionTypes"

const initialState = {
  user: null,
}

const user = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_USER:
      state.user = action.payload
      break
  }
  return state
}

export default user
