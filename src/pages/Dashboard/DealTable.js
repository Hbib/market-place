import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { withRouter, Link } from "react-router-dom"
import SweetAlert from "react-bootstrap-sweetalert"
import moment from "moment"

import BootstrapTable from "react-bootstrap-table-next"
import paginationFactory, {
  PaginationListStandalone,
  PaginationProvider,
} from "react-bootstrap-table2-paginator"
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit"
import Filter from "../../components/Common/Filter"

import { Col, Row, Modal, Table } from "reactstrap"
import { del, get, post, put } from "../../helpers/api_helper"
import { useSelector, useDispatch } from "react-redux"

import currency from "../../assets/currency.json"

import $ from "jquery"
//redux

const DealTable = props => {
  const route = window.location.pathname
  const [list, setList] = useState([])

  const { user } = useSelector(state => {
    return state.user
  })

  function getList() {
    get("/api/orders/status/" + user.company + "?status[]=6")
      .then(res => {
        console.log(res.data)
        setList(
          res.data.map(e => ({
            id: e.id,
            domicialiation: e.bank && e.bank,
            iduser: e.user && e.user.id,
            user: e.user && e.user.username,
            produit: e.productType,
            currency: e.currency.isoCode,
            sens: "Achat",
            position: e.sessionId,
            bayers: e.user.company && e.user.company.id,
            purchasename: e.user.company && e.user.company.name,
            amount: Number(e.amount).toFixed(e.currency.pattern),
            cible: Number(e.negotationRate).toFixed(3) || "",
            marge_platform:
              e.profit && Number(e.profit.platformShare).toFixed(3),
            profitI: e.profit && e.profit.insightSharePercent,
            profitP: e.profit && e.profit.partnerSharePercent,
            marge_transaction:
              e.profit && Number(e.profit.platformShareValue).toFixed(3),
            net: e.profit && Number(e.profit.finalPrice).toFixed(3),
            cvnet: e.profit && Number(e.profit.finalEquivelent).toFixed(3),
            margeI: e.profit && Number(e.profit.insightShare).toFixed(3),
            margeP: e.profit && Number(e.profit.partnerShare).toFixed(3),
            cvcible: e.negotationRate
              ? Number(
                  (e.negotationRate * e.amount) / e.currency?.unit
                ).toFixed(3)
              : null,
            ticketnumber: e.ticket && e.ticket.id,
            ticket: e.ticket && (
              <a target="_blank" rel="noreferrer" href={e.ticket.file}>
                ticket pdf
              </a>
            ),
            synd: e.syndMember.map(d => d.percentage),
            price: Number(e.originalRate).toFixed(3),
            contract: e.contrat,
            brute: Number(e.rate).toFixed(3),
            cvbrut: Number(e.equivalent).toFixed(3),

            contrevalue: Number(
              (e.amount * e.originalRate) / e.currency?.unit
            ).toFixed(3),
            agencyChef: e.leaderBoxInfo.ex_op && e.leaderBoxInfo.ex_op.district,
            benefet: e.name,
            dateorder: moment(e.createdAt).format("L"),
            hour: moment(e.createdAt).format("LT"),
            chef: e.leaderBoxInfo.ex_op.id,
            valuedate: "12:00",
            catseller: "A+",
            range: "A",
            typeposition:
              e.syndMember.length == 1 ? "Single position" : "Syndication",
            agency: e.leaderBoxInfo.ex_op.box_agency,

            // domicialiation: "1587",
            state: e.status.name,
            Type: e.type == "order" ? "Marché" : "Course",
            contractB: "contrat",
            contractS: "contrat",
            cheffile: e.leaderBoxInfo.id,
          }))
        )
      })
      .catch(err => console.log(err))
  }

  const selectRow = {
    hideSelectColumn: true,
  }
  function addRow() {
    setRowss(rowss => [
      ...rowss,
      {
        curr: "EUR",
        amount: 0,
        rate: 2.8,
        equivalent: 0,
        modal: false,
        id: rowss[rowss.length - 1].id + 1,
      },
    ])
  }

  function deleteRow(id) {
    let newRows = rowss.filter(item => item.id !== id)
    let newData = inputData
    let total = 0

    newRows.forEach(e => {
      total += Number(e.equivalent)
    })
    //newData.total = total

    setRowss(newRows)
  }
  const [selectedCurr, setSelectedCurr] = useState("TND")
  const [modal_xlarge, setmodal_xlarge] = useState(false)
  const [success_msg, setsuccess_msg] = useState(false)
  const [rowss, setRowss] = useState([
    { id: 0, curr: "EUR", amount: 0, rate: 2.8, equivalent: 0, modal: false },
  ])
  const [inputData, setInputData] = useState(null)
  const [filtertype, setFiltertype] = useState("market")
  function removeBodyCss() {
    document.body.classList.add("no_padding")
  }
  function tog_xlarge() {
    setmodal_xlarge(!modal_xlarge)
    removeBodyCss()
  }

  useEffect(() => {
    getList()
    $(function () {
      $(".wrapper1").scroll(function () {
        $(".wrapper2").scrollLeft($(".wrapper1").scrollLeft())
      })
      $(".wrapper2").scroll(function () {
        $(".wrapper1").scrollLeft($(".wrapper2").scrollLeft())
      })
    })
    document.title = "Reception devise"
  }, [])

  //pagination customization
  const pageOptions = {
    sizePerPage: 10,
    totalSize: list.length, // replace later with size(orders),
    custom: true,
  }
  const { SearchBar } = Search
  function tog_xlarge() {
    setmodal_xlarge(!modal_xlarge)
    removeBodyCss()
  }
  const EcommerceOrderColumns = [
    {
      dataField: "id",
      text: "ID order",
      sort: true,
    },
    {
      dataField: "action",
      text: "Action",
      sort: true,
    },
    {
      dataField: "dateorder",
      text: "Date ",
      sort: true,
    },
    {
      dataField: "hour",
      text: "Heure ",
      sort: true,
    },
    {
      dataField: "Type",
      text: "Type",
      sort: true,
    },
    {
      dataField: "state",
      text: "Statut",
      sort: true,
    },
    {
      dataField: "bayers",
      text: "ID acheteur",
      sort: true,
    },
    {
      dataField: "purchasename",
      text: "Nom acheterur",
      sort: true,
    },
    {
      dataField: "iduser",
      text: "Id utilisateur acheteur",
      sort: true,
    },
    {
      dataField: "user",
      text: "Utilisateur acheteur",
      sort: true,
    },
    {
      dataField: "currency",
      text: "Devise",
      sort: true,
    },
    {
      dataField: "produit",
      text: "Produit",
      sort: true,
    },
    {
      dataField: "typeposition",
      text: "Type position",
      sort: true,
    },

    {
      dataField: "cheffile",
      text: "Chef file",
      sort: true,
    },

    {
      dataField: "synd",
      text: "Exposition",
      sort: true,
    },
    {
      dataField: "agencyChef",
      text: "Agence chef lieu bureau",
      sort: true,
    },
    {
      dataField: "agency",
      text: "Agence bureau ",
      sort: true,
    },

    {
      text: "Contrat de vente",
      dataField: "contract",
      sort: true,
    },
    {
      dataField: "catseller",
      text: "Catégorie vendeur",
      sort: true,
    },
    {
      dataField: "range",
      text: "Catégorie acheteur",
      sort: true,
    },

    {
      dataField: "domicialiation",
      text: "Domiciliation",
      sort: true,
    },
    {
      dataField: "price",
      text: "Prix",
      sort: true,
    },
    {
      dataField: "cible",
      text: "Prix négociation",
      sort: true,
    },
    {
      dataField: "brute",
      text: "Prix brut",
      sort: true,
    },
    {
      dataField: "amount",
      text: "Montant",
      sort: true,
    },

    {
      dataField: "contrevalue",
      text: "Contre valeur",
      sort: true,
    },
    {
      dataField: "cvcible",
      text: "Contre valeur cible",
      sort: true,
    },
    {
      dataField: "cvbrut",
      text: "Contre valeur brute",
      sort: true,
    },
    {
      dataField: "ticketnumber",
      text: "Numéro tickt",
      sort: true,
    },
    {
      dataField: "marge_platform",
      text: "Marge platefotm",
      sort: true,
    },
    {
      dataField: "marge_transaction",
      text: "Marge sur transaction",
      sort: true,
    },
    {
      dataField: "net",
      text: "Prix net",
      sort: true,
    },
    {
      dataField: "cvnet",
      text: "Contre valeur net",
      sort: true,
    },
    {
      dataField: "profitI",
      text: "Part  Insight",
      sort: true,
    },
    {
      dataField: "profitP",
      text: "Part  partenaire",
      sort: true,
    },
    // {
    //   dataField: "profitTransaction",
    //   text: "Marge sur transaction",
    //   sort: true,
    // },
    {
      dataField: "margeI",
      text: "Marge sur transaction Insight",
      sort: true,
    },
    {
      dataField: "margeP",
      text: "Marge sur transaction Partenaire",
      sort: true,
    },
    {
      dataField: "ticket",
      text: "Ticket",
      sort: true,
    },
  ]

  const defaultSorted = [
    {
      dataField: "orderId",
      order: "desc",
    },
  ]

  return (
    <React.Fragment>
      <PaginationProvider
        pagination={paginationFactory(pageOptions)}
        keyField="id"
        columns={EcommerceOrderColumns}
        data={list}
      >
        {({ paginationProps, paginationTableProps }) => (
          <ToolkitProvider
            keyField="id"
            data={list}
            columns={EcommerceOrderColumns}
            bootstrap4
            search
          >
            {toolkitProps => (
              <React.Fragment>
                <Row className="mb-2 justify-content-between"></Row>
                <Row>
                  <Col xl="12">
                    <div className="wrapper2">
                      <div className="div2">
                        <BootstrapTable
                          keyField="id"
                          responsive
                          bordered={true}
                          striped={true}
                          defaultSorted={defaultSorted}
                          selectRow={selectRow}
                          classes={"table align-middle table-nowrap"}
                          headerWrapperClasses={"table-light"}
                          {...toolkitProps.baseProps}
                          {...paginationTableProps}
                        />
                      </div>
                    </div>
                  </Col>
                </Row>
                <div className="pagination  justify-content-end">
                  <PaginationListStandalone {...paginationProps} />
                </div>
              </React.Fragment>
            )}
          </ToolkitProvider>
        )}
      </PaginationProvider>

      <div>
        <Modal size="xl" isOpen={modal_xlarge}>
          <div className="modal-header">
            <h5 className="modal-title " id="myExtraLargeModalLabel">
              Veuillez télécharger les documents necessaires
            </h5>
          </div>
          <div
            className="table-responsive  "
            style={{ paddingLeft: "2%", paddingRight: "2%" }}
          >
            <Table striped className="table mb-0">
              <thead>
                <tr>
                  <th>Document</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {rowss.map((e, i) => {
                  return (
                    <tr key={{ i }}>
                      <td>
                        <div className="upload-button">
                          <i className="fa fa-arrow-circle-up"></i>
                          <input
                            className="file-upload"
                            type="file"
                            accept="image/*"
                          />
                        </div>
                      </td>
                      <td>
                        <div className="d-flex justify-content-around">
                          {rowss.length > 1 && (
                            <button
                              onClick={() => deleteRow(e.id)}
                              className="btn"
                              style={{ fontSize: "larger" }}
                            >
                              <i className="fas fa-trash" />
                            </button>
                          )}
                          {i === rowss.length - 1 && (
                            <button
                              onClick={addRow}
                              className="btn"
                              style={{ fontSize: "larger" }}
                            >
                              <i className="fas fa-plus-circle" />
                            </button>
                          )}
                        </div>
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </Table>
          </div>
          <div style={{ alignItems: "flex-start" }}>
            <button
              onClick={() => {
                setsuccess_msg(true)
                setmodal_xlarge(false)
              }}
              type="button"
              style={{
                width: "20%",
                marginBottom: "1%",
                marginTop: "1%",
              }}
              className="btn btn-primary w-md mx-2"
            >
              Valider
            </button>

            <button
              type="button"
              style={{
                width: "20%",
                marginBottom: "1%",
                marginTop: "1%",
              }}
              className="btn btn-secondary w-md mx-2"
              onClick={() => {
                setmodal_xlarge(false)
              }}
            >
              Annuler
            </button>
          </div>
          {success_msg && (
            <SweetAlert
              title="Validé!"
              success
              confirmBtnBsStyle="success"
              cancelBtnBsStyle="danger"
              onConfirm={() => {
                setsuccess_msg(false)
              }}
            >
              Deal confirmé avec success!
            </SweetAlert>
          )}
        </Modal>
      </div>
    </React.Fragment>
  )
}

DealTable.propTypes = {
  onGetOrders: PropTypes.func,
}

export default withRouter(DealTable)
