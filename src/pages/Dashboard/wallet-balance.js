import React from "react"
import { Row, Col, Card, CardBody } from "reactstrap"
import { Link } from "react-router-dom"

import ReactApexChart from "react-apexcharts"

const series = [76, 67]
const walletOptions = {
  plotOptions: {
    radialBar: {
      offsetY: 0,
      startAngle: 0,
      endAngle: 270,
      hollow: {
        margin: 5,
        size: "35%",
        background: "transparent",
        image: void 0,
      },
      track: {
        show: !0,
        startAngle: void 0,
        endAngle: void 0,
        background: "#f2f2f2",
        strokeWidth: "97%",
        opacity: 1,
        margin: 12,
        dropShadow: {
          enabled: !1,
          top: 0,
          left: 0,
          blur: 3,
          opacity: 0.5,
        },
      },
      dataLabels: {
        name: {
          show: !0,
          fontSize: "16px",
          fontWeight: 600,
          offsetY: -10,
        },
        value: {
          show: !0,
          fontSize: "14px",
          offsetY: 4,
          formatter: function (e) {
            return e + "%"
          },
        },
        total: {
          show: !0,
          label: "Total",
          color: "#373d3f",
          fontSize: "16px",
          fontFamily: void 0,
          fontWeight: 600,
          formatter: function (e) {
            return (
              e.globals.seriesTotals.reduce(function (e, t) {
                return e + t
              }, 0) + "%"
            )
          },
        },
      },
    },
  },
  stroke: {
    lineCap: "round",
  },
  colors: ["#3452e1", "#f1b44c", "#50a5f1"],
  labels: ["Ethereum", "Bitcoin"],
  legend: { show: !1 },
}

const WalletBalance = () => {
  return (
    <React.Fragment>
      <Card>
        <CardBody className="pb-0">
          <h4 className="mb-3">Part du marché</h4>

          <Row>
            <Col lg="8" sm="6">
              <div>
                <div id="wallet-balance-chart">
                  <ReactApexChart
                    options={walletOptions}
                    series={series}
                    type="radialBar"
                    height={300}
                    className="apex-charts"
                  />
                </div>
              </div>
            </Col>

            <Col lg="4" sm="6" className="align-self-center">
              <div>
                <p className="mb-2">
                  <i className="mdi mdi-circle align-middle font-size-10 me-2 text-primary" />{" "}
                  Achat
                </p>
                <h5>57.01 %</h5>
              </div>

              <div className="mt-4">
                <p className="mb-2">
                  <i className="mdi mdi-circle align-middle font-size-10 me-2 text-warning" />{" "}
                  Vente
                </p>
                <h5>44.12 %</h5>
              </div>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </React.Fragment>
  )
}

export default WalletBalance
