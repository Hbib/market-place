import { call, put, fork, takeLatest, takeEvery } from "redux-saga/effects"

// Login Redux States
import { SAVE_USER } from "./actionTypes"
import { saveUser } from "./actions"

function* save({ payload: user }) {
  console.log(user, "here3")
  yield put(saveUser(user))
}

export function* watchUser() {
  yield takeEvery(SAVE_USER, save)
}

function* userSaga() {
  yield all([fork(watchUser)])
}

export default userSaga
