import React, { useState } from "react"
import PropTypes from "prop-types"
import { Link } from "react-router-dom"
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  Row,
  Col,
  Table,
} from "reactstrap"
import currency from "../../../assets/currency"

//i18n
import { withTranslation } from "react-i18next"

const RatesDropdown = props => {
  // Declare a new state variable, which we'll call "menu"
  const [menu, setMenu] = useState(false)

  return (
    <React.Fragment>
      <Dropdown
        isOpen={menu}
        onMouseOver={() => setMenu(true)}
        onMouseLeave={() => setMenu(false)}
        className="dropdown d-inline-block"
        tag="li"
      >
        <DropdownToggle
          className="btn header-item noti-icon"
          tag="button"
          id="page-header-notifications-dropdown"
        >
          <i className="fas fa-coins " />
        </DropdownToggle>

        <DropdownMenu className="dropdown-menu dropdown-menu-lg p-0 rounded dropdown-menu-end">
          <div
            className="p-2 fw-bold text-center"
            style={{ borderBottom: "1px solid #979797a6" }}
          >
            Heure serveur : 13:25:00
          </div>
          <div className="table-responsive">
            <Table striped className="table topbar-drop mb-0">
              <thead>
                <tr>
                  <th>Devise</th>
                  <th>Unité</th>
                  <th>Achat</th>
                  <th>Vente</th>
                </tr>
              </thead>
              <tbody>
                {currency.map(e => (
                  <tr key={e}>
                    <td>
                      <img
                        className=""
                        src={
                          require("../../../assets/flags/" + e + ".png").default
                        }
                        alt={e}
                      />
                      {" " + e}
                    </td>
                    <td>0.00</td>
                    <td>0.00</td>
                    <td>0.00</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        </DropdownMenu>
      </Dropdown>
    </React.Fragment>
  )
}

export default withTranslation()(RatesDropdown)

RatesDropdown.propTypes = {
  t: PropTypes.any,
}
