import React, { Component, useState } from "react"
import MetaTags from "react-meta-tags"
import ProfitMargeTable from "./ProfitMargeTable"
import { Link } from "react-router-dom"
import { Breadcrumbs, Typography } from "@mui/material"
import {
  Container,
  Row,
  Col,
  Button,
  Card,
  CardBody,
  CardTitle,
  Input,
  Label,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Media,
  Table,
  CardHeader,
} from "reactstrap"
var options = {
  chart: { sparkline: { enabled: !0 } },
  dataLabels: { enabled: !1 },
  colors: ["#556ee6"],
  plotOptions: {
    radialBar: {
      hollow: { margin: 0, size: "60%" },
      track: { margin: 0 },
      dataLabels: { show: !1 },
    },
  },
}
// const getChartOptions = index => {
//   var options = {
//     chart: { sparkline: { enabled: !0 } },
//     dataLabels: { enabled: !1 },
//     colors: ["#556ee6"],
//     plotOptions: {
//       radialBar: {
//         hollow: { margin: 0, size: "60%" },
//         track: { margin: 0 },
//         dataLabels: { show: !1 },
//       },
//     },
//   };
//   switch (index) {
//     case 1:
//       options["colors"][0] = "#556ee6";
//       break;
//     case 2:
//       options["colors"][0] = "#34c38f";
//       break;
//     case 3:
//       options["colors"][0] = "#f46a6a";
//       break;
//     default:
//       break;
//   }

//   return options;
// };

class ProfitMarge extends Component {
  constructor(props) {
    super(props)

    this.state = {
      options: {
        plotOptions: {
          radialBar: {
            startAngle: -135,
            endAngle: 135,
            dataLabels: {
              name: {
                fontSize: "13px",
                color: void 0,
                offsetY: 60,
              },
              value: {
                offsetY: 22,
                fontSize: "16px",
                color: void 0,
                formatter: function (e) {
                  return e + "%"
                },
              },
            },
          },
        },
        colors: ["#556ee6"],
        fill: {
          type: "gradient",
          gradient: {
            shade: "dark",
            shadeIntensity: 0.15,
            inverseColors: !1,
            opacityFrom: 1,
            opacityTo: 1,
            stops: [0, 50, 65, 91],
          },
        },
        stroke: {
          dashArray: 4,
        },
        labels: ["Secteur"],
      },
      series: [67],
    }
  }

  render() {
    return (
      <React.Fragment>
        <div className="page-content">
          <Container fluid>
            <Row>
              <Col lg={12}>
                <ProfitMargeTable />
              </Col>
              {/* <Col lg={6}>
                <table className="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>Total marge de profit InsightPlus</th>
                      <th>Total marge de profit Partenaire</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>20000</td>
                      <td>20000</td>
                    </tr>
                  </tbody>
                </table>
              </Col> */}
            </Row>
          </Container>
        </div>
      </React.Fragment>
    )
  }
}

export default ProfitMarge
