import PropTypes from "prop-types"
import React, { useEffect, useState } from "react"
import { Breadcrumbs, Typography } from "@mui/material"
import { Link } from "react-router-dom"
import { withRouter } from "react-router-dom"
//import Lottie from "react-lottie"
//import animation from "../../assets/live"
import {
  changeLayout,
  changeSidebarTheme,
  changeSidebarThemeImage,
  changeSidebarType,
  changeTopbarTheme,
  changeLayoutWidth,
} from "../../store/actions"
import $ from "jquery"

// Layout Related Components
import Header from "./Header"
import Sidebar from "./Sidebar"
import Footer from "./Footer"
import Rightbar from "../CommonForBoth/RightSidebar"

//redux
import { useSelector, useDispatch } from "react-redux"

const Layout = props => {
  const dispatch = useDispatch()
  const [width, setWidth] = useState(window.innerWidth)
  const [footerFix, setFooterFix] = useState(false)
  const [darkmode, setdarkmode] = useState(
    JSON.parse(localStorage.getItem("darkmode")) || false
  )
  const defaultOptions = {
    loop: true,
    autoplay: true,
    // here is where we will declare lottie animation
    // "animation" is what we imported before animationData: animation,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  }
  const {
    isPreloader,
    leftSideBarThemeImage,
    layoutWidth,
    leftSideBarType,
    topbarTheme,
    showRightSidebar,
    leftSideBarTheme,
  } = useSelector(state => ({
    isPreloader: state.Layout.isPreloader,
    leftSideBarThemeImage: state.Layout.leftSideBarThemeImage,
    leftSideBarType: state.Layout.leftSideBarType,
    layoutWidth: state.Layout.layoutWidth,
    topbarTheme: state.Layout.topbarTheme,
    showRightSidebar: state.Layout.showRightSidebar,
    leftSideBarTheme: state.Layout.leftSideBarTheme,
  }))

  const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent)

  const toggleMenuCallback = () => {
    if (leftSideBarType === "default") {
      dispatch(changeSidebarType("condensed", isMobile))
    } else if (leftSideBarType === "condensed") {
      dispatch(changeSidebarType("default", isMobile))
    }
  }

  useEffect(() => {
    localStorage.setItem("darkmode", darkmode)
    setdarkmode(darkmode)
      $(() => {
        if (darkmode) {
          $(
            ".vertical-menu, .navbar-brand-box, body, .lower-top-bar, .lower-sidebar"
          ).css("background", "rgb(42 53 76)")
          $(
            ".line-before span, .sub-menu a, #sidebar-menu h6, .lower-top-bar span"
          ).css("color", "white")
          $("#position-count, #race-count").css("color", "white")
          $("#money").prop(
            "src",
            require("../../assets/money_white.png").default
          )
          $("#running-man").prop(
            "src",
            require("../../assets/running_white.png").default
          )
          $(".custom-scroll").css("background", "#384155")
          // $(
          //   ".custom-scroll:not(.approvedInterm div, .home-card span, .home-card label, .home-card div, .animated-container > div, .animated-container, .animated-container div > div)"
          // ).css("color", "white")
          $(".btn-outline-secondary, .btn-outline-primary").css(
            "color",
            "white"
          )
          $(
            "p ,.MuiStepLabel-label ,label:not(.alert label) ,h3:not(.home-card h3, .animated-container, .animated-container div), h4:not(.home-card h4, .animated-container, .animated-container div), h2:not(.home-card h2, .animated-container h2), .custom-scroll h5:not(.home-card h5, .animated-container h5), .op-table label, td:not(.table-striped td), b:not(.home-card b)"
          ).css("color", "white")
          $(
            "a:not(.sub-menu a, .has-arrow, .home-card a, .approvedInterm a), li:not(.sub-menu a, .home-card a, .approvedInterm li)"
          ).css("color", "white")
          $(".card-body:not(.home-card .card-body)").css(
            "background",
            "#384155"
          )
        } else {
          $(
            ".has-arrow span, .sub-menu a, #sidebar-menu h6, #sidebar-menu span, .lower-top-bar span"
          ).css("color", "")
          $(
            ".vertical-menu, .navbar-brand-box, body, .lower-top-bar, .lower-sidebar"
          ).css("background", "white")
          $("#position-count, #race-count").css("color", "")
          $("#money").prop("src", require("../../assets/money.png").default)
          $("#running-man").prop(
            "src",
            require("../../assets/running.png").default
          )
          $(".custom-scroll").css("background", "#F5FCFF")
          $(".custom-scroll:not(.approvedInterm div)").css("color", "")
          $(".btn-outline-secondary, .btn-outline-primary").css("color", "")
          $(
            "p ,.MuiStepLabel-label ,label ,h3, h4:not(.home-card h4), h2:not(.home-card h2),.custom-scroll h5:not(.home-card h5), .op-table label, td:not(.table-striped td)"
          ).css("color", "")
          $("a:not(.btn)").css("color", "gray")
          $(".card-body:not(.home-card .card-body)").css(
            "background",
            "#F5FCFF"
          )
        }
      })
    
  }, [darkmode])

  /*
  layout  settings
  */

  useEffect(() => {
    if (isPreloader === true) {
      document.getElementById("preloader").style.display = "block"
      document.getElementById("status").style.display = "block"

      setTimeout(function () {
        document.getElementById("preloader").style.display = "none"
        document.getElementById("status").style.display = "none"
      }, 2500)
    } else {
      document.getElementById("preloader").style.display = "none"
      document.getElementById("status").style.display = "none"
    }
  }, [isPreloader])
  const route = window.location.pathname

  function generateLinks() {
    if (route == "/new/race") {
      return (
        <Link underline="hover" color="inherit" to="/list-race">
          Liste courses
        </Link>
      )
    } else if (route == "/auth/new") {
      return (
        <Link underline="hover" color="inherit" to="/auth">
          Autorisation
        </Link>
      )
    } else if (route == "/new/agency") {
      return (
        <Link underline="hover" color="inherit" to="/agencies">
          Liste agences
        </Link>
      )
    } else if (route == "/users/new") {
      return (
        <Link underline="hover" color="inherit" to="/users">
          Liste utilisateurs
        </Link>
      )
    }
  }

  useEffect(() => {
    window.scrollTo(0, 0)

    let onresize = () => {
      setWidth(window.innerWidth)
    }
    window.onresize = onresize
  }, [])

  useEffect(() => {
    dispatch(changeLayout("vertical"))
  }, [dispatch])

  useEffect(() => {
    if (leftSideBarTheme) {
      dispatch(changeSidebarTheme(leftSideBarTheme))
    }
  }, [leftSideBarTheme, dispatch])

  useEffect(() => {
    if (leftSideBarThemeImage) {
      dispatch(changeSidebarThemeImage(leftSideBarThemeImage))
    }
  }, [leftSideBarThemeImage, dispatch])

  useEffect(() => {
    if (layoutWidth) {
      dispatch(changeLayoutWidth(layoutWidth))
    }
  }, [layoutWidth, dispatch])

  useEffect(() => {
    if (leftSideBarType) {
      dispatch(changeSidebarType(leftSideBarType))
    }
  }, [leftSideBarType, dispatch])

  useEffect(() => {
    if (topbarTheme) {
      dispatch(changeTopbarTheme(topbarTheme))
    }
  }, [topbarTheme, dispatch])

  useEffect(() => {
    $(() => {
      if (
        $(".page-content") &&
        $(".page-content")[0] &&
        $(".page-content")[0].offsetHeight
      ) {
        const contentHeight = $(".page-content")[0].offsetHeight + 70
        const windowHeight = window.innerHeight - 94.11
        if (contentHeight < windowHeight) {
          setFooterFix(true)
        } else {
          setFooterFix(false)
        }
      }
    })
  }, [props.location.pathname])

  return (
    <React.Fragment>
      <div id="preloader">
        <div id="status">
          <div className="spinner-chase">
            <div className="chase-dot" />
            <div className="chase-dot" />
            <div className="chase-dot" />
            <div className="chase-dot" />
            <div className="chase-dot" />
            <div className="chase-dot" />
          </div>
        </div>
      </div>

      {/* <div id="layout-wrapper" style={{ background: "rgb(246, 249, 252)" }}>
        <Sidebar
          theme={leftSideBarTheme}
          type={leftSideBarType}
          isMobile={isMobile}
        />
        <div
          className="main-content-wrapper"
          style={{
            width: "100%",
            position: "absolute",
            height: "100%",
            overflowY: "auto",
            boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
            height: "100vh",
          }}
        >
          <div
            style={{
              background: "rgb(246, 249, 252)",
              position: "fixed",
              height: "15px",
              top: 0,
              width: "100%",
              zIndex: 1,
            }}
          ></div>
          <Header toggleMenuCallback={toggleMenuCallback} />
          <div className="main-content bg-white">{props.children}</div>
        </div> */}
      {/* </div> */}

      <div style={{ background: "rgb(246, 249, 252)" }}>
        <Sidebar
          theme={leftSideBarTheme}
          type={leftSideBarType}
          isMobile={isMobile}
        />
        <div
          className="bg-white"
          style={{
            position: "fixed",
            bottom: 0,
            border: "1px solid rgb(197, 197, 197)",
            width: width <= 700 ? "100%" : "calc(100% - 220px)",
            height: "calc(100% - 15px)",
            marginLeft: width <= 700 ? 0 : 220,
            borderTopLeftRadius: "10px",
            overflowY: "hidden",
          }}
        >
          <Header
            darkmode={darkmode}
            setdarkmode={setdarkmode}
            toggleMenuCallback={toggleMenuCallback}
          />

          <div
            className="custom-scroll py-4 "
            style={{
              overflowY: "auto",
              height: "calc(100% - 79.11px)",
              background: "#F5FCFF",
            }}
          >
            <div className="d-flex px-5 mt-2">
              {route == "/auth/new" ? (
                <Link to="/auth" style={{ marginTop: "0.18rem" }}>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="30"
                    height="30"
                    fill="currentColor"
                    className="bi bi-arrow-left-circle text-primary"
                    viewBox="0 0 16 16"
                  >
                    <path
                      fillRule="evenodd"
                      d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-4.5-.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"
                    />
                  </svg>
                </Link>
              ) : route == "/new/race" ? (
                <Link to="/list-race" style={{ marginTop: "0.18rem" }}>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="30"
                    height="30"
                    fill="currentColor"
                    className="bi bi-arrow-left-circle text-primary"
                    viewBox="0 0 16 16"
                  >
                    <path
                      fillRule="evenodd"
                      d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-4.5-.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"
                    />
                  </svg>
                </Link>
              ) : route == "/users/new" ? (
                <Link to="/users" style={{ marginTop: "0.18rem" }}>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="30"
                    height="30"
                    fill="currentColor"
                    className="bi bi-arrow-left-circle text-primary"
                    viewBox="0 0 16 16"
                  >
                    <path
                      fillRule="evenodd"
                      d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-4.5-.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"
                    />
                  </svg>
                </Link>
              ) : route == "/new/agency" ? (
                <Link to="/agencies" style={{ marginTop: "0.18rem" }}>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="30"
                    height="30"
                    fill="currentColor"
                    className="bi bi-arrow-left-circle text-primary"
                    viewBox="0 0 16 16"
                  >
                    <path
                      fillRule="evenodd"
                      d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-4.5-.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"
                    />
                  </svg>
                </Link>
              ) : document.title == "Editer utilisateur" ? (
                <Link to="/users" style={{ marginTop: "0.18rem" }}>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="30"
                    height="30"
                    fill="currentColor"
                    className="bi bi-arrow-left-circle text-primary"
                    viewBox="0 0 16 16"
                  >
                    <path
                      fillRule="evenodd"
                      d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-4.5-.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"
                    />
                  </svg>
                </Link>
              ) : route.split("/")[1] == "users" &&
                route.split("/")[2] == "edit" ? (
                <Link to="/users" style={{ marginTop: "0.18rem" }}>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="30"
                    height="30"
                    fill="currentColor"
                    className="bi bi-arrow-left-circle text-primary"
                    viewBox="0 0 16 16"
                  >
                    <path
                      fillRule="evenodd"
                      d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-4.5-.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"
                    />
                  </svg>
                </Link>
              ) : (
                document.title.split("|")[0] != "MainScreen" && (
                  <Link to="/home" style={{ marginTop: "0.18rem" }}>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="30"
                      height="30"
                      fill="currentColor"
                      className="bi bi-arrow-left-circle text-primary"
                      viewBox="0 0 16 16"
                    >
                      <path
                        fillRule="evenodd"
                        d="M1 8a7 7 0 1 0 14 0A7 7 0 0 0 1 8zm15 0A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-4.5-.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"
                      />
                    </svg>
                  </Link>
                )
              )}
              <h3 className="mb-0 mx-2 pb-1" style={{ marginTop: "0.18rem" }}>
                {document.title.split("|")[0]}{" "}
              </h3>
            </div>
            {
              document.title.split("|")[0] == "MainScreen"
              // && (
              //   <span className="float-end d-flex mx-4">
              //     <div
              //       className="rec"
              //       style={{
              //         borderRadius: 15,
              //         width: 10,
              //         height: 10,
              //         marginTop: 12,
              //         background: "red",
              //       }}
              //       src={require("../../assets/real-time.svg").default}
              //     ></div>
              //     <h5 className="pt-2 mx-2"> Real time</h5>
              //   </span>
              // )
            }
            <div
              className="d-flex justify-content-between"
              style={{ paddingRight: 30 }}
            >
              <Breadcrumbs
                className="d-flex "
                aria-label="breadcrumb"
                style={{ marginLeft: "50px", marginTop: "15px" }}
              >
                <Link underline="hover" color="inherit" to="/home">
                  <small>
                    <i className="fas fa-chevron-right" />
                  </small>{" "}
                  MainScreen
                </Link>
                {generateLinks()}
                {document.title.split("|")[0] != "MainScreen" && (
                  <Link
                    underline="hover"
                    color="inherit"
                    to={"/" + document.title.split("|")[0]}
                  >
                    {document.title.split("|")[0]}
                  </Link>
                )}
              </Breadcrumbs>
              {/* {route == "/draft" || route == "/confirm" ? (
                <h5>Delai: 5 min</h5>
              ) : route == "/deal" ? (
                <h5>Delai: 60 min</h5>
              ) : route == "/ticket" ? (
                <h5>Delai: Avant fin de cession en cours</h5>
              ) : route == "/Market-sale-validation" ? (
                <h5>Delai: Fin de journée</h5>
              ) : route == "/Delivery" ? (
                <h5>Delai: Fin de journée</h5>
              ) : null} */}
            </div>
            {props.children}

            <footer
              className="pt-1 pb-2"
              style={
                !footerFix
                  ? {
                      borderTop: "1px solid rgb(197, 197, 197)",
                      background: "rgb(245, 252, 255)",
                      width: "100%",
                      height: "35px",
                      marginTop: "auto",
                      fontSize: "11px",
                    }
                  : {
                      borderTop: "1px solid rgb(197, 197, 197)",
                      background: "rgb(245, 252, 255)",
                      width: "100%",
                      height: "24px",
                      position: "absolute",
                      bottom: 0,
                      fontSize: "11px",
                    }
              }
            >
              <span className="px-5  text-secondary">
                Le contenu de cette page est protégé.©{new Date().getFullYear()}
                InsightPlus.
              </span>
            </footer>
          </div>
        </div>
      </div>
      {showRightSidebar ? <Rightbar /> : null}
    </React.Fragment>
  )
}

Layout.propTypes = {
  changeLayoutWidth: PropTypes.func,
  changeSidebarTheme: PropTypes.func,
  changeSidebarThemeImage: PropTypes.func,
  changeSidebarType: PropTypes.func,
  changeTopbarTheme: PropTypes.func,
  children: PropTypes.object,
  isPreloader: PropTypes.any,
  layoutWidth: PropTypes.any,
  leftSideBarTheme: PropTypes.any,
  leftSideBarThemeImage: PropTypes.any,
  leftSideBarType: PropTypes.any,
  location: PropTypes.object,
  showRightSidebar: PropTypes.any,
  topbarTheme: PropTypes.any,
}

export default withRouter(Layout)
