import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { withRouter, Link } from "react-router-dom"
import { isEmpty } from "lodash"
import BootstrapTable from "react-bootstrap-table-next"
import paginationFactory, {
  PaginationListStandalone,
  PaginationProvider,
} from "react-bootstrap-table2-paginator"
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit"
import Filter from "../../components/Common/Filter"

import { Col, Row } from "reactstrap"

import currency from "../../assets/currency.json"
import $ from "jquery"

//redux
import bank from "assets/bank.json"
import { useSelector, useDispatch } from "react-redux"

const OnHoldTable = props => {
  const rows = [
    {
      id: "00117",
      user: "Habib",
      produit: "BBE/compte",
      currency: "EUR",
      sens: "Achat",
      position: "AC6222415",
      bayers: "5141",
      purchasename: "El baraka",
      amount: 5000,
      cible: 3,
      brute: 3,
      cvbrute: 15629,
      profitI: "50%",
      profitP: "50%",
      net: 2.88,
      margeI: 0.06,
      margeP: 0.06,
      cvcible: 15629,
      ticketnumber: "AD158956",
      ticket: "TicketFile",
      price: 3.1258,
      brut: 3.1258,
      cvbrute: 15629,
      contrevalue: 15000,
      benefet: "Hbib",
      dateorder: "05/03/2022",
      hour: "09:00",
      chef: "12587",
      valuedate: "12:00",
      Refrence: "AF0000",

      catseller: "A+",
      range: "A",
      typeposition: "Single position",

      agency: "1258",
      domicialiation: "1587",
      state: "Ticket",
      Type: "Marché",
      contractB: "contrat",
      contractS: "contrat",
      action: (
        <div className="d-flex">
          <button className="btn btn-sm btn-success mx-1">Valider</button>
          <button className="btn btn-sm btn-danger mx-1">Rejeter</button>
        </div>
      ),
    },
  ]

  useEffect(() => {
    $(function () {
      $(".wrapper1").scroll(function () {
        $(".wrapper2").scrollLeft($(".wrapper1").scrollLeft())
      })
      $(".wrapper2").scroll(function () {
        $(".wrapper1").scrollLeft($(".wrapper2").scrollLeft())
      })
    })
    document.title = "On hold"
  }, [])
  const selectRow = {
    hideSelectColumn: true,
  }

  const [selectedCurr, setSelectedCurr] = useState("TND")
  const [filtertype, setFiltertype] = useState("market")
  //pagination customization
  const pageOptions = {
    sizePerPage: 10,
    totalSize: rows.length, // replace later with size(orders),
    custom: true,
  }
  const { SearchBar } = Search

  const EcommerceOrderColumns = [
    {
      dataField: "id",
      text: "ID order",
      sort: true,
    },
    {
      dataField: "action",
      text: "Action",
      sort: true,
    },
    {
      dataField: "dateorder",
      text: "Date ",
      sort: true,
    },
    {
      dataField: "hour",
      text: "Heure ",
      sort: true,
    },
    {
      dataField: "Type",
      text: "Type",
      sort: true,
    },
    {
      dataField: "state",
      text: "Statut",
      sort: true,
    },
    {
      dataField: "bayers",
      text: "ID acheteur",
      sort: true,
    },
    {
      dataField: "purchasename",
      text: "Nom acheterur",
      sort: true,
    },
    {
      dataField: "iduser",
      text: "Id utilisateur acheteur",
      sort: true,
    },
    {
      dataField: "user",
      text: "Utilisateur acheteur",
      sort: true,
    },
    {
      dataField: "currency",
      text: "Devise",
      sort: true,
    },
    {
      dataField: "produit",
      text: "Produit",
      sort: true,
    },
    {
      dataField: "typeposition",
      text: "Type position",
      sort: true,
    },

    {
      dataField: "cheffile",
      text: "Chef file",
      sort: true,
    },

    {
      dataField: "synd",
      text: "Menbre syndication",
      sort: true,
    },
    {
      dataField: "agencyChef",
      text: "Agence chef lieu bureau",
      sort: true,
    },
    {
      dataField: "agency",
      text: "Agence bureau ",
      sort: true,
    },

    {
      dataField: "Contrat de vente",
      text: "contract",
      sort: true,
    },
    {
      dataField: "catseller",
      text: "Catégorie vendeur",
      sort: true,
    },
    {
      dataField: "range",
      text: "Catégorie acheteur",
      sort: true,
    },

    {
      dataField: "domicialiation",
      text: "Domiciliation",
      sort: true,
    },
    {
      dataField: "price",
      text: "Prix",
      sort: true,
    },
    {
      dataField: "cible",
      text: "Prix négociation",
      sort: true,
    },
    {
      dataField: "brute",
      text: "Prix brut",
      sort: true,
    },
    {
      dataField: "amount",
      text: "Montant",
      sort: true,
    },

    {
      dataField: "contrevalue",
      text: "Contre valeur",
      sort: true,
    },
    {
      dataField: "cvcible",
      text: "Contre valeur cible",
      sort: true,
    },
    {
      dataField: "cvbrut",
      text: "Contre valeur brute",
      sort: true,
    },
    {
      dataField: "ticketnumber",
      text: "Numéro tickt",
      sort: true,
    },
    {
      dataField: "marge_platform",
      text: "Marge platefotm",
      sort: true,
    },
    {
      dataField: "marge_transaction",
      text: "Marge transaction",
      sort: true,
    },
    {
      dataField: "net",
      text: "Prix net",
      sort: true,
    },
    {
      dataField: "cvnet",
      text: "Contre valeur net",
      sort: true,
    },
    {
      dataField: "profitI",
      text: "Part  Insight",
      sort: true,
    },
    {
      dataField: "profitP",
      text: "Part  partenaire",
      sort: true,
    },
    {
      dataField: "profitTransaction",
      text: "Marge sur transaction",
      sort: true,
    },
    {
      dataField: "margeI",
      text: "Marge sur transaction Insight",
      sort: true,
    },
    {
      dataField: "margeP",
      text: "Marge sur transaction Partenaire",
      sort: true,
    },
    {
      dataField: "ticket",
      text: "Ticket",
      sort: true,
    },
  ]

  const defaultSorted = [
    {
      dataField: "orderId",
      order: "desc",
    },
  ]

  return (
    <React.Fragment>
      <PaginationProvider
        pagination={paginationFactory(pageOptions)}
        keyField="id"
        columns={EcommerceOrderColumns}
        data={rows}
      >
        {({ paginationProps, paginationTableProps }) => (
          <ToolkitProvider
            keyField="id"
            data={rows}
            columns={EcommerceOrderColumns}
            bootstrap4
            search
          >
            {toolkitProps => (
              <React.Fragment>
                {" "}
                <Row className="mb-2 justify-content-between">
                  <Col md="3" className="d-flex">
                    {/* <Filter
                      filters={[
                        { label: "Date début", type: "date" },
                        { label: "Date fin", type: "date" },
                        {
                          label: "Devise",
                          type: "select",
                          options: currency,
                        },
                        {
                          label: "Référence",
                          type: "text",
                          options: ["Choisissez"],
                        },
                        {
                          label: "Utilisateur",
                          type: "select",
                          options: ["Tout", "Hbib", "Amal"],
                        },
                      ]}
                    /> */}
                  </Col>
                  <Col md="3"></Col>
                </Row>
                <Row>
                  <Col xl="12">
                    <div className="wrapper2">
                      <div className="div2">
                        <BootstrapTable
                          keyField="id"
                          responsive
                          bordered={true}
                          striped={true}
                          defaultSorted={defaultSorted}
                          selectRow={selectRow}
                          classes={"table align-middle table-nowrap"}
                          headerWrapperClasses={"table-light"}
                          {...toolkitProps.baseProps}
                          {...paginationTableProps}
                        />
                      </div>
                    </div>
                  </Col>
                </Row>
                <div className="pagination justify-content-end">
                  <PaginationListStandalone {...paginationProps} />
                </div>
              </React.Fragment>
            )}
          </ToolkitProvider>
        )}
      </PaginationProvider>
    </React.Fragment>
  )
}

OnHoldTable.propTypes = {
  onGetOrders: PropTypes.func,
}

export default withRouter(OnHoldTable)
