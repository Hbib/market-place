import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { withRouter, Link } from "react-router-dom"
import moment from "moment"
import currency from "assets/currency"
import { Col, Row } from "reactstrap"
import { ToastContainer, toast } from "react-toastify"
import { NumericFormat } from "react-number-format"
import Filter from "../../components/Common/Filter"
import { del, get, post, put } from "../../helpers/api_helper"

import $ from "jquery"
//redux
import { useSelector, useDispatch } from "react-redux"
import TableCustom from "components/Common/TableCustom"
import PaginationCustom from "components/Common/PaginationCustom"
const GenerateTicketTable = props => {
  const [list, setList] = useState([])
  const route = window.location.pathname
  const [page, setpage] = useState(1)
  const [totalSize, settotalSize] = useState(20)
  const [filter, setfilter] = useState()
  const [loading, setLoading] = useState(false)
  const [actionloading, setactionloading] = useState(false)
  const [totalEquivalent, settotalEquivalent] = useState(0)
  const [totalAmount, settotalAmount] = useState(0)
  const [selectedCurr, setSelectedCurr] = useState()
  const [pattern, setPattern] = useState(0)
  async function generateTicket(orderId) {
    setactionloading(true)
    try {
      await put(
        `/api/orders/confirm-banque/${orderId}`,
        {},
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      getList(filter)
    } catch (e) {
      toast["error"](
        e.response?.data?.message || "Erreur serveur code: " + e.response.status
      )
    } finally {
      setactionloading(false)
    }
  }

  const { user } = useSelector(state => {
    return state.user
  })
  const token = localStorage.getItem("authToken")

  function handleFilter(form) {
    if (form?.currency && form?.currency != "All") {
      setSelectedCurr(form.currency)
    }
    getList(form)
    setfilter(form)
    setPage(1)
  }

  function onPageChange(p) {
    setPage(p)
    if (filter) {
      getList({ ...filter, offset: p })
    } else {
      getList({ offset: p })
    }
  }

  function getList(filter) {
    setLoading(true)
    let url =
      "/api/orders/filter/" +
      user.company +
      "?status[]=700&status[]=11&pageSize=10&expired=0"
    if (!filter || !filter.offset) {
      url += "&offset=1"
    }
    if (filter) {
      for (let key in filter) {
        if (filter[key]) {
          url += `&${key}=${filter[key]}`
        }
      }
    }
    get(url)
      .then(res => {
        console.log(res.data)
        settotalSize(res.data.count)
        settotalEquivalent(res?.data.total_equivalent)
        settotalAmount(res?.data.total_amount)
        setPattern(res?.data.data[0]?.currency.pattern)
        setList(
          res.data.data.map(e => ({
            id_position: e.sessionId,
            id: e.id,
            domicialiation: e.bank,
            iduser: e.user.id,
            user: e.user.username,
            produit: e.productType,
            currency: e.currency.isoCode,
            sens: "Achat",
            position: e.sessionId,
            payment:
              e.cashOutType == "CASH_PAYMENT" ? "Cash" : "Virement bancaire",
            billLines: e.bills
              ? e.bills.map(d => d.nbr + "-" + d.bill.label + "/")
              : null,
            bayers: e.user.company && e.user.company.id,
            purchasename: e.user.company && e.user.company.name,
            amount: (
              <NumericFormat
                displayType="text"
                thousandSeparator=" "
                value={Number(e.amount).toFixed(e.currency.pattern)}
              />
            ),
            agencyChoice: e.leaderBoxInfo.favoriteLocation,
            cible: e.negotationRate && Number(e.negotationRate).toFixed(3),
            // marge_platform: e.profit && e.profit.platformShareValue,
            // profitI: e.profit && e.profit.insightSharePercent,
            // profitP: e.profit && e.profit.partnerSharePercent,
            //  marge_transaction:
            //   e.profit && parseFloat(e.profit.platformShareValue).toFixed(3),
            net: e.profit && Number(e.profit.price).toFixed(3),
            cvnet: (
              <NumericFormat
                displayType="text"
                thousandSeparator=" "
                value={e.profit && Number(e.profit.equivelent).toFixed(3)}
              />
            ),
            // margeI: e.profit && Number(e.profit.insightShare).toFixed(3),
            // margeP: e.profit && Number(e.profit.partnerShare).toFixed(3),
            cvcible: e.negotationRate ? (
              <NumericFormat
                displayType="text"
                thousandSeparator=" "
                value={Number(
                  (e.negotationRate * e.amount) / e.currency?.unit
                ).toFixed(3)}
              />
            ) : (
              ""
            ),
            ticketnumber: e.ticket && e.ticket.reference,
            ticket: e.ticket && (
              <a target="_blank" rel="noreferrer" href={e.ticket.file}>
                ticket pdf
              </a>
            ),
            price: Number(e.originalRate).toFixed(3),
            contract: e.contrat,
            //synd: e.syndMember.map(d => d.percentage),
            brute: Number(e.rate).toFixed(3),
            cvbrut: Number(e.equivalent).toFixed(3),
            contrevalue: (
              <NumericFormat
                displayType="text"
                thousandSeparator=" "
                value={Number(
                  (e.amount * e.originalRate) / e.currency?.unit
                ).toFixed(3)}
              />
            ),
            region: e.members[0].region,
            zone: e.members[0].zone,
            agencyChef: e.agence && e.agence.name,
            benefet: e.name,
            dateorder: moment(e.createdAt).format("DD-MM-yyyy"),
            hour: moment(e.createdAt).format("HH:mm"),
            //chef: e.leaderBoxInfo.ex_op.id,
            // valuedate: "12:00",
            //catseller: "A+",
            //range: "A",
            typeposition: "Single position",
            //   e.syndMember.length == 1 ? "Single position" : "Syndication",
            //agency: e.leaderBoxInfo.ex_op.box_agency,

            // // domicialiation: "1587",
            state: e.status.name,
            Type: e.type == "order" ? "Marché" : "Course",
            // contractB: "contrat",
            // contractS: "contrat",
            cheffile: e.leaderBoxInfo.name,
            // commissionPer: e.user.company?.commission,
            // commissionHt: (
            //   <NumericFormat
            //     displayType="text"
            //     thousandSeparator=" "
            //     value={Number(e.profit?.commissionValueHT).toFixed(3)}
            //   />
            // ),
            // commissionTtc: (
            //   <NumericFormat
            //     displayType="text"
            //     thousandSeparator=" "
            //     value={Number(e.profit?.commissionValueTTC).toFixed(3)}
            //   />
            // ),
            action: user.roles[0] != "ROLE_ROOT" &&
              user.roles[0] != "ROLE_TRADER" &&
              user.roles[0] != "ROLE_ADVANCED_TRADER" && (
                <button
                  disabled={e.status.code == 11 || actionloading}
                  onClick={() => generateTicket(e.id)}
                  className={
                    e.status.code == 11
                      ? "btn btn-sm btn-secondary"
                      : "btn btn-sm btn-primary"
                  }
                >
                  {loading ? (
                    <div
                      className="spinner-border spinner-border-sm text-light"
                      role="status"
                    >
                      <span className="sr-only">Loading...</span>
                    </div>
                  ) : (
                    <span>Générer ticket</span>
                  )}
                </button>
              ),
          }))
        )
        setLoading(false)
      })
      .catch(err => console.log(err.response))
  }

  useEffect(() => {
    getList()
    $(function () {
      $(".wrapper1").scroll(function () {
        $(".wrapper2").scrollLeft($(".wrapper1").scrollLeft())
      })
      $(".wrapper2").scroll(function () {
        $(".wrapper1").scrollLeft($(".wrapper2").scrollLeft())
      })
    })
    document.title = "Générateur ticket"
  }, [])

  const cols = [
    {
      dataField: "id_position",
      text: "Id order",
      sort: true,
    },
    {
      dataField: "action",
      text: "Action",
      sort: true,
    },

    {
      dataField: "purchasename",
      text: "Acheteur",
      sort: true,
    },
    {
      dataField: "cheffile",
      text: "Bureau de change",
      sort: true,
    },
    {
      dataField: "currency",
      text: "Devise",
      sort: true,
    },
    {
      dataField: "price",
      text: "Prix",
      sort: true,
    },
    {
      dataField: "cible",
      text: "Prix négocié",
      sort: true,
    },
    {
      dataField: "amount",
      text: "Valeur",
      sort: true,
    },
    {
      dataField: "agencyChef",
      text: "Agence de livraison",
      sort: true,
    },
    {
      dataField: "payment",
      text: "Mode de livraison",
      sort: true,
    },
    {
      dataField: "state",
      text: "Statut",
      sort: true,
    },
    {
      dataField: "cut",
      text: "Coupure",
      sort: true,
    },
    {
      dataField: "billLines",
      text: "Coupure détails",
      sort: true,
    },

    {
      dataField: "contrevalue",
      text: "Contre valeur",
      sort: true,
    },
    {
      dataField: "cvcible",
      text: "Contre valeur négociée",
      sort: true,
    },
    {
      dataField: "net",
      text: "Ticket price",
      sort: true,
    },
    {
      dataField: "cvnet",
      text: "Contre valeur ticket",
      sort: true,
    },
    {
      dataField: "ticketnumber",
      text: "Numéro tickt",
      sort: true,
    },
    {
      dataField: "ticket",
      text: "Ticket",
      sort: true,
    },
    {
      dataField: "dateorder",
      text: "Date ",
      sort: true,
    },
    {
      dataField: "hour",
      text: "Heure ",
      sort: true,
    },
    {
      dataField: "Type",
      text: "Type",
      sort: true,
    },

    {
      dataField: "user",
      text: "Utilisateur acheteur",
      sort: true,
    },
    {
      dataField: "bayers",
      text: "Id acheteur",
      sort: true,
    },

    {
      dataField: "iduser",
      text: "Id utilisateur acheteur",
      sort: true,
    },

    {
      dataField: "typeposition",
      text: "Type position",
      sort: true,
    },

    {
      dataField: "region",
      text: "Gouvernorat",
      sort: true,
    },
    {
      dataField: "zone",
      text: "Zone",
      sort: true,
    },

    // {
    //   dataField: "contract",
    //   text: "Contrat de vente",
    //   sort: true,
    // },
    {
      dataField: "catseller",
      text: "Catégorie vendeur",
      sort: true,
    },
    {
      dataField: "range",
      text: "Catégorie acheteur",
      sort: true,
    },
  ]

  return (
    <React.Fragment>
      <Col md="3" className="d-flex">
        <Filter
          handleSubmit={handleFilter}
          filters={[
            {
              label: "Date début",
              type: "date",
              name: "startDate",
            },
            {
              label: "Date fin",
              type: "date",
              name: "endDate",
            },
            {
              label: "Devise",
              type: "select",
              options: currency.map(e => ({ label: e, value: e })),
              name: "currency",
            },
            {
              label: "Référence",
              type: "text",
              name: "ref",
            },
          ]}
        />
      </Col>
      <TableCustom cols={cols} items={list} loading={loading} />

      <div className="d-flex ">
        <div className="col-lg-11">
          <h4>
            Total contre valeur :{" "}
            <NumericFormat
              displayType="text"
              thousandSeparator=" "
              value={Number(totalEquivalent).toFixed(3)}
            />
            {" TND"}
          </h4>
          {!!totalAmount && (
            <h4>
              Total valeur :{" "}
              <NumericFormat
                displayType="text"
                thousandSeparator=" "
                value={Number(totalAmount).toFixed(pattern)}
              />
              {` ${selectedCurr}`}
            </h4>
          )}
        </div>
        <div className="col-lg-1">
          <PaginationCustom
            page={page}
            onPageChange={onPageChange}
            totalSize={totalSize}
          />
        </div>
      </div>
      <ToastContainer autoClose={5000} />
    </React.Fragment>
  )
}

GenerateTicketTable.propTypes = {
  onGetOrders: PropTypes.func,
}

export default withRouter(GenerateTicketTable)
