import React, { useState, useRef, useEffect } from "react"
import { del, get, post, put } from "../../helpers/api_helper"
import { useSelector, useDispatch } from "react-redux"

import PropTypes from "prop-types"
import { Link } from "react-router-dom"
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  Row,
  Col,
  Table,
} from "reactstrap"
import $ from "jquery"

const Filter = props => {
  const menuRef = useRef(null)
  // const user = JSON.parse(localStorage.getItem("user"))
  const [isOpen, setIsOpen] = useState(false)
  const [users, setUsers] = useState([])
  const [listagency, setListagency] = useState([])
  const [regions, setRegions] = useState([])
  const [zone, setZone] = useState([])
  const [listchef, setListchef] = useState([])
  const [selectedRegion, setselectedRegion] = useState([])
  const [selectedAgency, setselectedAgency] = useState("")
  const [bankAgency, setBankAgency] = useState([])
  const [isFiltered, setisFiltered] = useState(false)
  const { user } = useSelector(state => {
    return state.user
  })
  const route = location.pathname

  const toggle = () => setIsOpen(!isOpen)

  useEffect(() => {
    // chefData()
  }, [selectedAgency])

  useEffect(() => {
    // agencyData()
    getAgencies()
  }, [])
  useEffect(() => {
    getRegions()
    getZone()
  }, [selectedRegion])
  function agencyData() {
    let url = `http://197.14.56.60:8445/api/agencies`
    get(url)
      .then(res => {
        let arr = []

        setListagency(res.data)
        setselectedAgency(arr[0].id)
      })
      .catch(err => {
        console.log(err.response)
      })
  }
  function chefData() {
    let url = `http://197.14.56.60:8445/api/districts?agency=${selectedAgency}`
    get(url)
      .then(res => {
        let arr = []
        res.data.forEach(e => {
          arr.push({ id: e.id, firstname: e.firstname, lastname: e.lastname })
        })
        setListchef(arr)
      })
      .catch(err => {
        console.log(err.response)
      })
  }
  async function getRegions() {
    const { data } = await get("/api/hive/hive-zone")
    setRegions(data)
  }
  async function getZone() {
    const { data } = await get("/api/hive/hive-region?region=" + selectedRegion)
    setZone(data)
  }
  async function getAgencies() {
    const { data } = await get("/api/agence?company=" + user.company)

    setBankAgency(data)
  }
  function serialize(form) {
    let obj = {}
    form.serializeArray().forEach(e => {
      if (e.value) {
        obj[e.name] = e.value
      }
    })

    return obj
  }

  return (
    <div ref={menuRef}>
      <Dropdown
        toggle={toggle}
        isOpen={isOpen}
        className="dropdown d-inline-block"
        tag="li"
      >
        <div className="d-flex">
          <DropdownToggle
            className={
              isFiltered ? "btn btn-primary text-danger" : "btn btn-primary"
            }
            onClick={() => setIsOpen(!isOpen)}
            style={
              isOpen
                ? { borderBottomLeftRadius: 0, borderBottomRightRadius: 0 }
                : {}
            }
            tag="button"
          >
            <i className="fas fa-filter" /> Filtre
          </DropdownToggle>
          {isFiltered && (
            <button
              onClick={() => {
                document.getElementById("filter-form").reset()
                setIsOpen(false)
                props.handleSubmit(null)
                setisFiltered(false)
              }}
              type="reset"
              className="mx-2 btn btn-danger "
            >
              Reset filtre
            </button>
          )}
        </div>

        <DropdownMenu
          persist={true}
          className="dropdown-menu dropdown-menu-lg  p-3 border-primary"
          style={{ border: "2px solid", borderTopLeftRadius: 0 }}
        >
          <form
            id="filter-form"
            onSubmit={e => {
              e.preventDefault()
              setIsOpen(false)
              props.handleSubmit(serialize($("form")))
              setisFiltered(true)
            }}
          >
            {props.filters &&
              props.filters.map((e, i) => {
                if (e.type == "select") {
                  return (
                    <div
                      key={i}
                      className="d-flex justify-content-between mb-2"
                    >
                      <label>{e.label}</label>
                      <select
                        className="form-select-sm"
                        style={{ width: "45%" }}
                        name={e.name ? e.name : ""}
                      >
                        <option value="">All</option>
                        {e.options.map(option => (
                          <option key={option.value} value={option.value}>
                            {option.label}
                          </option>
                        ))}
                      </select>
                    </div>
                  )
                } else {
                  return (
                    <div
                      key={i}
                      className="d-flex justify-content-between mb-2 "
                    >
                      <label>{e.label}</label>
                      <input
                        name={e.name ? e.name : ""}
                        className="form-control-sm"
                        type={e.type}
                        style={{ width: "45%" }}
                      />
                    </div>
                  )
                }
              })}
            {props.user && (
              <div className="d-flex justify-content-between mb-2">
                <label>Utilisateur</label>
                <select
                  className="form-select-sm"
                  style={{ width: "45%" }}
                  name={props.user.name ? props.user.name : "user"}
                >
                  <option value="">All</option>
                  {users.map(option => (
                    <option key={option.id} value={option.id}>
                      {option.username}
                    </option>
                  ))}
                </select>
              </div>
            )}
            {/* {props.agency && (
              <div className="d-flex justify-content-between mb-2">
                <label>Agence</label>
                <select
                  onChange={e => setselectedAgency(e.currentTarget.value)}
                  className="form-select-sm"
                  style={{ width: "45%" }}
                  name={"agency"}
                >
                  <option></option>
                  {listagency.map(option => (
                    <option key={option.id} value={option.id}>
                      {option.region}
                    </option>
                  ))}
                </select>
              </div>
            )} */}
            {props.regions && (
              <div className="d-flex justify-content-between mb-2">
                <label>Gouvernorat</label>
                <select
                  onChange={e => setselectedRegion(e.currentTarget.value)}
                  className="form-select-sm"
                  style={{ width: "45%" }}
                  name={"regionBox"}
                >
                  <option value="">All</option>
                  {regions.map(option => (
                    <option key={option.id} value={option.name}>
                      {option.name}
                    </option>
                  ))}
                </select>
              </div>
            )}
            {props.zone && (
              <div className="d-flex justify-content-between mb-2">
                <label>Zone</label>
                <select
                  //  onChange={e => setselectedAgency(e.currentTarget.value)}
                  className="form-select-sm"
                  style={{ width: "45%" }}
                  name={"zoneBox"}
                >
                  <option value="">All</option>
                  {zone.map(option => (
                    <option key={option.id} value={option.name}>
                      {option.name}
                    </option>
                  ))}
                </select>
              </div>
            )}
            {props.bankAgency && (
              <div className="d-flex justify-content-between mb-2">
                <label>Agence de livraison</label>
                <select
                  //  onChange={e => setselectedAgency(e.currentTarget.value)}
                  className="form-select-sm"
                  style={{ width: "45%" }}
                  name={"zoneAgence"}
                >
                  <option value="">All</option>
                  {bankAgency.map(option => (
                    <option key={option.id} value={option.name}>
                      {option.name}
                    </option>
                  ))}
                </select>
              </div>
            )}
            {/* {props.chef && (
              <div className="d-flex justify-content-between mb-2">
                <label>Chef lieu</label>
                <select
                  className="form-select-sm"
                  style={{ width: "45%" }}
                  name={"chef"}
                >
                  <option></option>
                  {listchef.map(option => (
                    <option key={option.id} value={option.id}>
                      {option.firstname + " " + option.lastname}
                    </option>
                  ))}
                </select>
              </div>
            )} */}

            <div className="d-flex float-end">
              <button type="submit" className="btn btn-sm btn-primary ">
                Valider
              </button>
            </div>
          </form>
        </DropdownMenu>
      </Dropdown>
    </div>
  )
}

Filter.propTypes = {
  filters: PropTypes.array,
  handleSubmit: PropTypes.func,
  cashbox: PropTypes.object,
  box: PropTypes.object,
  user: PropTypes.object,
  sens: PropTypes.bool,
  zone: PropTypes.bool,
  agency: PropTypes.bool,
  bankAgency: PropTypes.bool,
  regions: PropTypes.bool,
  chef: PropTypes.bool,
}

export default Filter
