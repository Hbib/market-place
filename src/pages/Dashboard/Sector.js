import React, { useState } from "react"
import { Link } from "react-router-dom"
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  NavItem,
  NavLink,
  Row,
  TabContent,
  TabPane,
  DropdownMenu,
  DropdownToggle,
  UncontrolledDropdown,
} from "reactstrap"
import classnames from "classnames"
import img1 from "../../assets/images/small/img-6.jpg"
import img2 from "../../assets/images/small/img-2.jpg"
import img3 from "../../assets/images/small/img-1.jpg"

//SimpleBar
import SimpleBar from "simplebar-react"

const Sector = props => {
  const [activeTab, setActiveTab] = useState("1")

  const toggle = tab => {
    if (activeTab !== tab) setActiveTab(tab)
  }
  return (
    <React.Fragment>
      <Card>
        <CardHeader className="bg-transparent border-bottom">
          <div className="d-flex flex-wrap">
            <div className="me-2">
              <h5 className="mt-1 mb-0">Secteur</h5>
            </div>
            <ul
              className="nav nav-tabs nav-tabs-custom card-header-tabs ms-auto"
              role="tablist"
            >
              <NavItem>
                <NavLink
                  to="#"
                  className={classnames({ active: activeTab === "1" })}
                  onClick={() => {
                    toggle("1")
                  }}
                >
                  Totale
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: activeTab === "2" })}
                  onClick={() => {
                    toggle("2")
                  }}
                >
                  EUR
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: activeTab === "3" })}
                  onClick={() => {
                    toggle("3")
                  }}
                >
                  Nombre opération
                </NavLink>
              </NavItem>
            </ul>
          </div>
        </CardHeader>

        <CardBody className="p-5">
          <Row>
            <Col lg={6}>
              <h3 className="text-center">
                Achat{" "}
                <span style={{ fontSize: "larger" }} className="text-primary">
                  <b>200 </b>{" "}
                  {activeTab !== "3" ? (
                    <small
                      className="text-secondary"
                      style={{ fontSize: "1rem" }}
                    >
                      TND
                    </small>
                  ) : (
                    <small
                      className="text-secondary"
                      style={{ fontSize: "1rem" }}
                    >
                      Nb
                    </small>
                  )}
                </span>
              </h3>
            </Col>

            <Col lg={6} style={{ borderLeft: "1px solid gray" }}>
              <h3 className="text-center">
                Vente{" "}
                <span style={{ fontSize: "larger" }} className="text-primary">
                  <b>120 </b>{" "}
                  {activeTab !== "3" ? (
                    <small
                      className="text-secondary"
                      style={{ fontSize: "1rem" }}
                    >
                      TND
                    </small>
                  ) : (
                    <small
                      className="text-secondary"
                      style={{ fontSize: "1rem" }}
                    >
                      Nb
                    </small>
                  )}
                </span>
              </h3>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </React.Fragment>
  )
}

export default Sector
