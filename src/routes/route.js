import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { Route, Redirect, useHistory } from "react-router-dom"
import { useSelector, useDispatch } from "react-redux"
import { del, get, post, put } from "../helpers/api_helper"
import { saveUser } from "../store/actions"

const Authmiddleware = ({
  component: Component,
  layout: Layout,
  isAuthProtected,
  ...rest
}) => {
  const { user } = useSelector(state => {
    return state.user
  })
  const [done, setDone] = useState(!!user)
  const history = useHistory()
  const dispatch = useDispatch()
  useEffect(() => {
    init()
  }, [])

  function init() {
    const token = localStorage.getItem("authToken")
    const stayOnline = JSON.parse(localStorage.getItem("stayOnline"))
    if ((isAuthProtected && !token) || (!user && !stayOnline)) {
      history.push("/login")
    } else if (token) {
      get("/api/users/get/user-by-token", {
        headers: { Authorization: `Bearer ${token}` },
      })
        .then(res => {
          dispatch(saveUser(res.data))
          localStorage.setItem("authToken", res.data.token)
          setDone(true)
        })
        .catch(err => {
          console.log(err.response)
          history.push("/login")
        })
    }
  }
  return (
    <Route
      {...rest}
      render={props => {
        if (!isAuthProtected) {
          return (
            <Layout>
              <Component {...props} />
            </Layout>
          )
        } else if (isAuthProtected && done) {
          return (
            <Layout>
              <Component {...props} />
            </Layout>
          )
        }
      }}
    />
  )
}

Authmiddleware.propTypes = {
  isAuthProtected: PropTypes.bool,
  component: PropTypes.any,
  location: PropTypes.object,
  layout: PropTypes.any,
}

export default Authmiddleware
