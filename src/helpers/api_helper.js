import axios from "axios"
import accessToken from "./jwt-token-access/accessToken"


//apply base url for axios
const API_URL = process.env.REACT_APP_APIKEY

const axiosApi = axios.create({
  baseURL: API_URL,

})

// axiosApi.defaults.headers.common["Authorization"] = token

axiosApi.interceptors.response.use(
  response => response,
  error => {
    if (error.response?.status == 500) {
      location.replace("/error")
      return
    }

    return Promise.reject(error)
  }
)

export function get(url, config = {}) {
  return axiosApi.get(url, {
    ...config,
    headers: {
      Authorization: `Bearer ${localStorage.getItem("authToken")}`,
    },
  })
}

export function post(url, data, config = {}) {
  return axiosApi.post(
    url,
    { ...data },
    {
      ...config,
      headers: {
        Authorization: `Bearer ${localStorage.getItem("authToken")}`,
      },
    }
  )
}

export function put(url, data, config = {}) {
  return axiosApi.put(
    url,
    { ...data },
    {
      ...config,
      headers: {
        Authorization: `Bearer ${localStorage.getItem("authToken")}`,
      },
    }
  )
}

export function del(url, config = {}) {
  return axiosApi.delete(url, {
    ...config,
    headers: {
      Authorization: `Bearer ${localStorage.getItem("authToken")}`,
    },
  })
}
