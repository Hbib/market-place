import React from "react"
import { Container, Row, Col } from "reactstrap"

const Footer = () => {
  return (
    <React.Fragment>
      <footer
        className="footer"
        style={{
          position: "fixed",
          bottom: 0,
          zIndex: 1000,
          background: "rgb(246, 249, 252)",
          borderTop: "1px solid rgb(197, 197, 197)",
          height: 45,
        }}
      >
        <Container fluid={true}>
          <Row>
            <Col md={6}>{new Date().getFullYear()} © InsightPlus.</Col>
            <Col md={6}>
              <div className="text-sm-end d-none d-sm-block">
                Design & Development by Insight Plus
              </div>
            </Col>
          </Row>
        </Container>
      </footer>
    </React.Fragment>
  )
}

export default Footer
