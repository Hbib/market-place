import PropTypes from "prop-types"
import React from "react"
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"

//i18n
import { withTranslation } from "react-i18next"
import SidebarContent from "./SidebarContent"

import { Link } from "react-router-dom"

import logo from "../../assets/images/exsys_logo.jpg"
import logoDark from "../../assets/images/exsys_logo.jpg"

const Sidebar = props => {
  return (
    <React.Fragment>
      <div
        className="vertical-menu"
        style={{ background: "rgb(255 255 255 / 62%)" }}
      >
        <div
          className="navbar-brand-box pb-2"
          style={{
            background: "rgb(255 255 255 / 62%)",
          }}
        >
          {/* <Link to="/" className="logo logo-dark" style={{ lineHeight: 0 }}>
            <span className="logo-sm">
              <img style={{ width: "120px" }} src={logo} alt="" />
            </span>
            <span className="logo-lg">
              <img style={{ width: "120px" }} src={logo} alt="" />
            </span>
          </Link> */}

          <Link
            to="/"
            className="logo logo-light"
            style={{ lineHeight: 0, marginTop: 16 }}
          >
            {/* <span className="logo-sm">
              <img style={{ width: "120px" }} src={logoDark} alt="" />
            </span> */}
            <span className="logo">
              <img style={{ width: "120px" }} src={logoDark} alt="" />
            </span>
          </Link>
        </div>
        <div data-simplebar className="h-100">
          {props.type !== "condensed" ? <SidebarContent /> : <SidebarContent />}
        </div>
        <div className="sidebar-background"></div>
      </div>
    </React.Fragment>
  )
}

Sidebar.propTypes = {
  type: PropTypes.string,
}

const mapStatetoProps = state => {
  return {
    layout: state.Layout,
  }
}
export default connect(
  mapStatetoProps,
  {}
)(withRouter(withTranslation()(Sidebar)))
