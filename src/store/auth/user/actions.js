import { SAVE_USER } from "./actionTypes"

export const saveUser = user => {
  return {
    type: SAVE_USER,
    payload: user,
  }
}
