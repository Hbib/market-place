import React, { useState } from "react"
import PropTypes from "prop-types"
import { Link } from "react-router-dom"
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  Row,
  Col,
  Table,
} from "reactstrap"
import currency from "../../../assets/currency"

//i18n
import { withTranslation } from "react-i18next"

const CashboxDropdown = props => {
  // Declare a new state variable, which we'll call "menu"
  const [menu, setMenu] = useState(false)

  return (
    <React.Fragment>
      <Dropdown
        isOpen={menu}
        onMouseOver={() => setMenu(true)}
        onMouseLeave={() => setMenu(false)}
        className="dropdown d-inline-block"
        tag="li"
      >
        <DropdownToggle
          className="btn header-item noti-icon mx-3"
          tag="button"
          id="page-header-notifications-dropdown"
        >
          <i className="fas fa-cash-register" />
        </DropdownToggle>

        <DropdownMenu className="dropdown-menu dropdown-menu-lg p-0 dropdown-menu-end">
          <div className="table-responsive">
            <Table striped className="table topbar-drop mb-0">
              <thead>
                <tr>
                  <th>Devise</th>
                  <th>Solde</th>
                </tr>
              </thead>
              <tbody>
                {currency.map(e => (
                  <tr key={e}>
                    <td>
                      <img
                        className=""
                        src={
                          require("../../../assets/flags/" + e + ".png").default
                        }
                        alt={e}
                      />
                      {" " + e}
                    </td>
                    <td>0.00</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        </DropdownMenu>
      </Dropdown>
    </React.Fragment>
  )
}

export default withTranslation()(CashboxDropdown)

CashboxDropdown.propTypes = {
  t: PropTypes.any,
}
