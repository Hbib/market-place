import React, { Component } from "react"
import MetaTags from "react-meta-tags"

import Earning from "./earning"
import Sector from "./Sector"
import Office from "./Office"
import CurrencyTable from "./CurrencyTable"
import WalletBalance from "./wallet-balance"

import { Container, Row, Col } from "reactstrap"

class Dashboard extends Component {
  constructor(props) {
    super(props)

    this.state = {
      options: {
        plotOptions: {
          radialBar: {
            startAngle: -135,
            endAngle: 135,
            dataLabels: {
              name: {
                fontSize: "13px",
                color: void 0,
                offsetY: 60,
              },
              value: {
                offsetY: 22,
                fontSize: "16px",
                color: void 0,
                formatter: function (e) {
                  return e + "%"
                },
              },
            },
          },
        },
        colors: ["#556ee6"],
        fill: {
          type: "gradient",
          gradient: {
            shade: "dark",
            shadeIntensity: 0.15,
            inverseColors: !1,
            opacityFrom: 1,
            opacityTo: 1,
            stops: [0, 50, 65, 91],
          },
        },
        stroke: {
          dashArray: 4,
        },
        labels: ["Secteur"],
      },
      series: [67],
    }
  }
  render() {
    return (
      <React.Fragment>
        <div className="page-content">
          <MetaTags>
            <title>Dashboard | Skote - Hive</title>
          </MetaTags>
          <Container style={{ width: "85%" }} fluid>
            <Row>
              <Col lg={12}>
                <Earning />
              </Col>
              <Col lg={6}>
                <Office />
              </Col>
              <Col lg={6}>
                <Sector />
              </Col>
              <Col lg={8}>
                <CurrencyTable />
              </Col>
              <Col lg={4}>
                <WalletBalance />
              </Col>
            </Row>
          </Container>
        </div>
      </React.Fragment>
    )
  }
}

export default Dashboard
