import React, { Component, useState } from "react"
import MetaTags from "react-meta-tags"

import ReactApexChart from "react-apexcharts"

import CurrencyTable from "./CurrencyTable"

import currency from "../../assets/currency"
import { Container, Row, Col, Card, Table, CardHeader } from "reactstrap"
import BankTable from "./BankTable"
var options = {
  chart: { sparkline: { enabled: !0 } },
  dataLabels: { enabled: !1 },
  colors: ["#556ee6"],
  plotOptions: {
    radialBar: {
      hollow: { margin: 0, size: "60%" },
      track: { margin: 0 },
      dataLabels: { show: !1 },
    },
  },
}

class Dashboard2 extends Component {
  constructor(props) {
    super(props)

    this.state = {
      options: {
        plotOptions: {
          radialBar: {
            startAngle: -135,
            endAngle: 135,
            dataLabels: {
              name: {
                fontSize: "13px",
                color: void 0,
                offsetY: 60,
              },
              value: {
                offsetY: 22,
                fontSize: "16px",
                color: void 0,
                formatter: function (e) {
                  return e + "%"
                },
              },
            },
          },
        },
        colors: ["#556ee6"],
        fill: {
          type: "gradient",
          gradient: {
            shade: "dark",
            shadeIntensity: 0.15,
            inverseColors: !1,
            opacityFrom: 1,
            opacityTo: 1,
            stops: [0, 50, 65, 91],
          },
        },
        stroke: {
          dashArray: 4,
        },
        labels: ["Secteur"],
      },
      series: [67],
    }
  }

  render() {
    return (
      <React.Fragment>
        <div className="page-content">
          <MetaTags>
            <title>Dashboard | Skote - Hive</title>
          </MetaTags>
          <Container style={{ width: "85%" }} fluid>
            <Row>
              <Col lg={6}>
                <Card>
                  <CardHeader className="bg-transparent border-bottom">
                    <div className="d-flex flex-wrap">
                      <div className="me-2">
                        <h3 className="mt-1 mb-0">
                          Bureau:{" "}
                          <span className="text-primary">Jerba change</span>
                        </h3>
                      </div>
                    </div>
                  </CardHeader>

                  <div className="clearfix" style={{ marginBottom: "10%" }}>
                    <div className="float-end">
                      <div className="input-group">
                        <label className="input-group-text">Devise</label>
                        <select className="form-select form-select-sm">
                          {currency.map((j, k) => (
                            <option key={k}>{j}</option>
                          ))}
                        </select>
                      </div>
                    </div>
                  </div>

                  <div className="table-responsive mt-4">
                    <Table className="table align-middle mb-0">
                      <tbody>
                        <tr>
                          <td>
                            <h3 className=" mb-1">Total Achat</h3>
                            <p
                              style={{
                                fontSize: "2rem",
                                color: "#556ee6",
                                marginLeft: "1%",
                              }}
                              className=" mb-1 "
                            >
                              1520
                            </p>
                          </td>
                          <td></td>
                          <td>
                            <h3 className="font-size-23 mb-1">Total Vente</h3>
                            <p
                              style={{
                                fontSize: "2rem",
                                color: "#556ee6",
                                marginLeft: "1%",
                              }}
                              className=" mb-1 font-size-23"
                            >
                              15520
                            </p>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <h3 className="font-size-23 mb-1">Vente</h3>
                            <p
                              style={{
                                fontSize: "2rem",
                                color: "#556ee6",
                                marginLeft: "1%",
                              }}
                              className=" mb-1 font-size-23"
                            >
                              520
                            </p>
                          </td>

                          <td>
                            <div id="radialchart-1">
                              <ReactApexChart
                                options={options}
                                series={[76]}
                                type="radialBar"
                                height={60}
                                width={60}
                                className="apex-charts"
                              />
                            </div>
                          </td>
                          <td>
                            <h3
                              style={{
                                fontSize: "2rem",
                                color: "#556ee6",
                                marginLeft: "1%",
                              }}
                              className="text-muted mb-1"
                            >
                              Part du marché
                            </h3>
                            <h5
                              style={{
                                fontSize: "2rem",
                                color: "#556ee6",
                                marginLeft: "1%",
                              }}
                              className="mb-0"
                            >
                              50 %
                            </h5>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <h3
                              style={{
                                fontSize: "2rem",

                                marginLeft: "1%",
                              }}
                              className="font-size-23 mb-1"
                            >
                              Achat
                            </h3>
                            <p
                              style={{
                                fontSize: "2rem",
                                color: "#556ee6",
                                marginLeft: "1%",
                              }}
                              className=" mb-1 font-size-23"
                            >
                              420
                            </p>
                          </td>

                          <td>
                            <div id="radialchart-1">
                              <ReactApexChart
                                options={options}
                                series={[76]}
                                type="radialBar"
                                height={60}
                                width={60}
                                className="apex-charts"
                              />
                            </div>
                          </td>
                          <td>
                            <h3
                              style={{
                                fontSize: "2rem",
                                color: "#556ee6",
                                marginLeft: "1%",
                              }}
                              className="text-muted mb-1"
                            >
                              Part du marché
                            </h3>
                            <h5
                              style={{
                                fontSize: "2rem",
                                color: "#556ee6",
                                marginLeft: "1%",
                              }}
                              className="mb-0"
                            >
                              70 %
                            </h5>
                          </td>
                        </tr>
                      </tbody>
                    </Table>
                    <div style={{ marginTop: "17%" }}></div>
                  </div>
                </Card>
              </Col>
              <Col lg={6}>
                <CurrencyTable />
              </Col>
            </Row>
          </Container>
        </div>
      </React.Fragment>
    )
  }
}

export default Dashboard2
