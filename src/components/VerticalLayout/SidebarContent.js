import PropTypes from "prop-types"
import React, { useEffect, useRef, useState } from "react"
// //Import Scrollbar
import SimpleBar from "simplebar-react"
// MetisMenu
import MetisMenu from "metismenujs"
import { withRouter } from "react-router-dom"
import { Link } from "react-router-dom"
import { useSelector, useDispatch } from "react-redux"
import { del, get, post, put } from "../../helpers/api_helper"

//i18n
import { withTranslation } from "react-i18next"
import userProfile from "pages/Authentication/user-profile"
import moment from "moment"
const SidebarContent = props => {
  const [footerFix, setFooterFix] = useState(true)
  const [notifs, setNotifs] = useState({})
  const [confirmNotif, setconfirmNotif] = useState(0)
  const today = new Date()
  const date = today.setDate(today.getDate())
  const ref = useRef()
  const { user } = useSelector(state => {
    return state.user
  })

  function getCount() {
    get(
      `/api/orders/count/${user.company}?startDate=${moment(date).format(
        "yyyy-MM-DD"
      )} 00:00:00`
    )
      .then(res => {
        console.log(res.data)
        let acc = 0
        if (res.data[2]) acc += res.data[2]
        if (res.data[3]) acc += res.data[3]
        if (res.data[4]) acc += res.data[4]
        setconfirmNotif(acc)
        setNotifs(res.data)
      })
      .catch(err => console.log(err.response))
  }

  function getUpdates() {
    let eventSource = new EventSource(
      `${process.env.REACT_APP_MERCURE}/.well-known/mercure?topic=/company-notif/${user.company}`,
      {
        withCredentials: true,
      }
    )
    eventSource.onmessage = event => {
      const { data } = JSON.parse(event.data)
      setNotifs(data.sidebar)
    }

    return eventSource
  }

  useEffect(() => {
    const event = getUpdates()

    return () => event.close()
  }, [])

  useEffect(() => {
    if (user) {
      getCount()
    }
  }, [])

  // Use ComponentDidMount and ComponentDidUpdate method symultaniously
  useEffect(() => {
    const pathName = props.location.pathname

    console.log(user, "user here")
    const initMenu = () => {
      new MetisMenu("#side-menu")
      let matchingMenuItem = null
      const ul = document.getElementById("side-menu")
      const items = ul.getElementsByTagName("a")
      for (let i = 0; i < items.length; ++i) {
        if (pathName === items[i].pathname) {
          matchingMenuItem = items[i]
          break
        }
      }
      if (matchingMenuItem) {
        activateParentDropdown(matchingMenuItem)
      }
    }
    initMenu()
  }, [props.location.pathname])
  useEffect(() => {
    ref.current.recalculate()
  })

  function scrollElement(item) {
    if (item) {
      const currentPosition = item.offsetTop
      if (currentPosition > window.innerHeight) {
        ref.current.getScrollElement().scrollTop = currentPosition - 300
      }
    }
  }
  function activateParentDropdown(item) {
    item.classList.add("active")
    const parent = item.parentElement
    const parent2El = parent.childNodes[1]
    if (parent2El && parent2El.id !== "side-menu") {
      parent2El.classList.add("mm-show")
    }
    if (parent) {
      parent.classList.add("mm-active")
      const parent2 = parent.parentElement
      if (parent2) {
        parent2.classList.add("mm-show") // ul tag
        const parent3 = parent2.parentElement // li tag
        if (parent3) {
          parent3.classList.add("mm-active") // li
          parent3.childNodes[0].classList.add("mm-active") //a
          const parent4 = parent3.parentElement // ul
          if (parent4) {
            parent4.classList.add("mm-show") // ul
            const parent5 = parent4.parentElement
            if (parent5) {
              parent5.classList.add("mm-show") // li
              parent5.childNodes[0].classList.add("mm-active") // a tag
            }
          }
        }
      }
      scrollElement(item)
      return false
    }
    scrollElement(item)
    return false
  }
  return (
    <React.Fragment>
      <SimpleBar className="h-100" ref={ref}>
        <div id="sidebar-menu">
          <ul className="metismenu list-unstyled" id="side-menu">
            {/* <h5
              style={{
                marginLeft: "15px",
                paddingTop: "75px",
                color: "black",
              }}
            >
              <i className="fas fa-store" /> Marché & Course
            </h5> */}
            <div className="mx-4 mt-5 pt-5">
              <li>
                <Link
                  className="line-before"
                  style={{
                    paddingLeft: "0.85rem",
                    paddingTop: 6,
                    paddingBottom: 4,
                    cursor: "default",
                  }}
                >
                  {/* <i className="bx bx-home-circle"></i> */}

                  <span>{props.t("Processus order")}</span>
                </Link>
              </li>
              <li style={{ paddingBottom: "0px" }}>
                <Link
                  to="/draft"
                  className=""
                  style={{
                    paddingLeft: "0.85rem",
                    fontSize: "13px",
                    paddingTop: 5,
                    paddingBottom: 0,
                  }}
                >
                  <div
                    className="d-flex justify-content-between"
                    style={{
                      backgroundColor: "#ADD8E6",
                      padding: 10,
                      paddingTop: 5,
                      paddingBottom: 5,
                      borderRadius: 15,
                      width: 160,
                    }}
                  >
                    <span className="mt-1">{props.t("1 Draft Order ")}</span>
                    <span
                      className="badge bg-danger rounded text-white"
                      style={{ fontSize: "0.85rem" }}
                    >
                      {notifs[1] && notifs[1]}
                    </span>
                  </div>
                </Link>
              </li>
              <li style={{ paddingBottom: "0px" }}>
                <Link
                  to="/confirm"
                  className=""
                  style={{
                    paddingLeft: "0.85rem",
                    fontSize: "13px",
                    paddingTop: 5,
                    paddingBottom: 0,
                  }}
                >
                  {/* <i className="bx bx-cog"></i> */}
                  <div
                    className="d-flex justify-content-between"
                    style={{
                      backgroundColor: "#ADD8E6",
                      padding: 10,
                      paddingTop: 5,
                      paddingBottom: 5,
                      borderRadius: 15,
                      width: 160,
                    }}
                  >
                    <span className="mt-1">{props.t("2 Confirmation")}</span>
                    {!!(
                      (Number(notifs[2]) || 0) + (Number(notifs[5]) || 0)
                    ) && (
                      <span
                        className="badge bg-danger rounded text-white"
                        style={{ fontSize: "0.85rem" }}
                      >
                        {(Number(notifs[2]) || 0) + (Number(notifs[5]) || 0)}
                      </span>
                    )}
                  </div>
                </Link>
              </li>
              <li style={{ paddingBottom: "0px" }}>
                <Link
                  to="/bills-notes"
                  className=""
                  style={{
                    paddingLeft: "0.85rem",
                    fontSize: "13px",
                    paddingTop: 5,
                    paddingBottom: 0,
                  }}
                >
                  {/* <i className="bx bx-cog"></i> */}
                  <div
                    className="d-flex justify-content-between"
                    style={{
                      backgroundColor: "#ADD8E6",
                      padding: 10,
                      paddingTop: 5,
                      paddingBottom: 5,
                      borderRadius: 15,
                      width: 160,
                    }}
                  >
                    <span className="mt-1">{props.t("3 Bank Notes ")}</span>
                    {!!notifs[7] && (
                      <span
                        className="badge bg-danger rounded text-white"
                        style={{ fontSize: "0.85rem" }}
                      >
                        {notifs[7]}
                      </span>
                    )}
                  </div>
                </Link>
              </li>
              <li style={{ paddingBottom: "0px" }}>
                <Link
                  to="/ticket"
                  className=""
                  style={{
                    paddingLeft: "0.85rem",
                    fontSize: "13px",
                    paddingTop: 5,
                    paddingBottom: 0,
                  }}
                >
                  {/* <i className="bx bx-cog"></i> */}
                  <div
                    className="d-flex justify-content-between"
                    style={{
                      backgroundColor: "#ADD8E6",
                      padding: 10,
                      paddingTop: 5,
                      paddingBottom: 5,
                      borderRadius: 15,
                      width: 160,
                    }}
                  >
                    <span className="mt-1">{props.t("4 Ticket ")}</span>
                    {!!(
                      (Number(notifs[700]) || 0) + (Number(notifs[11]) || 0)
                    ) && (
                      <span
                        className="badge bg-danger rounded text-white"
                        style={{ fontSize: "0.85rem" }}
                      >
                        {(Number(notifs[11]) || 0) + (Number(notifs[700]) || 0)}
                      </span>
                    )}
                  </div>
                </Link>
              </li>

              <li style={{ paddingBottom: "0px" }}>
                <Link
                  to="/Delivery"
                  style={{
                    paddingLeft: "0.85rem",
                    fontSize: "13px",
                    paddingTop: 5,
                    paddingBottom: 0,
                  }}
                >
                  {/* <i className="bx bx-cog"></i> */}
                  <div
                    className="d-flex justify-content-between"
                    style={{
                      backgroundColor: "#ADD8E6",
                      padding: 10,
                      paddingTop: 5,
                      paddingBottom: 5,
                      borderRadius: 15,
                      width: 160,
                    }}
                  >
                    <span>{props.t("5 Delivery")}</span>
                    {!!(
                      (Number(notifs[13]) || 0) + (Number(notifs[14]) || 0)
                    ) && (
                      <span
                        className="badge bg-danger rounded text-white"
                        style={{ fontSize: "0.85rem" }}
                      >
                        {(Number(notifs[13]) || 0) + (Number(notifs[14]) || 0)}
                      </span>
                    )}
                  </div>
                </Link>
              </li>
              <li style={{ paddingBottom: "0px" }}>
                <Link
                  to="/book"
                  style={{
                    paddingLeft: "0.85rem",
                    fontSize: "13px",
                    paddingTop: 5,
                    paddingBottom: 0,
                  }}
                >
                  {/* <i className="bx bx-cog"></i> */}
                  <div
                    className=""
                    style={{
                      backgroundColor: "#ADD8E6",
                      padding: 10,
                      paddingTop: 5,
                      paddingBottom: 5,
                      borderRadius: 15,
                      width: 160,
                    }}
                  >
                    <span>{props.t("6 Livre")}</span>
                  </div>
                </Link>
              </li>

              <li style={{ paddingBottom: "0px" }}>
                <Link
                  to="/generate-ticket"
                  style={{
                    paddingLeft: "0.85rem",
                    fontSize: "13px",
                    paddingTop: 5,
                    paddingBottom: 0,
                    marginTop: 15,
                  }}
                >
                  {/* <i className="bx bx-cog"></i> */}
                  <div
                    className="d-flex justify-content-between"
                    style={{
                      backgroundColor: "#667ADC",
                      padding: 10,
                      paddingTop: 5,
                      paddingBottom: 5,
                      borderRadius: 15,
                      width: 160,
                    }}
                  >
                    <span>{props.t("Générateur ticket")}</span>
                    {!!notifs[700] && (
                      <span
                        className="badge bg-danger rounded text-white"
                        style={{ fontSize: "0.85rem" }}
                      >
                        {notifs[700]}
                      </span>
                    )}
                  </div>
                </Link>
              </li>
              <li style={{ paddingBottom: "0px" }}>
                <Link
                  to="/change-password"
                  style={{
                    paddingLeft: "0.85rem",
                    fontSize: "13px",
                    paddingTop: 5,
                    paddingBottom: 0,
                    marginTop: 15,
                  }}
                >
                  {/* <i className="bx bx-cog"></i> */}
                  <div
                    className="d-flex justify-content-between"
                    style={{
                      backgroundColor: "#ADD8E6",
                      padding: 10,
                      paddingTop: 5,
                      paddingBottom: 5,
                      borderRadius: 15,
                      width: 160,
                    }}
                  >
                    <span>{props.t("Change Password")}</span>
                    {!!notifs[700] && (
                      <span
                        className="badge bg-danger rounded text-white"
                        style={{ fontSize: "0.85rem" }}
                      >
                        {notifs[700]}
                      </span>
                    )}
                  </div>
                </Link>
              </li>
              {/* <li
                style={{ paddingBottom: "0px" }}
                onClick={() => setFooterFix(!footerFix)}
              >
                <Link
                  to="/on-hold"
                  className={
                    document.title.split("|")[0] == "On hold"
                      ? "active-link line-before"
                      : "line-before"
                  }
                  style={{ paddingLeft: "0.85rem", fontSize: "13px" }}
                >
                  <i className="fas fa-chevron-right d-lg-none" />
                  <span>{props.t("On hold")}</span>
                </Link>
              </li> */}
            </div>
          </ul>
        </div>
        <footer
          style={{
            borderTop: "2px solid #ff7600",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            zIndex: 100,
            position: "fixed",
            bottom: 0,
            width: "220px",
            height: 40,
            fontSize: 13,
          }}
          className="mt-4 lower-sidebar"
        >
          Insight plus product © {new Date().getFullYear()}
        </footer>
      </SimpleBar>
    </React.Fragment>
  )
}
SidebarContent.propTypes = {
  location: PropTypes.object,
  t: PropTypes.any,
}
export default withRouter(withTranslation()(SidebarContent))
