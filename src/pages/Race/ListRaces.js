import React, { useEffect, useState } from "react"

import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit"
import { ToastContainer, toast } from "react-toastify"
import { Col, Container, Row, Modal, ModalBody } from "reactstrap"
import { useSelector, useDispatch } from "react-redux"
import { del, get, post, put } from "../../helpers/api_helper"
import { Link } from "react-router-dom"
import moment from "moment"
import PaginationCustom from "components/Common/PaginationCustom"
import TableCustom from "components/Common/TableCustom"
const ListRaces = () => {
  const route = location.pathname

  useEffect(() => {
    document.title = "Liste courses"
  })
  const [success_msg, setsuccess_msg] = useState(false)
  const [modal, setModal] = useState(false)
  const [infoModal, setInfoModal] = useState(false)
  const [visibility, setVisibility] = useState("display")
  const [list, setList] = useState([])
  const [totalSize, settotalSize] = useState(10)
  const [page, setpage] = useState(1)
  const [loading, setLoading] = useState(false)

  const { user } = useSelector(state => {
    return state.user
  })

  const notify = (text, type) => toast[type](text)

  function closeRace(id) {
    put(`/api/course/close/${id}`)
      .then(res => {
        console.log(res)
        getList()
        setpage(1)
        notify("Course cloturé avec succée", "success")
      })
      .catch(err => console.log(err.response))
  }

  function onPageChange(p) {
    setpage(p)
    getList({ offset: p })
  }

  function getList(filter) {
    setLoading(true)
    let url = `/api/course?company=${user.company}&pageSize=10`
    if (!filter?.offset) {
      url += "&offset=1"
    }
    if (filter) {
      for (let key in filter) {
        if (filter[key]) {
          url += `&${key}=${filter[key]}`
        }
      }
    }
    get(url)
      .then(res => {
        console.log(res)

        settotalSize(res.data.count)
        setList(
          res.data.data.map(e => ({
            id: e.id,
            currency: e.currency.isoCode,
            amount: e.amount,
            closingpersent: e.coursePercent,
            closing: e.courseSum,
            cours: e.rate,
            status: e.isOpen == "1" ? "En cours" : "Cloturée",
            date: moment(e.expireDate).format("L"),
            action: (
              <button
                onClick={() => closeRace(e.id)}
                className="btn btn-sm btn-danger"
                disabled={!e.isOpen}
              >
                Cloturer
              </button>
            ),
          }))
        )
        setLoading(false)
      })
      .catch(err => console.log(err.response))
  }

  const cols = [
    {
      text: "Id",
      dataField: "id",
      sort: true,
    },
    {
      text: "Devise",
      dataField: "currency",
      sort: true,
    },
    {
      text: "Montant",
      dataField: "amount",
      sort: true,
    },
    {
      text: "Prix de vente",
      dataField: "cours",
      sort: true,
    },
    {
      text: "Statut",
      dataField: "status",
      sort: true,
    },
    {
      text: "%Closing",
      dataField: "closingpersent",
      sort: true,
    },
    {
      text: "Montant closing",
      dataField: "closing",
      sort: true,
    },
    {
      text: "Date maturité",
      dataField: "date",
      sort: true,
    },
    {
      text: "Action",
      dataField: "action",
      sort: false,
    },
  ]
  useEffect(() => {
    getList()
    document.onkeydown = function (evt) {
      evt = evt || window.event
      var isEscape = false
      if ("key" in evt) {
        isEscape = evt.key === "Escape" || evt.key === "Esc"
      } else {
        isEscape = evt.keyCode === 27
      }
      if (isEscape) {
        setRows(rows => {
          let newRows = rows.map(ele => ele)
          newRows[modalId].modal = false
          return newRows
        })
        setShow({ isOpen: false })
      }
    }
  }, [])
  useEffect(() => {
    document.title = "Liste courses"
  }, [])
  const { SearchBar } = Search
  return (
    <React.Fragment>
      <ToastContainer autoClose={5000} />
      <div className="page-content">
        <Modal
          contentClassName="card-bg-gray"
          isOpen={infoModal}
          size="md"
          id="modal-info"
          centered={true}
        >
          <ModalBody>
            {" "}
            <div className="d-flex justify-content-between mb-3">
              <h4 className="mt-2">
                info client: <span className="text-primary">Bekir Hbib</span>
              </h4>
              <i
                onClick={() => {
                  setRows(rows => {
                    let newRows = rows.map(ele => ele)
                    newRows[modalId].modal = false
                    return newRows
                  })
                  setShow({ isOpen: false })
                }}
                className="btn pt-0 pb-0 bx bx-x-circle fs-2"
              />
            </div>
            <h4>
              Total achat: <span className="text-primary fw-bold">12000</span>
              <hr />
              Total vente: <span className="text-primary fw-bold">12000</span>
            </h4>
          </ModalBody>
        </Modal>

        <Container fluid={true}>
          {user.roles[0] != "ROLE_ROOT" && user.roles[0] != "ROLE_TRADER" && (
            <Link className="btn btn-primary" to="/new/race">
              Nouveau
            </Link>
          )}
          <TableCustom cols={cols} items={list} loading={loading} />

          <div className="d-flex justify-content-end">
            <PaginationCustom
              page={page}
              onPageChange={onPageChange}
              totalSize={totalSize}
            />
          </div>
        </Container>
      </div>
    </React.Fragment>
  )
}

export default ListRaces
