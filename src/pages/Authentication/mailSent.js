import PropTypes from "prop-types"
import MetaTags from "react-meta-tags"
import React, { useState } from "react"
import { Row, Col, Alert, Card, CardBody, Container } from "reactstrap"
import SweetAlert from "react-bootstrap-sweetalert"
//redux
import { useSelector, useDispatch } from "react-redux"

import { withRouter, Link } from "react-router-dom"

// availity-reactstrap-validation
import { AvForm, AvField } from "availity-reactstrap-validation"

// action
import { userForgetPassword } from "../../store/actions"

// import images
import profile from "../../assets/images/profile-img.png"
import logo from "../../assets/images/logo.svg"
import mailSentimage from "assets/images/mailSent.jpg"
const mailSent = props => {
  const dispatch = useDispatch()

  const { forgetError, forgetSuccessMsg } = useSelector(state => ({
    forgetError: state.ForgetPassword.forgetError,
    forgetSuccessMsg: state.ForgetPassword.forgetSuccessMsg,
  }))

  function handleValidSubmit(event, values) {
    dispatch(userForgetPassword(values, props.history))
  }

  return (
    <React.Fragment>
      <MetaTags>
        <title>
          Forget Password | Skote - React Admin & Dashboard Template
        </title>
      </MetaTags>

      <div className="bg-white" style={{ height: "100vh" }}>
        <Container>
          <Card className="overflow-hidden mb-5 bg-white">
            <div className="w-50 text-center mx-auto">
              <img src={mailSentimage} alt="" className="img-fluid" />
              <h1 style={{ textAlign: "center", backgroundColor: "white" }}>
                Un mail vous a été envoyé avec votre nouveau mot de passe!
              </h1>
            </div>
          </Card>
          <div className="mt-5 text-center">
            <h4 className="mb-5">
              Go back to{" "}
              <Link to="login" className="font-weight-medium text-primary">
                Login
              </Link>{" "}
            </h4>
            <p>
              © {new Date().getFullYear()} Hive +. Crafted with{" "}
              <i className="mdi mdi-heart text-danger" /> by INSGHT PLUS
            </p>
          </div>
        </Container>
      </div>
    </React.Fragment>
  )
}

mailSent.propTypes = {
  history: PropTypes.object,
}

export default withRouter(mailSent)
