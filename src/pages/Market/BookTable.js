import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { withRouter, Link } from "react-router-dom"
import { isEmpty } from "lodash"
import BootstrapTable from "react-bootstrap-table-next"
import paginationFactory, {
  PaginationListStandalone,
  PaginationProvider,
} from "react-bootstrap-table2-paginator"
import Filter from "../../components/Common/Filter"
import { NumericFormat } from "react-number-format"
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit"
import moment from "moment"

import { Col, Row, Table } from "reactstrap"

import currency from "../../assets/currency.json"
import $ from "jquery"
import { del, get, post, put } from "../../helpers/api_helper"

//redux
import PaginationCustom from "components/Common/PaginationCustom"
import TableCustom from "components/Common/TableCustom"

import { useSelector, useDispatch } from "react-redux"
import { compareDates } from "../../helpers/compareDates"

const BookTable = props => {
  const [list, setList] = useState([])
  const day = new Date()
  const date = day.setDate(day.getDate() - 1)
  const [page, setPage] = useState(1)
  const [totalSize, settotalSize] = useState(0)
  const [filter, setfilter] = useState()
  const [loading, setloading] = useState(false)
  const [listTotals, setListTotals] = useState([])
  const [totalEquivalent, settotalEquivalent] = useState(0)
  const [totalAmount, settotalAmount] = useState(0)
  const [selectedCurr, setSelectedCurr] = useState()
  const [pattern, setPattern] = useState(0)
  const { user } = useSelector(state => {
    return state.user
  })

  function handleFilter(form) {
    if (form?.currency && form?.currency != "All") {
      setSelectedCurr(form.currency)
    }
    getList(form)
    setfilter(form)
    setPage(1)
  }

  function onPageChange(p) {
    setPage(p)
    if (filter) {
      getList({ ...filter, offset: p })
    } else {
      getList({ offset: p })
    }
  }
  function getList(filter) {
    setloading(true)
    let url =
      "/api/orders/filter/" +
      user.company +
      "?status[]=13&status[]=14&status[]=999&status[]=12&status[]=10&status[]=9&status[]=8&status[]=4&status[]=3&pageSize=10"

    if (!filter || !filter.offset) {
      url += "&offset=1"
    }
    if (filter) {
      for (let key in filter) {
        if (key == "endDate") {
          console.log("hhh")
          url += `&endDate=${filter[key]} 23:59:59`
        } else if (key == "startDate") {
          url += `&startDate=${filter[key]} 00:00:00 `
        } else {
          url += `&${key}=${filter[key]}`
        }
      }
    } else {
      url += `&endDate=${moment(date).format("yyyy-MM-DD")} 23:59:59`
    }
    get(url)
      .then(res => {
        settotalSize(res?.data?.count)
        settotalEquivalent(res?.data.total_equivalent)
        settotalAmount(res?.data.total_amount)
        setPattern(res?.data.data[0]?.currency.pattern)
        let arr = res.data.data.map(e => ({
          id_position: e.sessionId,
          //domicialiation: e.bank,
          payment:
            e.cashOutType == "CASH_PAYMENT" ? "Cash" : "Virement bancaire",
          iduser: e.user.id,
          user: e.user.username,
          produit: e.productType,
          currency: e.currency.isoCode,
          sens: "Achat",
          position: e.sessionId,
          bayers: e.user.company && e.user.company.id,
          purchasename: e.user.company && e.user.company.name,
          amount: (
            <NumericFormat
              displayType="text"
              thousandSeparator=" "
              value={Number(e.amount).toFixed(e.currency.pattern)}
            />
          ),
          cible: e.negotationRate && e.negotationRate,

          net: e.profit && e.profit.price,
          cvnet: (
            <NumericFormat
              displayType="text"
              thousandSeparator=" "
              value={e.profit && Number(e.profit.equivalent).toFixed(3)}
            />
          ),

          closer: e.rejectedBy && e.rejectedBy.name,

          region: e.members[0].region,
          zone: e.members[0].zone,

          ticketnumber: e.ticket && e.ticket.id,
          ticket: e.ticket && (
            <a
              target="_blank"
              rel="noreferrer"
              href={e.ticket.file}
              className="btn btn-primary btn-sm"
            >
              Voir pdf
            </a>
          ),
          price: e.originalRate,
          // contract: e.contrat,
          brute: e.rate,
          // commissionPer: e.user.company?.commission,
          // commissionHt: (
          //   <NumericFormat
          //     displayType="text"
          //     thousandSeparator=" "
          //     value={Number(e.profit?.commissionValueHT).toFixed(3)}
          //   />
          // ),
          // commissionTtc: (
          //   <NumericFormat
          //     displayType="text"
          //     thousandSeparator=" "
          //     value={Number(e.profit?.commissionValueTTC).toFixed(3)}
          //   />
          // ),
          cvbrut: (
            <NumericFormat
              displayType="text"
              thousandSeparator=" "
              value={Number(e.equivalent).toFixed(3)}
            />
          ),
          contrevalue: (
            <NumericFormat
              displayType="text"
              thousandSeparator=" "
              value={Number(
                (e.amount * e.originalRate) / e.currency?.unit
              ).toFixed(3)}
            />
          ),
          agencyChef: e.agence && e.agence.name,
          benefet: e.name,

          dateorder: moment(e.createdAt).format("DD-MM-yyyy"),
          hour: moment(e.createdAt).format("HH:mm"),

          // catseller: "A+",
          // range: "A",
          typeposition: "Single position",

          cvcible: e.negotationRate ? (
            <NumericFormat
              displayType="text"
              thousandSeparator=" "
              value={(
                (Number(e.negotationRate) * Number(e.amount)) /
                e.currency?.unit
              ).toFixed(3)}
            />
          ) : null,
          billLines: e.bills
            ? e.bills.map(d => d.nbr + "-" + d.bill.label + "/")
            : null,
          agencyChoice: e.leaderBoxInfo.favoriteLocation,
          state: e.status.name,
          Type: e.type == "order" ? "Marché" : "Course",
          contractB: "contrat",
          contractS: "contrat",
          cheffile: e.leaderBoxInfo.name,
        }))

        setList(arr)
        setloading(false)
      })
      .catch(err => console.log(err.response))
  }

  useEffect(() => {
    getList()
    $(function () {
      $(".wrapper1").scroll(function () {
        $(".wrapper2").scrollLeft($(".wrapper1").scrollLeft())
      })
      $(".wrapper2").scroll(function () {
        $(".wrapper1").scrollLeft($(".wrapper2").scrollLeft())
      })
    })
    document.title = "Livre | Hive+"
  }, [])

  const cols = [
    {
      dataField: "id_position",
      text: "Id order",
      sort: true,
    },
    {
      dataField: "action",
      text: "Action",
      sort: true,
    },
    {
      dataField: "state",
      text: "Statut",
      sort: true,
    },
    {
      dataField: "cheffile",
      text: "Bureau de change",
      sort: true,
    },
    {
      dataField: "currency",
      text: "Devise",
      sort: true,
    },
    {
      dataField: "price",
      text: "Prix",
      sort: true,
    },
    {
      dataField: "cible",
      text: "Prix négocié",
      sort: true,
    },
    {
      dataField: "amount",
      text: "Valeur",
      sort: true,
    },
    {
      dataField: "agencyChef",
      text: "Agence de livraison",
      sort: true,
    },
    {
      dataField: "payment",
      text: "Mode de livraison",
      sort: true,
    },

    {
      dataField: "cut",
      text: "Coupure",
      sort: true,
    },
    {
      dataField: "billLines",
      text: "Coupure détails",
      sort: true,
    },

    {
      dataField: "contrevalue",
      text: "Contre valeur",
      sort: true,
    },
    {
      dataField: "cvcible",
      text: "Contre valeur négociée",
      sort: true,
    },
    {
      dataField: "net",
      text: "Ticket price",
      sort: true,
    },
    {
      dataField: "cvnet",
      text: "Contre valeur ticket",
      sort: true,
    },

    {
      dataField: "dateorder",
      text: "Date ",
      sort: true,
    },
    {
      dataField: "hour",
      text: "Heure ",
      sort: true,
    },
    {
      dataField: "Type",
      text: "Type",
      sort: true,
    },

    {
      dataField: "purchasename",
      text: "Acheteur",
      sort: true,
    },
    {
      dataField: "user",
      text: "Utilisateur acheteur",
      sort: true,
    },
    {
      dataField: "bayers",
      text: "Id acheteur",
      sort: true,
    },

    {
      dataField: "iduser",
      text: "Id utilisateur acheteur",
      sort: true,
    },

    {
      dataField: "typeposition",
      text: "Type position",
      sort: true,
    },

    {
      dataField: "region",
      text: "Gouvernorat",
      sort: true,
    },
    {
      dataField: "zone",
      text: "Zone",
      sort: true,
    },
    // {
    //   dataField: "commissionPer",
    //   text: "Commission INSIGHT %",
    //   sort: true,
    // },

    // {
    //   dataField: "commissionHt",
    //   text: "Commission INSIGHT HT",
    //   sort: true,
    // },

    // {
    //   dataField: "commissionTtc",
    //   text: "Commission INSIGHT TTC",
    //   sort: true,
    // },

    // {
    //   dataField: "contract",
    //   text: "Contrat de vente",
    //   sort: true,
    // },
    {
      dataField: "catseller",
      text: "Catégorie vendeur",
      sort: true,
    },
    {
      dataField: "range",
      text: "Catégorie acheteur",
      sort: true,
    },

    {
      dataField: "ticketnumber",
      text: "Numéro tickt",
      sort: true,
    },
    {
      dataField: "ticket",
      text: "Ticket",
      sort: true,
    },
  ]

  const defaultSorted = [
    {
      dataField: "orderId",
      order: "desc",
    },
  ]

  return (
    <React.Fragment>
      <Col md="3" className="d-flex">
        <Filter
          regions
          zone
          bankAgency
          handleSubmit={handleFilter}
          filters={[
            {
              label: "Date début",
              type: "date",
              name: "startDate",
            },
            {
              label: "Date fin",
              type: "date",
              name: "endDate",
            },
            {
              label: "Devise",
              type: "select",
              options: currency.map(e => ({ label: e, value: e })),
              name: "currency",
            },
            {
              label: "Statut",
              type: "select",
              options: [
                { label: "Draft rejected", value: "3" },
                { label: "Draft canceled", value: "4" },
                { label: "Rejected bank", value: "8" },
                { label: "Rejected box", value: "9" },
                { label: "Confirmation canceled", value: "10" },
                { label: "Ticket rejected", value: "12" },
                { label: "Delivered", value: "13" },
                { label: "Delivery rejected", value: "14" },
              ],
              name: "statusCode",
            },
          ]}
        />
        {/* <a
          className="btn btn-primary mx-2"
          target="_blank"
          rel="noreferrer"
          href={`${process.env.REACT_APP_APIKEY}/api/orders/exportOrder/${user.id}?status[]=10&status[]=404`}
        >
          <i className="fas fa-file-excel" /> Excel
        </a> */}
      </Col>
      <TableCustom cols={cols} items={list} loading={loading} />{" "}
      <div className="d-flex justify-content-end">
        <PaginationCustom
          page={page}
          onPageChange={onPageChange}
          totalSize={totalSize}
        />
      </div>
      <div className="d-flex ">
        <div className="col-lg-11">
          <h4>
            Total contre valeur :{" "}
            <NumericFormat
              displayType="text"
              thousandSeparator=" "
              value={Number(totalEquivalent).toFixed(3)}
            />
            {" TND"}
          </h4>
          {!!totalAmount && (
            <h4>
              Total valeur :{" "}
              <NumericFormat
                displayType="text"
                thousandSeparator=" "
                value={Number(totalAmount).toFixed(pattern)}
              />
              {` ${selectedCurr}`}
            </h4>
          )}
        </div>
      </div>
    </React.Fragment>
  )
}

BookTable.propTypes = {
  onGetOrders: PropTypes.func,
}

export default withRouter(BookTable)
