import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { withRouter, Link } from "react-router-dom"
import { isEmpty } from "lodash"
import BootstrapTable from "react-bootstrap-table-next"
import paginationFactory, {
  PaginationListStandalone,
  PaginationProvider,
} from "react-bootstrap-table2-paginator"
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit"
import Filter from "../../components/Common/Filter"
import { del, get, post, put } from "../../helpers/api_helper"

import { Col, Row } from "reactstrap"
import moment from "moment"

import currency from "../../assets/currency.json"
import $ from "jquery"
import PaginationCustom from "components/Common/PaginationCustom"
import TableCustom from "components/Common/TableCustom"
//redux

import { useSelector, useDispatch } from "react-redux"
import { compareDates } from "../../helpers/compareDates"

const CashOutTable = props => {
  const [list, setList] = useState([])
  const [page, setpage] = useState(1)
  const [totalSize, settotalSize] = useState(0)
  const [filter, setfilter] = useState()
  const [loading, setloading] = useState(false)
  const [listTotals, setListTotals] = useState([])

  const { user } = useSelector(state => {
    return state.user
  })
  function handleFilter(form) {
    getList(form)
    setfilter(form)
    setPage(1)
  }

  function onPageChange(p) {
    setpage(p)
    if (filter) {
      getList({ ...filter, offset: p })
    } else {
      getList({ offset: p })
    }
  }
  function getList(filter) {
    setloading(true)
    let url = "/api/orders/filter/" + user.id + "?status[]=10&pageSize=10"
    let startDate, endDate
    if (!filter || !filter.offset) {
      url += "&offset=1"
    }
    if (filter) {
      for (let key in filter) {
        if (filter[key]) {
          url += `&${key}=${filter[key]}`
        }
      }
    }
    get(url)
      .then(res => {
        console.log(res)
        settotalSize(res.data.count)
        let arr = res.data.data.map(e => ({
          id: e.ref,
          domicialiation: e.bank,
          iduser: e.user.id,
          user: e.user.username,
          produit: e.productType,
          currency: e.currency.isoCode,
          sens: "Achat",
          position: e.sessionId,
          bayers: e.user.company && e.user.company.id,
          purchasename: e.user.company && e.user.company.name,
          amount: Number(e.amount).toFixed(e.currency.pattern),
          cible: Number(e.negotationRate).toFixed(3) || "",
          marge_platform: e.profit && Number(e.profit.platformShare).toFixed(3),
          synd: e.syndMember.map(d => d.percentage),
          profitI: e.profit && e.profit.insightSharePercent,
          profitP: e.profit && e.profit.partnerSharePercent,
          marge_transaction:
            e.profit && Number(e.profit.platformShareValue).toFixed(3),
          net: e.profit && Number(e.profit.finalPrice).toFixed(3),
          cvnet: e.profit && Number(e.profit.finalEquivelent).toFixed(3),
          margeI: e.profit && Number(e.profit.insightShare).toFixed(3),
          margeP: e.profit && Number(e.profit.partnerShare).toFixed(3),
          cvcible: e.negotationRate
            ? Number((e.negotationRate * e.amount) / e.currency?.unit).toFixed(
                3
              )
            : null,
          ticketnumber: e.ticket && e.ticket.id,
          ticket: e.ticket && (
            <a
              target="_blank"
              rel="noreferrer"
              href={e.ticket.file}
              className="btn btn-primary"
            >
              <i className="fas fa-arrow-up-right-from-square" /> Voir pdf
            </a>
          ),
          price: Number(e.originalRate).toFixed(3),
          contract: e.contrat,
          brute: Number(e.rate).toFixed(3),
          cvbrut: Number(e.equivalent).toFixed(3),
          contrevalue: Number(
            (e.amount * e.originalRate) / e.currency?.unit
          ).toFixed(3),
          agencyChef: e.leaderBoxInfo.ex_op && e.leaderBoxInfo.ex_op.district,
          benefet: e.name,
          dateorder: moment(e.createdAt).format("L"),
          hour: moment(e.createdAt).format("LT"),
          chef: e.leaderBoxInfo.ex_op.id,
          valuedate: "12:00",
          catseller: "A+",
          range: "A",
          typeposition:
            e.syndMember.length == 1 ? "Single position" : "Syndication",
          agency: e.leaderBoxInfo.ex_op.box_agency,

          state: e.status.name,
          Type: e.type == "order" ? "Marché" : "Course",
          contractB: "contrat",
          contractS: "contrat",
          cheffile: e.leaderBoxInfo.id,
        }))

        // if (startDate && endDate) {
        //   arr = arr.filter(
        //     e =>
        //       compareDates(
        //         moment(e.dateorder).format("L"),
        //         moment(startDate).format("L")
        //       ) >= 0 &&
        //       compareDates(
        //         moment(endDate).format("L"),
        //         moment(e.dateorder).format("L")
        //       ) >= 0
        //   )
        // } else if (startDate) {
        //   console.log("here")
        //   arr = arr.filter(
        //     e =>
        //       compareDates(
        //         moment(e.dateorder).format("L"),
        //         moment(startDate).format("L")
        //       ) >= 0
        //   )
        // } else if (endDate) {
        //   arr = arr.filter(
        //     e =>
        //       compareDates(
        //         moment(endDate).format("L"),
        //         moment(e.dateorder).format("L")
        //       ) >= 0
        //   )
        // }
        // if (filter && filter.currency) {
        //   arr = arr.filter(e => e.currency == filter.currency)
        // }
        // if (filter && filter.ref) {
        //   arr = arr.filter(e => e.id == filter.ref)
        // }
        // if (filter && filter.agency) {
        //   arr = arr.filter(
        //     e => e.syndMember[0].ex_op.box_agency == filter.agency
        //   )
        // }
        // if (filter && filter.chef) {
        //   arr = arr.filter(e => e.syndMember[0].ex_op.district == filter.chef)
        // }
        setList(arr)
        setloading(false)
      })
      .catch(err => console.log(err.response))
  }

  useEffect(() => {
    getList()
    $(function () {
      $(".wrapper1").scroll(function () {
        $(".wrapper2").scrollLeft($(".wrapper1").scrollLeft())
      })
      $(".wrapper2").scroll(function () {
        $(".wrapper1").scrollLeft($(".wrapper2").scrollLeft())
      })
    })
    document.title = "CashOut"
  }, [])

  const cols = [
    {
      dataField: "id",
      text: "ID order",
      sort: true,
    },
    {
      dataField: "action",
      text: "Action",
      sort: true,
    },
    {
      dataField: "state",
      text: "Statut",
      sort: true,
    },
    {
      dataField: "dateorder",
      text: "Date ",
      sort: true,
    },
    {
      dataField: "hour",
      text: "Heure ",
      sort: true,
    },
    {
      dataField: "Type",
      text: "Type",
      sort: true,
    },

    {
      dataField: "bayers",
      text: "ID acheteur",
      sort: true,
    },
    {
      dataField: "purchasename",
      text: "Nom acheterur",
      sort: true,
    },
    {
      dataField: "iduser",
      text: "Id utilisateur acheteur",
      sort: true,
    },
    {
      dataField: "user",
      text: "Utilisateur acheteur",
      sort: true,
    },
    {
      dataField: "currency",
      text: "Devise",
      sort: true,
    },
    {
      dataField: "produit",
      text: "Produit",
      sort: true,
    },
    {
      dataField: "typeposition",
      text: "Type position",
      sort: true,
    },

    {
      dataField: "cheffile",
      text: "Chef file",
      sort: true,
    },

    {
      dataField: "synd",
      text: "Exposition",
      sort: true,
    },
    {
      dataField: "agencyChef",
      text: "Agence chef lieu bureau",
      sort: true,
    },
    {
      dataField: "agency",
      text: "Agence bureau ",
      sort: true,
    },

    {
      text: "Contrat de vente",
      dataField: "contract",
      sort: true,
    },
    {
      dataField: "catseller",
      text: "Catégorie vendeur",
      sort: true,
    },
    {
      dataField: "range",
      text: "Catégorie acheteur",
      sort: true,
    },

    {
      dataField: "domicialiation",
      text: "Domiciliation",
      sort: true,
    },
    {
      dataField: "price",
      text: "Prix",
      sort: true,
    },
    {
      dataField: "cible",
      text: "Prix négociation",
      sort: true,
    },
    {
      dataField: "brute",
      text: "Prix brut",
      sort: true,
    },
    {
      dataField: "amount",
      text: "Montant",
      sort: true,
    },

    {
      dataField: "contrevalue",
      text: "Contre valeur",
      sort: true,
    },
    {
      dataField: "cvcible",
      text: "Contre valeur cible",
      sort: true,
    },
    {
      dataField: "cvbrut",
      text: "Contre valeur brute",
      sort: true,
    },
    {
      dataField: "ticketnumber",
      text: "Numéro tickt",
      sort: true,
    },
    {
      dataField: "marge_platform",
      text: "Marge platefotm",
      sort: true,
    },
    {
      dataField: "marge_transaction",
      text: "Marge sur transaction",
      sort: true,
    },
    {
      dataField: "net",
      text: "Prix net",
      sort: true,
    },
    {
      dataField: "cvnet",
      text: "Contre valeur net",
      sort: true,
    },
    {
      dataField: "profitI",
      text: "Part  Insight",
      sort: true,
    },
    {
      dataField: "profitP",
      text: "Part  partenaire",
      sort: true,
    },
    // {
    //   dataField: "profitTransaction",
    //   text: "Marge sur transaction",
    //   sort: true,
    // },
    {
      dataField: "margeI",
      text: "Marge sur transaction Insight",
      sort: true,
    },
    {
      dataField: "margeP",
      text: "Marge sur transaction Partenaire",
      sort: true,
    },
    {
      dataField: "ticket",
      text: "Ticket",
      sort: true,
    },
  ]

  const defaultSorted = [
    {
      dataField: "orderId",
      order: "desc",
    },
  ]

  return (
    <React.Fragment>
      <Col md="3" className="d-flex">
        <Filter
          agency
          chef
          handleSubmit={handleFilter}
          filters={[
            {
              label: "Date début",
              type: "date",
              name: "startDate",
            },
            {
              label: "Date fin",
              type: "date",
              name: "endDate",
            },
            {
              label: "Devise",
              type: "select",
              options: currency.map(e => ({ label: e, value: e })),
              name: "currency",
            },
            {
              label: "Référence",
              type: "text",
              name: "ref",
            },
          ]}
        />{" "}
        <a
          className="btn btn-primary mx-2"
          target="_blank"
          rel="noreferrer"
          href={`${process.env.REACT_APP_APIKEY}/api/orders/exportOrder/${user.id}?status[]=10`}
        >
          <i className="fas fa-file-excel" /> Excel
        </a>
      </Col>
      <TableCustom cols={cols} items={list} loading={loading} />

      <div className="d-flex justify-content-end">
        <PaginationCustom
          page={page}
          onPageChange={onPageChange}
          totalSize={totalSize}
        />
      </div>
    </React.Fragment>
  )
}

CashOutTable.propTypes = {
  onGetOrders: PropTypes.func,
}

export default withRouter(CashOutTable)
