import { SAVE_NOTIF } from "./actionTypes"

export const saveNotif = notif => {
  return {
    type: SAVE_NOTIF,
    payload: notif,
  }
}
