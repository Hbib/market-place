import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { withRouter, Link } from "react-router-dom"
import { del, get, post, put } from "../../helpers/api_helper"
import Select from "react-select"
import moment from "moment"
import { NumericFormat } from "react-number-format"
import BootstrapTable from "react-bootstrap-table-next"
import paginationFactory, {
  PaginationListStandalone,
  PaginationProvider,
} from "react-bootstrap-table2-paginator"
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit"
import currency from "assets/currency"
import { Col, Row, Table } from "reactstrap"
import { saveNotif } from "../../store/actions"

import Filter from "../../components/Common/Filter"
import PaginationCustom from "components/Common/PaginationCustom"
import $ from "jquery"
//redux
import bank from "assets/bank.json"
import { useSelector, useDispatch } from "react-redux"
import TableCustom from "components/Common/TableCustom"
const BillsNotesTable = props => {
  const dispatch = useDispatch()
  const route = window.location.pathname
  const today = new Date()
  const date = today.setDate(today.getDate())
  const [list, setList] = useState([])
  const [loading, setloading] = useState([])
  const [agencies, setAgencies] = useState({})

  const [filter, setfilter] = useState()
  const [page, setPage] = useState(1)
  const [totalSize, settotalSize] = useState(0)
  const [totalEquivalent, settotalEquivalent] = useState(0)
  const [totalAmount, settotalAmount] = useState(0)
  const [selectedCurr, setSelectedCurr] = useState()
  const [pattern, setPattern] = useState(0)
  const { user } = useSelector(state => {
    return state.user
  })
  function handleFilter(form) {
    if (form?.currency && form?.currency != "All") {
      setSelectedCurr(form.currency)
    }
    getList(form)
    setfilter(form)
    setPage(1)
  }
  function onPageChange(p) {
    setPage(p)
    if (filter) {
      getList({ ...filter, offset: p })
    } else {
      getList({ offset: p })
    }
  }

  function checkExpire(list) {
    if (list.length) {
      const arr = []
      list.forEach(e => {
        if (moment().isBefore(e.expireDate)) {
          arr.push(e)
        }
      })
      setList(arr)
    }
  }

  // let int
  // useEffect(() => {
  //   if (int) clearInterval(int)
  //   int = setInterval(() => {
  //     checkExpire(list)
  //   }, 1000)
  // }, [list])
  function rejectOrder(orderId) {
    put(`/api/orders/reject/${orderId}`, { from: "mp" }).finally(() =>
      getList()
    )
  }
  function validateOffer(orderId, agencies) {
    console.log(agencies[orderId])
    var url = "/api/orders/confirm-mp/" + orderId

    put(url, { agence: agencies[String(orderId)] }).finally(() => getList())
  }

  useEffect(() => {
    const newList = [...list]
    newList.forEach(
      e =>
        (e.action =
          e.type == "order" ? (
            <div className="d-flex justify-content-around">
              <button disabled className="btn btn-sm btn-secondary mx-1">
                Valider
              </button>
              <button disabled className="btn btn-sm btn-danger mx-1">
                Rejeter
              </button>
            </div>
          ) : (
            <div className="d-flex justify-content-around">
              <button
                onClick={() => validateOffer(e.id, agencies)}
                className="btn btn-sm btn-success mx-1"
              >
                Valider
              </button>
              <button
                className="btn btn-sm btn-danger mx-1"
                onClick={() => rejectOrder(e.id)}
              >
                Rejeter
              </button>
            </div>
          ))
    )
    setList(newList)
  }, [agencies])

  function getList(filter) {
    setloading(true)
    let url =
      "/api/orders/status/" +
      user.company +
      "?pageSize=10&status[]=11&status[]=12&status[]=13&status[]=14&status[]=700&status[]=10&status[]=9&status[]=8&status[]=7&status[]=6&status[]=5&status[]=4&status[]=3&status[]=2&status[]=1&startDate=" +
      moment(date).format("yyyy-MM-DD") +
      " 00:00:00"
    if (!filter || !filter.offset) {
      url += "&offset=1"
    }
    if (filter) {
      for (let key in filter) {
        if (filter[key] != "All") {
          url += `&${key}=${filter[key]}`
        }
      }
    }

    get(url)
      .then(async res => {
        const { data } = await getAgencies()
        console.log(res)
        settotalSize(res?.data.count)
        settotalEquivalent(res?.data.total_equivalent)
        settotalAmount(res?.data.total_amount)
        setPattern(res?.data.data[0]?.currency.pattern)
        setList(
          res.data.data.map(e => {
            return {
              id_position: e.sessionId,
              domicialiation: e.bank,
              user: e.user.name,
              produit: e.productType,
              currency: e.currency.isoCode,
              billLines: e.bills
                ? e.bills.map(d => d.nbr + "-" + d.bill.label + "/")
                : null,
              // synd: e.syndMember.map(d => d.percentage),
              sens: "Achat",
              agencyChef: e.agence && e.agence.name,
              cheffile: e.leaderBoxInfo && e.leaderBoxInfo.name,
              position: e.sessionId,
              bayers: e.user.company && e.user.company.id,
              purchasename: e.user.company && e.user.company.name,
              amount: (
                <NumericFormat
                  displayType="text"
                  thousandSeparator=" "
                  value={Number(e.amount).toFixed(e.currency.pattern)}
                />
              ),
              iduser: e.user.id,
              user: e.user.username,
              cible: e.negotationRate
                ? Number(e.negotationRate).toFixed(3)
                : "",
              brute: e.rawPrice && Number(e.rawPrice).toFixed(3),
              cvcible: e.negotationRate ? (
                <NumericFormat
                  displayType="text"
                  thousandSeparator=" "
                  value={Number(
                    (e.amount * e.negotationRate) / e.currency?.unit
                  ).toFixed(3)}
                />
              ) : (
                ""
              ),
              price: Number(e.originalRate).toFixed(3),
              contrevalue: (
                <NumericFormat
                  displayType="text"
                  thousandSeparator=" "
                  value={Number(
                    (e.amount * e.originalRate) / e.currency?.unit
                  ).toFixed(3)}
                />
              ),
              agencyChoice: e.leaderBoxInfo.favoriteLocation,
              benefet: "Hbib",
              dateorder: moment(e.createdAt).format("DD-MM-yyyy"),
              region: e.members[0].region,
              zone: e.members[0].zone,
              payment:
                e.cashOutType == "CASH_PAYMENT" ? "Cash" : "Virement bancaire",
              hour: moment(e.createdAt).format("HH:mm"),
              // chef: e.leaderBoxInfo.ex_op.id,
              expireDate: e.expireDate,
              // profitI: e.profit.insightShare,
              // profitP: e.profit.partnerShare,
              // net: e.profit && e.profit.finalPrice,
              // catseller: "A+",
              //range: "A",
              typeposition: "Single position",
              // typeposition:
              //   e.syndMember.length == 1 ? "Single position" : "Syndication",
              //agency: e.leaderBoxInfo.ex_op.box_agency,

              state: e.status && e.status.name,
              Type: e.type == "order" ? "Marché" : "Course",
              agency: e.type != "order" && (
                <Select
                  id="draft-table-select"
                  styles={{ zIndex: 1000, position: "none" }}
                  options={data.map(e => ({
                    label: e.name,
                    value: e.id,
                  }))}
                  onChange={v => {
                    setAgencies({ ...agencies, [String(e.id)]: v.value })
                  }}
                  name="agence"
                ></Select>
              ),
            }
          })
        )
        setloading(false)
      })
      .catch(err => console.log(err))
  }

  useEffect(() => {
    getAgencies()
    getList()
    $(function () {
      $(".wrapper1").scroll(function () {
        $(".wrapper2").scrollLeft($(".wrapper1").scrollLeft())
      })
      $(".wrapper2").scroll(function () {
        $(".wrapper1").scrollLeft($(".wrapper2").scrollLeft())
      })
    })
    document.title = "Bank Notes"
  }, [])

  async function getAgencies() {
    return get(`/api/agence?company=${user.company}`)
  }

  const EcommerceOrderColumns = [
    {
      dataField: "id_position",
      text: "Id order",
      sort: true,
    },
    {
      dataField: "action",
      text: "Action",
      sort: true,
    },
    {
      dataField: "state",
      text: "Statut",
      sort: true,
    },

    {
      dataField: "cheffile",
      text: "Bureau de change",
      sort: true,
    },
    {
      dataField: "currency",
      text: "Devise",
      sort: true,
    },
    {
      dataField: "price",
      text: "Prix",
      sort: true,
    },
    {
      dataField: "cible",
      text: "Prix négocié",
      sort: true,
    },
    {
      dataField: "amount",
      text: "Valeur",
      sort: true,
    },
    {
      dataField: "agencyChef",
      text: "Agence de livraison",
      sort: true,
    },
    {
      dataField: "payment",
      text: "Mode de livraison",
      sort: true,
    },

    {
      dataField: "cut",
      text: "Coupure",
      sort: true,
    },
    {
      dataField: "billLines",
      text: "Coupure détails",
      sort: true,
    },

    {
      dataField: "contrevalue",
      text: "Contre valeur",
      sort: true,
    },
    {
      dataField: "cvcible",
      text: "Contre valeur négociée",
      sort: true,
    },
    {
      dataField: "net",
      text: "Ticket price",
      sort: true,
    },
    {
      dataField: "cvnet",
      text: "Contre valeur ticket",
      sort: true,
    },
    {
      dataField: "dateorder",
      text: "Date ",
      sort: true,
    },
    {
      dataField: "hour",
      text: "Heure ",
      sort: true,
    },
    {
      dataField: "Type",
      text: "Type",
      sort: true,
    },
    {
      dataField: "purchasename",
      text: "Acheteur",
      sort: true,
    },
    {
      dataField: "user",
      text: "Utilisateur acheteur",
      sort: true,
    },
    {
      dataField: "bayers",
      text: "Id acheteur",
      sort: true,
    },

    {
      dataField: "iduser",
      text: "Id utilisateur acheteur",
      sort: true,
    },

    {
      dataField: "typeposition",
      text: "Type position",
      sort: true,
    },

    {
      dataField: "region",
      text: "Gouvernorat",
      sort: true,
    },
    {
      dataField: "zone",
      text: "Zone",
      sort: true,
    },

    // {
    //   dataField: "contract",
    //   text: "Contrat de vente",
    //   sort: true,
    // },
    {
      dataField: "catseller",
      text: "Catégorie vendeur",
      sort: true,
    },
    {
      dataField: "range",
      text: "Catégorie acheteur",
      sort: true,
    },

    {
      dataField: "ticketnumber",
      text: "Numéro tickt",
      sort: true,
    },
    {
      dataField: "ticket",
      text: "Ticket",
      sort: true,
    },
  ]

  return (
    <React.Fragment>
      <Col md="3" className="d-flex">
        <Filter
          regions
          zone
          bankAgency
          handleSubmit={handleFilter}
          filters={[
            {
              label: "Devise",
              type: "select",
              options: currency.map(e => ({ label: e, value: e })),
              name: "currency",
            },
          ]}
        />
      </Col>
      <TableCustom
        cols={EcommerceOrderColumns}
        items={list}
        loading={loading}
      />{" "}
      <div className="d-flex justify-content-end">
        <PaginationCustom
          page={page}
          onPageChange={onPageChange}
          totalSize={totalSize}
        />
      </div>
      <div className="d-flex ">
        <div className="col-lg-11">
          <h4>
            Total contre valeur :{" "}
            <NumericFormat
              displayType="text"
              thousandSeparator=" "
              value={Number(totalEquivalent).toFixed(3)}
            />
            {" TND"}
          </h4>
          {!!totalAmount && (
            <h4>
              Total valeur :{" "}
              <NumericFormat
                displayType="text"
                thousandSeparator=" "
                value={Number(totalAmount).toFixed(pattern)}
              />
              {` ${selectedCurr}`}
            </h4>
          )}
        </div>
      </div>
    </React.Fragment>
  )
}

BillsNotesTable.propTypes = {
  onGetOrders: PropTypes.func,
}

export default withRouter(BillsNotesTable)
