import React, { useEffect, useState } from "react"
import MetaTags from "react-meta-tags"
import PropTypes from "prop-types"
import { Link } from "react-router-dom"
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit"
import { get, post, put, del } from "../../helpers/api_helper"
import BootstrapTable from "react-bootstrap-table-next"
import moment from "moment"
import paginationFactory, {
  PaginationProvider,
  PaginationListStandalone,
  SizePerPageDropdownStandalone,
} from "react-bootstrap-table2-paginator"
import { useSelector, useDispatch } from "react-redux"
import { Card, Col, Container, Row, CardBody, Badge } from "reactstrap"

const Agencies = ({ location }) => {
  const { user } = useSelector(state => {
    return state.user
  })
  const route = location.pathname
  const [list, setList] = useState([])
  useEffect(() => {
    document.title = "Liste agences"
    getList()
  }, [])

  async function remove(id) {
    try {
      await del(`/api/agence/delete/${id}`)

      getList()
    } catch (e) {
      console.log(e)
    }
  }

  function getList() {
    get(`/api/agence?company=${user.company}`)
      .then(res => {
        console.log(res)
        setList(
          res.data.map(e => {
            e.action = (
              <div className="d-flex justify-content-around">
                <button
                  className="btn btn-sm btn-danger"
                  onClick={() => {
                    remove(e.id)
                  }}
                >
                  Supprimer
                </button>
                <Link
                  to={"new/agency/" + e.id}
                  className="btn btn-sm btn-primary"
                >
                  Edit
                </Link>
              </div>
            )
            return e
          })
        )
      })
      .catch(err => console.log(err.response))
  }

  const selectRow = {
    mode: "checkbox",
    hideSelectColumn: true,
  }

  const columns = [
    {
      text: "Reference",
      dataField: "id",
      sort: false,
    },
    {
      text: "Nom",
      dataField: "name",
      sort: true,
    },
    {
      text: "Code BCT",
      dataField: "codeBCT",
      sort: true,
    },
    {
      text: "Region",
      dataField: "state",
      sort: true,
    },
    {
      text: "Action",
      dataField: "action",
      sort: false,
    },
  ]

  const pageOptions = {
    sizePerPhour: 10,
    totalSize: list.length,
    custom: true,
  }

  const defaultSorted = [
    {
      dataField: "reference",
      order: "desc",
    },
  ]
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid={true}>
          <Card>
            <CardBody>
              <Link to="new/agency" className="btn btn-primary mb-3">
                Nouveau
              </Link>

              <PaginationProvider
                pagination={paginationFactory(pageOptions)}
                keyField="id"
                columns={columns}
                data={list}
              >
                {({ paginationProps, paginationTableProps }) => (
                  <ToolkitProvider
                    keyField="id"
                    columns={columns}
                    data={list}
                    search
                  >
                    {toolkitProps => (
                      <React.Fragment>
                        <Row>
                          <Col xl="12">
                            <div className="wrapper2">
                              <div className="mb-2 div2">
                                <BootstrapTable
                                  keyField={"ref"}
                                  responsive
                                  hover
                                  // onTableChange={onTableChange}
                                  // remote={{
                                  //   pagination: true,
                                  //   filter: false,
                                  //   sort: false,
                                  // }}
                                  bordered={true}
                                  striped={true}
                                  defaultSorted={defaultSorted}
                                  selectRow={selectRow}
                                  classes={
                                    list.length
                                      ? "table align-middle table-nowrap purchase-table"
                                      : "d-none"
                                  }
                                  headerWrapperClasses="thead-dark"
                                  {...toolkitProps.baseProps}
                                  {...paginationTableProps}
                                />
                                <div
                                  className={
                                    list.length
                                      ? "d-none"
                                      : "border rounded border-2 text-center"
                                  }
                                  style={{ padding: "20vh" }}
                                >
                                  <h3>Aucune entreprise trouvée</h3>
                                </div>
                              </div>
                            </div>
                          </Col>
                        </Row>

                        <Row className="align-items-md-center mt-30">
                          <Col className="inner-custom-pagination d-flex">
                            <div className="d-inline"></div>
                            <div className="text-md-right ms-auto">
                              <PaginationListStandalone {...paginationProps} />
                            </div>
                          </Col>
                        </Row>
                      </React.Fragment>
                    )}
                  </ToolkitProvider>
                )}
              </PaginationProvider>
            </CardBody>
          </Card>
        </Container>
      </div>
    </React.Fragment>
  )
}

Agencies.propTypes = {
  location: PropTypes.object,
}

export default Agencies
