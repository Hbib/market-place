import React from "react"
import { Redirect } from "react-router-dom"

import Login from "../pages/Authentication/MyLogin1"
import Logout from "../pages/Authentication/Logout"
import Register from "../pages/Authentication/Register"
import ForgetPwd from "../pages/Authentication/ForgetPassword"
import EditPwd from "../pages/Authentication/EditPassword"
import mailSent from "../pages/Authentication/mailSent"
import Draft from "pages/Dashboard/Draft"

import Deal from "pages/Dashboard/Deal"
import ProposalDraft from "pages/proposalDraft/ProposalDraft"
import Market from "pages/Market/Market"
import Ticket from "pages/Ticket/Ticket"
import BillsNotes from "pages/Dashboard/BillsNotes"

// Dashboard
import Home from "../pages/Home"
import Cession from "pages/Market/Cession"
import ConfigIndexMarket from "pages/ConfigIndexMarket"
import ListRaces from "pages/Race/ListRaces"
import newRace from "pages/Race/newRace"
import Book from "pages/Market/Book"
import GenerateTicket from "pages/Dashboard/GenerateTicket"
import CashOutadmin from "pages/Dashboard/CashOut"
import CashOut from "pages/Market/CashOut"
import AuthUsers from "pages/Dashboard/AuthUsers"
import NewAuthregle from "pages/Dashboard/NewAuthregle"
import OnHold from "pages/Ticket/OnHold"
import Users from "pages/Market/Users"
import NewUser from "pages/Market/NewUser"
import OpeningPrice from "pages/Market/OpeningPrice"
import ProfitMarge from "pages/Dashboard/ProfitMarge"
import Agencies from "pages/Market/Agencies"
import NewAgency from "pages/Market/NewAgency"
import MyLogin2 from "pages/Authentication/MyLogin2"
import ErrorPage from "components/Common/ErrorPage"
import changePassword from "pages/Market/changePassword"
const authProtectedRoutes = [
  { path: "/draft", component: Draft },
  { path: "/change-password", component: changePassword },
  { path: "/Delivery", component: Cession },
  { path: "/opening-price", component: OpeningPrice },
  { path: "/users", component: Users },
  // { path: "/users/new", component: NewUser },
  { path: "/users/edit/:id", component: NewUser },
  { path: "/deal", component: Deal },
  { path: "/agencies", component: Agencies },
  { path: "/new/agency", component: NewAgency },
  { path: "/new/agency/:id", component: NewAgency },
  { path: "/home", component: Home },
  // { path: "/Market-sale-validation", component: Market },
  { path: "/ticket", component: Ticket },
  { path: "/bills-notes", component: BillsNotes },
  // { path: "/backoffice-SM", component: BackofficeSM },
  // { path: "/available-equivalent", component: AvailableEquivalent },
  { path: "/confirm", component: ProposalDraft },
  // { path: "/cash-out", component: CashOut },
  { path: "/config", component: ConfigIndexMarket },
  // { path: "/list-race", component: ListRaces },
  // { path: "/new/race", component: newRace },
  { path: "/book", component: Book },
  { path: "/generate-ticket", component: GenerateTicket },
  { path: "/cash-out-admin", component: CashOutadmin },
  { path: "/on-hold", component: OnHold },
  { path: "/auth", component: AuthUsers },
  { path: "/auth/new", component: NewAuthregle },
  { path: "/profit-margin", component: ProfitMarge },

  //======================= CONFIGURATION ============================//

  // this route should be at the end of all other routes
  // eslint-disable-next-line react/display-name
  { path: "/", exact: true, component: () => <Redirect to="/home" /> },
]

const publicRoutes = [
  { path: "/logout", component: Logout },
  { path: "/login", component: MyLogin2 },
  { path: "/error", component: ErrorPage },
  { path: "/forgot-password", component: ForgetPwd },
  { path: "/edit-password", component: EditPwd },
  { path: "/mail-sent", component: mailSent },
  { path: "/register", component: Register },
]

export { publicRoutes, authProtectedRoutes }
