import React, { useEffect, useState } from "react"

import {
  Card,
  Col,
  Container,
  Row,
  Input,
  Label,
  Button,
  CardBody,
  CardTitle,
  Table,
} from "reactstrap"
import { Link } from "react-router-dom"

import $ from "jquery"

const ConfigIndexMarket = () => {
  const route = location.pathname

  useEffect(() => {
    document.title = "Configuration"
  })
  useEffect(() => {
    var readURL = function (input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader()

        reader.onload = function (e) {
          $(".profile-pic").attr("src", e.target.result)
        }

        reader.readAsDataURL(input.files[0])
      }
    }

    $(".file-upload").on("change", function () {
      readURL(this)
    })

    $(".upload-button").on("click", function () {
      $(".file-upload").click()
    })
  }, [])
  const [success_msg, setsuccess_msg] = useState(false)
  const [state, setstate] = useState("edit")
  const [desibled, setdesibled] = useState("true")
  const [show, setShow] = useState(false)
  const [status, setStatus] = useState(2)

  return (
    <React.Fragment>
      <div className="page-content config-page">
        <Container fluid={true} style={{ width: "85%" }}>
          <Row className="mb-3">
            <Col lg={12}>
              <div className=" float-end">
                <label className="mx-2 fs-4">Statut: </label>
                <div className="btn-group">
                  <button
                    onClick={() => setStatus(0)}
                    className={
                      !status
                        ? "btn btn-secondary"
                        : "btn btn-outline-secondary"
                    }
                  >
                    Veille
                  </button>
                  <button
                    onClick={() => setStatus(1)}
                    className={
                      status == 1 ? "btn btn-danger" : "btn btn-outline-danger"
                    }
                  >
                    Off
                  </button>
                  <button
                    onClick={() => setStatus(2)}
                    className={
                      status == 2
                        ? "btn btn-primary"
                        : "btn btn-outline-primary"
                    }
                  >
                    On
                  </button>
                </div>
              </div>
            </Col>
          </Row>
          <Row>
            <Col lg={6}>
              <Row>
                <Col lg={12}>
                  <Card className="home-card border border-primary bg-white">
                    <CardBody>
                      <CardTitle className="mb-4">
                        <h2>Course</h2>
                      </CardTitle>
                      <div className="py-3 hoverable rounded-3">
                        <Link
                          to="/list-race"
                          style={{ color: "inherit", alignItems: "flex-start" }}
                          className="d-flex"
                        >
                          <span
                            className="col-lg-10 "
                            style={{ fontSize: "1rem" }}
                          >
                            Liste courses
                          </span>
                          <i className="fas fa-chevron-right fs-4 col-lg-3" />
                        </Link>{" "}
                      </div>
                      <hr />

                      <div className="py-3 hoverable rounded-3">
                        <Link
                          to="/new/race"
                          style={{ color: "inherit", alignItems: "flex-start" }}
                          className="d-flex"
                        >
                          <span
                            className="col-lg-10 "
                            style={{ fontSize: "1rem" }}
                          >
                            Création course
                          </span>
                          <i className="fas fa-chevron-right fs-4 col-lg-3" />
                        </Link>{" "}
                      </div>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </Col>

            <Col lg={6}>
              <Row>
                <Col lg={12}>
                  <Card className="home-card border border-primary bg-white">
                    <CardBody>
                      <CardTitle className="mb-4">
                        <h2>Position</h2>
                      </CardTitle>
                      <div className="py-3 hoverable rounded-3">
                        <Link
                          to="/market/pmp"
                          style={{ color: "inherit", alignItems: "flex-start" }}
                          className="d-flex"
                        >
                          <span
                            className="col-lg-10 "
                            style={{ fontSize: "1rem" }}
                          >
                            Configuration
                          </span>
                          <i className="fas fa-chevron-right fs-4 col-lg-3" />
                        </Link>{" "}
                      </div>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}

export default ConfigIndexMarket
