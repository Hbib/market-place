import PropTypes from "prop-types"
import MetaTags from "react-meta-tags"
import React, { useEffect, useState } from "react"
import $ from "jquery"

import { Row, Col, CardBody, Card, Alert, Container } from "reactstrap"

//redux
import { useSelector, useDispatch } from "react-redux"

import { withRouter, Link, useHistory } from "react-router-dom"

// availity-reactstrap-validation
import { AvForm, AvField } from "availity-reactstrap-validation"

//Social Media Imports
import { GoogleLogin } from "react-google-login"
// import TwitterLogin from "react-twitter-auth"
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props"

// actions
import { loginUser, apiError, socialLogin, saveUser } from "../../store/actions"

// import images
import profile from "assets/images/profile-img.png"
import logo from "assets/images/logo.svg"
import insightLogo from "assets/images/logo-white.png"
import hiveLogo from "assets/images/exsys_logo.png"

import login1 from "assets/images/login-bg/login-1.jpg"
import login2 from "assets/images/login-bg/login-2.jpg"
import { del, get, post, put } from "../../helpers/api_helper"

//Import config
import { facebook, google } from "../../config"

const Login = props => {
  const dispatch = useDispatch()
  const history = useHistory()

  const [error, setError] = useState("")
  const [loading, setLoading] = useState(false)

  // handleValidSubmit
  const handleValidSubmit = (event, values) => {
    setLoading(true)
    post("/api/login_check", values)
      .then(res => {
        dispatch(saveUser(res.data))
        localStorage.setItem("authToken", res.data.token)
        localStorage.setItem("stayOnline", JSON.stringify(values.stayOnline))
        console.log(res.data)
        history.push("/home")
        setLoading(false)
      })
      .catch(err => {
        console.log(err)
        if (err.response.status == 401) {
          setError("Nom d'utilisateur ou mot de passe incorrecte")
        } else {
          setError("Erreur serveur")
        }
        setLoading(false)
      })

    localStorage.setItem("userEmail", values.email)
  }

  const signIn = (res, type) => {
    if (type === "google" && res) {
      const postData = {
        name: res.profileObj.name,
        email: res.profileObj.email,
        token: res.tokenObj.access_token,
        idToken: res.tokenId,
      }
      dispatch(socialLogin(postData, props.history, type))
    } else if (type === "facebook" && res) {
      const postData = {
        name: res.name,
        email: res.email,
        token: res.accessToken,
        idToken: res.tokenId,
      }
      dispatch(socialLogin(postData, props.history, type))
    }
  }

  useEffect(() => {
    cycleBg()
  }, [])

  function cycleBg() {
    let images = [login1, login2]
    let index = 0
    $(".login-bg").css({
      "background-image": "url('" + images[images.length - 1] + "')",
    })
    setInterval(() => {
      $(".login-bg").fadeOut(500, function () {
        $(".login-bg").css({
          "background-image": "url('" + images[index] + "')",
        })
        $(".login-bg").fadeIn(500, function () {})
        index++
        if (index > images.length - 1) {
          index = 0
        }
      })
    }, 5000)
  }

  //handleGoogleLoginResponse
  const googleResponse = response => {
    signIn(response, "google")
  }

  //handleTwitterLoginResponse
  // const twitterResponse = e => {}

  //handleFacebookLoginResponse
  const facebookResponse = response => {
    signIn(response, "facebook")
  }

  return (
    <React.Fragment>
      <MetaTags>
        <title>Login | Hive+</title>
      </MetaTags>
      <div className="login-bg"></div>
      {/* <div className="login-footer">
        <div style={{ width: "50%" }} className=" d-flex">
          <img src={insightLogo} className="img-fluid col-3" />
          <div className="col-9">
            <h5 className="text-white fs-4" style={{ marginTop: "4rem" }}>
              1ère plateforme SaaS de gestion des opérations de change BBE
            </h5>
          </div>
        </div>
      </div> */}
      <div className="auth-container overflow-hidden">
        <Card
          className="vh-100 rounded-0 mb-0 bg-white home-card"
          style={{ background: "#ffffffc7" }}
        >
          <div>
            <div className="text-primary text-center">
              <img
                className="img-fluid"
                src={hiveLogo}
                style={{
                  width: "40%",
                  marginTop: "2rem",
                }}
              />
              {/* <img
                src={profile}
                alt=""
                className="img-fluid"
                style={{ width: "50%", marginTop: "30%" }}
              /> */}
            </div>
            <div className="text-center" style={{ marginTop: "4rem" }}>
              <h5 className="mb-0">Welcome back!</h5>
              <p className="text-muted ">
                Connectez vous pour continuer vers la platforme Market Place
              </p>
            </div>
          </div>
          <CardBody className="pt-0">
            {/* <div>
              <Link to="/" className="auth-logo-light">
                <div className="avatar-md profile-user-wid mb-4">
                  <span className="avatar-title rounded-circle bg-light">
                    <img
                      src={logo}
                      alt=""
                      className="rounded-circle"
                      height="34"
                    />
                  </span>
                </div>
              </Link>
            </div> */}

            <div className="px-5" style={{ marginTop: "4rem" }}>
              <AvForm
                className="form-horizontal mt-3"
                onValidSubmit={handleValidSubmit}
              >
                {error ? <Alert color="danger">{error}</Alert> : null}

                <div className="mb-3">
                  <AvField
                    name="username"
                    label="Nom utilisateur"
                    className="form-control"
                    placeholder="Nom utilisateur"
                    type="text"
                    required
                  />
                </div>

                <div className="mb-3">
                  <div className="d-flex align-items-start">
                    <div className="flex-grow-1">
                      <label className="form-label">Mot de passe</label>
                    </div>
                    <div className="flex-shrink-0">
                      <div>
                        <Link to="/forgot-password" className="text-muted">
                          <i className="mdi mdi-lock me-1" />
                          Mot de passe oublié?
                        </Link>
                      </div>
                    </div>
                  </div>
                  <AvField
                    name="password"
                    type="password"
                    required
                    placeholder="Mot de passe"
                  />
                </div>

                <AvField
                  type="checkbox"
                  className="form-check-input"
                  name="stayOnline"
                  defaultChecked
                  checked={true}
                  label="Rester connecté"
                />

                <div className="mt-3 d-grid">
                  <button className="btn btn-primary btn-block" type="submit">
                    {loading ? (
                      <div
                        className="spinner-border text-light"
                        style={{ width: "1.5rem", height: "1.5rem" }}
                        role="status"
                      >
                        <span className="sr-only">Loading...</span>
                      </div>
                    ) : (
                      <span>Log In</span>
                    )}
                  </button>
                </div>

                {/* <div className="mt-4 text-center">
                  <Link to="/forgot-password" className="text-muted">
                    <i className="mdi mdi-lock me-1" />
                    Mot de passe oublié?
                  </Link>
                </div> */}
              </AvForm>
            </div>
            <div className=" text-center" style={{ marginTop: "20rem" }}>
              <p>
                ©<i className="mdi mdi-heart text-danger"></i>
                by INSIGHT PLUS
              </p>
            </div>
          </CardBody>
        </Card>
      </div>
    </React.Fragment>
  )
}

export default withRouter(Login)

Login.propTypes = {
  history: PropTypes.object,
}
