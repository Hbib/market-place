import React, { useEffect, useState } from "react"
import { Row, Col, Card, CardBody } from "reactstrap"
import { Link } from "react-router-dom"
import ReactApexChart from "react-apexcharts"
import PropTypes from "prop-types"
import moment from "moment"

function Earning({ data = {}, name }) {
  const options = {
    chart: {
      toolbar: "false",
      dropShadow: {
        enabled: 0,
        color: "#000",
        top: 18,
        left: 7,
        blur: 8,
        opacity: 0.2,
      },
    },
    dataLabels: {
      enabled: !1,
    },
    colors: ["#556ee6"],
    stroke: {
      curve: "straight",
      width: 3,
    },
    xaxis: {
      categories: data.map(e => e.time),
      labels: {
        show: false,
        formatter: value => moment(value).format("HH:mm:SS"),
      },
    },
    yaxis: {
      labels: {
        formatter: value =>
          name == "Position" ? Number(value).toFixed(0) : value,
      },
    },
  }

  const series = [
    {
      name: name,
      data: data.map(e => e.value),
    },
  ]

  return (
    <React.Fragment>
      <Row>
        <Col lg="12">
          <div id="line-chart" dir="ltr">
            <ReactApexChart
              series={series}
              options={options}
              type="line"
              height={260}
              className="apex-charts border bg-white border-primary rounded"
            />
          </div>
        </Col>
      </Row>
    </React.Fragment>
  )
}

Earning.propTypes = {
  data: PropTypes.object,
  name: PropTypes.string,
}

export default Earning
