import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import TableCustom from "components/Common/TableCustom"
import { NumericFormat } from "react-number-format"
import { withRouter, Link } from "react-router-dom"
import { isEmpty } from "lodash"
import BootstrapTable from "react-bootstrap-table-next"
import paginationFactory, {
  PaginationListStandalone,
  PaginationProvider,
} from "react-bootstrap-table2-paginator"
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit"
import Filter from "../../components/Common/Filter"

import { Col, Row, Table } from "reactstrap"
import moment from "moment"

import currency from "../../assets/currency.json"
import $ from "jquery"
import { del, get, post, put } from "../../helpers/api_helper"

//redux

import { useSelector, useDispatch } from "react-redux"
import BackofficeSM from "./backofficeSM"
import { stubTrue } from "lodash"
import PaginationCustom from "components/Common/PaginationCustom"

const CessionTable = props => {
  const [list, setList] = useState([])
  const [loading, setloading] = useState(stubTrue)
  const today = new Date()
  const date = today.setDate(today.getDate())

  const [filter, setfilter] = useState()
  const [page, setPage] = useState(1)
  const [totalSize, settotalSize] = useState(0)
  const [totalEquivalent, settotalEquivalent] = useState(0)
  const [totalAmount, settotalAmount] = useState(0)
  const [selectedCurr, setSelectedCurr] = useState()
  const [pattern, setPattern] = useState(0)
  const { user } = useSelector(state => {
    return state.user
  })

  function handleFilter(form) {
    if (form?.currency && form?.currency != "All") {
      setSelectedCurr(form.currency)
    }
    getList(form)
    setfilter(form)
    setPage(1)
  }

  function onPageChange(p) {
    setPage(p)
    if (filter) {
      getList({ ...filter, offset: p })
    } else {
      getList({ offset: p })
    }
  }

  function getList(filter) {
    setloading(true)
    let url =
      "/api/orders/status/" +
      user.company +
      "?pageSize=10&status[]=13&status[]=14&startDate=" +
      moment(date).format("yyyy-MM-DD") +
      " 00:00:00"
    if (!filter || !filter.offset) {
      url += "&offset=1"
    }
    if (filter) {
      for (let key in filter) {
        if (filter[key] != "All") {
          url += `&${key}=${filter[key]}`
        }
      }
    }
    get(url)
      .then(res => {
        settotalSize(res.data.count)
        settotalEquivalent(res?.data.total_equivalent)
        settotalAmount(res?.data.total_amount)
        setPattern(res?.data.data[0]?.currency.pattern)
        setList(
          res.data.data.map(e => ({
            id_position: e.sessionId,
            domicialiation: e.bank,
            iduser: e.user.id,
            user: e.user.username,
            produit: e.productType,
            currency: e.currency.isoCode,
            sens: "Achat",
            agencyChoice: e.leaderBoxInfo.favoriteLocation,
            position: e.sessionId,
            bayers: e.user.company && e.user.company.id,
            purchasename: e.user.company && e.user.company.name,
            amount: (
              <NumericFormat
                displayType="text"
                thousandSeparator=" "
                value={Number(e.amount).toFixed(e.currency.pattern)}
              />
            ),
            cible: Number(e.negotationRate).toFixed(3) || "",
            // marge_platform:
            //   e.profit && Number(e.profit.platformShareValue).toFixed(3),
            // profitI: e.profit && e.profit.insightSharePercent,
            // profitP: e.profit && e.profit.partnerSharePercent,
            // marge_transaction:
            //   e.profit && Number(e.profit.platformShareValue).toFixed(3),
            net: e.profit && Number(e.profit.price).toFixed(3),
            cvnet: (
              <NumericFormat
                displayType="text"
                thousandSeparator=" "
                value={e.profit && Number(e.profit.equivalent).toFixed(3)}
              />
            ),
            // margeI: e.profit && Number(e.profit.insightShare).toFixed(3),
            // margeP: e.profit && Number(e.profit.partnerShare).toFixed(3),
            region: e.members[0].region,
            zone: e.members[0].zone,
            cvcible: e.negotationRate ? (
              <NumericFormat
                displayType="text"
                thousandSeparator=" "
                value={Number(
                  (e.negotationRate * e.amount) / e.currency?.unit
                ).toFixed(3)}
              />
            ) : null,
            ticketnumber: e.ticket && e.ticket.reference,
            ticket: e.ticket && (
              <a target="_blank" rel="noreferrer" href={e.ticket.file}>
                ticket pdf
              </a>
            ),
            //synd: e.syndMember.map(d => d.percentage),
            rapport: "mmm",
            price: Number(e.originalRate).toFixed(3),
            payment:
              e.cashOutType == "CASH_PAYMENT" ? "Cash" : "Virement bancaire",
            billLines: e.bills
              ? e.bills.map(d => d.nbr + "-" + d.bill.label + "/")
              : null,
            // contract: e.contrat,
            brute: Number(e.rate).toFixed(3),
            cvbrut: Number(e.equivalent).toFixed(3),
            contrevalue: (
              <NumericFormat
                displayType="text"
                thousandSeparator=" "
                value={Number(
                  (e.amount * e.originalRate) / e.currency?.unit
                ).toFixed(3)}
              />
            ),
            agencyChef: e.agence && e.agence.name,
            benefet: e.name,
            dateorder: moment(e.createdAt).format("DD-MM-yyyy"),
            hour: moment(e.createdAt).format("HH:mm"),
            // commissionPer: e.user.company?.commission,
            // commissionHt: (
            //   <NumericFormat
            //     displayType="text"
            //     thousandSeparator=" "
            //     value={Number(e.profit?.commissionValueHT).toFixed(3)}
            //   />
            // ),
            // commissionTtc: (
            //   <NumericFormat
            //     displayType="text"
            //     thousandSeparator=" "
            //     value={Number(e.profit?.commissionValueTTC).toFixed(3)}
            //   />
            // ),
            // chef: e.leaderBoxInfo.ex_op.id,
            valuedate: "12:00",
            // catseller: "A+",
            // range: "A",
            typeposition: "Single position",
            //  e.syndMember.length == 1 ? "Single position" : "Syndication",
            //agency: e.leaderBoxInfo.ex_op.box_agency,

            // domicialiation: "1587",
            state: e.status.name,
            Type: e.type == "order" ? "Marché" : "Course",
            contractB: "contrat",
            contractS: "contrat",
            cheffile: e.leaderBoxInfo.name,
          }))
        )
        setloading(false)
      })
      .catch(err => console.log(err.response))
  }

  useEffect(() => {
    getList()
    $(function () {
      $(".wrapper1").scroll(function () {
        $(".wrapper2").scrollLeft($(".wrapper1").scrollLeft())
      })
      $(".wrapper2").scroll(function () {
        $(".wrapper1").scrollLeft($(".wrapper2").scrollLeft())
      })
    })
    document.title = "Delivery"
  }, [])

  const cols = [
    {
      dataField: "id_position",
      text: "Id order",
      sort: true,
    },
    {
      dataField: "action",
      text: "Action",
      sort: true,
    },
    {
      dataField: "state",
      text: "Statut",
      sort: true,
    },
    {
      dataField: "cheffile",
      text: "Bureau de change",
      sort: true,
    },
    {
      dataField: "currency",
      text: "Devise",
      sort: true,
    },
    {
      dataField: "price",
      text: "Prix",
      sort: true,
    },
    {
      dataField: "cible",
      text: "Prix négocié",
      sort: true,
    },
    {
      dataField: "amount",
      text: "Valeur",
      sort: true,
    },
    {
      dataField: "agencyChef",
      text: "Agence de livraison",
      sort: true,
    },
    {
      dataField: "payment",
      text: "Mode de livraison",
      sort: true,
    },

    {
      dataField: "cut",
      text: "Coupure",
      sort: true,
    },
    {
      dataField: "billLines",
      text: "Coupure détails",
      sort: true,
    },

    {
      dataField: "contrevalue",
      text: "Contre valeur",
      sort: true,
    },
    {
      dataField: "cvcible",
      text: "Contre valeur négociée",
      sort: true,
    },
    {
      dataField: "net",
      text: "Ticket price",
      sort: true,
    },
    {
      dataField: "cvnet",
      text: "Contre valeur ticket",
      sort: true,
    },

    {
      dataField: "dateorder",
      text: "Date ",
      sort: true,
    },
    {
      dataField: "hour",
      text: "Heure ",
      sort: true,
    },
    {
      dataField: "Type",
      text: "Type",
      sort: true,
    },
    {
      dataField: "purchasename",
      text: "Acheteur",
      sort: true,
    },
    {
      dataField: "user",
      text: "Utilisateur acheteur",
      sort: true,
    },
    {
      dataField: "bayers",
      text: "Id acheteur",
      sort: true,
    },

    {
      dataField: "iduser",
      text: "Id utilisateur acheteur",
      sort: true,
    },

    {
      dataField: "typeposition",
      text: "Type position",
      sort: true,
    },

    {
      dataField: "region",
      text: "Gouvernorat",
      sort: true,
    },
    {
      dataField: "zone",
      text: "Zone",
      sort: true,
    },
    // {
    //   dataField: "commissionPer",
    //   text: "Commission INSIGHT %",
    //   sort: true,
    // },

    // {
    //   dataField: "commissionHt",
    //   text: "Commission INSIGHT HT",
    //   sort: true,
    // },

    // {
    //   dataField: "commissionTtc",
    //   text: "Commission INSIGHT TTC",
    //   sort: true,
    // },
    // {
    //   dataField: "contract",
    //   text: "Contrat de vente",
    //   sort: true,
    // },
    {
      dataField: "catseller",
      text: "Catégorie vendeur",
      sort: true,
    },
    {
      dataField: "range",
      text: "Catégorie acheteur",
      sort: true,
    },

    {
      dataField: "ticketnumber",
      text: "Numéro tickt",
      sort: true,
    },
    {
      dataField: "ticket",
      text: "Ticket",
      sort: true,
    },
  ]

  const defaultSorted = [
    {
      dataField: "orderId",
      order: "desc",
    },
  ]

  return (
    <React.Fragment>
      <Col md="3" className="d-flex">
        <Filter
          regions
          zone
          bankAgency
          handleSubmit={handleFilter}
          filters={[
            {
              label: "Devise",
              type: "select",
              options: currency.map(e => ({ label: e, value: e })),
              name: "currency",
            },
          ]}
        />
      </Col>
      <TableCustom cols={cols} items={list} loading={loading} />{" "}
      <div className="d-flex justify-content-end">
        <PaginationCustom
          page={page}
          onPageChange={onPageChange}
          totalSize={totalSize}
        />
      </div>
      <div className="d-flex ">
        <div className="col-lg-11">
          <h4>
            Total contre valeur :{" "}
            <NumericFormat
              displayType="text"
              thousandSeparator=" "
              value={Number(totalEquivalent).toFixed(3)}
            />
            {" TND"}
          </h4>
          {!!totalAmount && (
            <h4>
              Total valeur :{" "}
              <NumericFormat
                displayType="text"
                thousandSeparator=" "
                value={Number(totalAmount).toFixed(pattern)}
              />
              {` ${selectedCurr}`}
            </h4>
          )}
        </div>
      </div>
    </React.Fragment>
  )
}

CessionTable.propTypes = {
  onGetOrders: PropTypes.func,
}

export default withRouter(CessionTable)
