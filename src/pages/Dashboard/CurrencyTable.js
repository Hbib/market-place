import React, { useState } from "react"
import PropTypes from "prop-types"
import { withRouter, Link } from "react-router-dom"
import { isEmpty } from "lodash"
import BootstrapTable from "react-bootstrap-table-next"
import paginationFactory, {
  PaginationProvider,
} from "react-bootstrap-table2-paginator"
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit"

import { Card, CardBody, Col, Row } from "reactstrap"

import currency from "../../assets/currency.json"

//redux
import bank from "assets/bank.json"
import { useSelector, useDispatch } from "react-redux"

const CurrencyTable = props => {
  const rows = bank.map(e => ({
    bank: (
      <span>
        <img
          src={require(`../../assets/images/${e.logo}`).default}
          className="rounded-circle"
          style={{ width: "35px", height: "35px" }}
        />{" "}
        {e.name}
      </span>
    ),
    purchase: 3.2,
    sale: 2.3,
    unit: 10,
  }))

  const selectRow = {
    hideSelectColumn: true,
  }

  const [selectedCurr, setSelectedCurr] = useState("TND")

  //pagination customization
  const pageOptions = {
    sizePerPage: 10,
    totalSize: rows.length, // replace later with size(orders),
    custom: true,
  }
  const { SearchBar } = Search

  const EcommerceOrderColumns = [
    {
      dataField: "bank",
      text: "Banque",
      sort: true,
    },
    {
      dataField: "unit",
      text: "Unité",
      sort: true,
    },
    {
      dataField: "purchase",
      text: "Achat",
      sort: true,
    },
    {
      dataField: "sale",
      text: "Vente",
      sort: true,
    },
  ]

  const defaultSorted = [
    {
      dataField: "orderId",
      order: "desc",
    },
  ]

  return (
    <React.Fragment>
      <Card>
        <div className="clearfix mb-3">
          <div className="float-end">
            <div className="input-group ">
              <label className="input-group-text">Devise</label>
              <select
                value={selectedCurr}
                onInput={v => setSelectedCurr(v.target.value)}
                className="form-select"
              >
                {currency.map(e => (
                  <option key={e}>{e}</option>
                ))}
              </select>
            </div>
          </div>
          <h4>Cours de change banque </h4>
        </div>
        <PaginationProvider
          pagination={paginationFactory(pageOptions)}
          keyField="id"
          columns={EcommerceOrderColumns}
          data={rows}
        >
          {({ paginationProps, paginationTableProps }) => (
            <ToolkitProvider
              keyField="id"
              data={rows}
              columns={EcommerceOrderColumns}
              bootstrap4
              search
            >
              {toolkitProps => (
                <React.Fragment>
                  <Row>
                    <Col xl="12">
                      <div className="table-responsive">
                        <BootstrapTable
                          keyField="id"
                          responsive
                          bordered={true}
                          striped={true}
                          defaultSorted={defaultSorted}
                          selectRow={selectRow}
                          classes={"table align-middle table-nowrap"}
                          headerWrapperClasses={"table-light"}
                          {...toolkitProps.baseProps}
                          {...paginationTableProps}
                        />
                      </div>
                    </Col>
                  </Row>
                </React.Fragment>
              )}
            </ToolkitProvider>
          )}
        </PaginationProvider>
      </Card>
    </React.Fragment>
  )
}

CurrencyTable.propTypes = {
  onGetOrders: PropTypes.func,
}

export default withRouter(CurrencyTable)
