import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { withRouter, Link } from "react-router-dom"
import { NumericFormat } from "react-number-format"
import BootstrapTable from "react-bootstrap-table-next"
import paginationFactory, {
  PaginationListStandalone,
  PaginationProvider,
} from "react-bootstrap-table2-paginator"
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit"
import Filter from "../../components/Common/Filter"
import { Button, Card, CardBody, Col, Row, Badge, Table } from "reactstrap"
import { del, get, post, put } from "../../helpers/api_helper"
import $ from "jquery"
import currency from "assets/currency"
import PaginationCustom from "components/Common/PaginationCustom"
import TableCustom from "components/Common/TableCustom"
//redux

import { useSelector, useDispatch } from "react-redux"
import moment from "moment"

const TicketTable = props => {
  const [list, setList] = useState([])

  const [totalSize, settotalSize] = useState(20)
  const today = new Date()
  const date = today.setDate(today.getDate())

  const [loading, setloading] = useState(false)

  const [filter, setfilter] = useState()
  const [page, setPage] = useState(1)

  const [totalEquivalent, settotalEquivalent] = useState(0)
  const [totalAmount, settotalAmount] = useState(0)
  const [selectedCurr, setSelectedCurr] = useState()
  const [pattern, setPattern] = useState(0)
  const { user } = useSelector(state => {
    return state.user
  })

  function handleFilter(form) {
    if (form?.currency && form?.currency != "All") {
      setSelectedCurr(form.currency)
    }
    getList(form)
    setfilter(form)
    setPage(1)
  }

  function onPageChange(p) {
    setPage(p)
    if (filter) {
      getList({ ...filter, offset: p })
    } else {
      getList({ offset: p })
    }
  }

  function checkExpire(list) {
    if (list.length) {
      const arr = []
      list.forEach(e => {
        if (moment().isBefore(e.expireDate)) {
          arr.push(e)
        }
      })
      setList(arr)
    }
  }

  // let int
  // useEffect(() => {
  //   if (int) clearInterval(int)
  //   int = setInterval(() => {
  //     checkExpire(list)
  //   }, 1000)
  // }, [list])

  function getList(filter) {
    setloading(true)
    let url =
      "/api/orders/status/" +
      user.company +
      "?pageSize=10&status[]=11&status[]=12&status[]=13&status[]=14&status[]=700&startDate=" +
      moment(date).format("yyyy-MM-DD") +
      " 00:00:00"
    if (!filter || !filter.offset) {
      url += "&offset=1"
    }
    if (filter) {
      for (let key in filter) {
        if (filter[key] != "All") {
          url += `&${key}=${filter[key]}`
        }
      }
    }
    get(url)
      .then(res => {
        settotalSize(res.data?.count)
        settotalEquivalent(res?.data?.total_equivalent)
        settotalAmount(res?.data?.total_amount)
        setPattern(res?.data?.data[0]?.currency?.pattern)
        setList(
          res.data.data.map(e => {
            return {
              id_position: e.sessionId,
              payment:
                e.cashOutType == "CASH_PAYMENT" ? "Cash" : "Virement bancaire",
              billLines: e.bills
                ? e.bills.map(d => d.nbr + "-" + d.bill?.label + "/")
                : null,
              expireDate: e.expireDate,
              domicialiation: e.bank,
              iduser: e.user?.id,
              user: e.user?.username,
              produit: e.productType,
              currency: e.currency?.isoCode,
              sens: "Achat",
              position: e.sessionId,
              bayers: e.user?.company?.id,
              purchasename: e.user?.company?.name,
              amount: (
                <NumericFormat
                  displayType="text"
                  thousandSeparator=" "
                  value={Number(e.amount).toFixed(e.currency?.pattern)}
                />
              ),
              cible: e.negotationRate && Number(e.negotationRate).toFixed(3),

              agencyChoice: e.leaderBoxInfo?.favoriteLocation,
              net: e.profit && Number(e.profit?.price).toFixed(3),
              cvnet: (
                <NumericFormat
                  displayType="text"
                  thousandSeparator=" "
                  value={e.profit && Number(e.profit?.equivalent).toFixed(3)}
                />
              ),
              billLines: e.bills
                ? e.bills.map(d => d.nbr + "-" + d.bill.label + "/")
                : null,
              // margeI: e.profit && Number(e.profit.insightShare).toFixed(3),
              // margeP: e.profit && Number(e.profit.partnerShare).toFixed(3),
              cvcible: e.negotationRate ? (
                <NumericFormat
                  displayType="text"
                  thousandSeparator=" "
                  value={Number(
                    (e.negotationRate * e.amount) / e.currency?.unit
                  ).toFixed(3)}
                />
              ) : (
                ""
              ),
              ticketnumber: e.ticket?.reference,
              ticket: e.ticket && (
                <a
                  target="_blank"
                  rel="noreferrer"
                  href={e.ticket?.file}
                  className="btn btn-primary btn-sm"
                >
                  <i className="fas fa-arrow-up-right-from-square" /> Voir pdf
                </a>
              ),
              price: Number(e.originalRate).toFixed(3),
              // contract: e.contrat,
              //synd: e.syndMember.map(d => d.percentage),
              brute: Number(e.rate).toFixed(3),
              cvbrut: Number(e.equivalent).toFixed(3),
              contrevalue: (
                <NumericFormat
                  displayType="text"
                  thousandSeparator=" "
                  value={Number(
                    (e.amount * e.originalRate) / e.currency?.unit
                  ).toFixed(3)}
                />
              ),
              agencyChef: e.agence?.name,
              benefet: e.name,
              dateorder: moment(e.createdAt).format("DD-MM-yyyy"),
              hour: moment(e.createdAt).format("HH:mm"),
              cheffile: e.leaderBoxInfo?.name,
              // valuedate: "12:00",
              // catseller: "A+",
              // range: "A",
              typeposition: "Single position",
              // commissionPer: e.user.company?.commission,
              // commissionHt: (
              //   <NumericFormat
              //     displayType="text"
              //     thousandSeparator=" "
              //     value={Number(e.profit?.commissionValueHT).toFixed(3)}
              //   />
              // ),
              // commissionTtc: (
              //   <NumericFormat
              //     displayType="text"
              //     thousandSeparator=" "
              //     value={Number(e.profit?.commissionValueTTC).toFixed(3)}
              //   />
              // ),
              //  e.syndMember.length == 1 ? "Single position" : "Syndication",
              // agency: e.leaderBoxInfo.ex_op.box_agency,

              // // domicialiation: "1587",
              state: e.status?.name,
              region: e.members[0]?.region,
              zone: e.members[0]?.zone,
              Type: e.type == "order" ? "Marché" : "Course",
              // contractB: "contrat",
              // contractS: "contrat",
              // cheffile: e.leaderBoxInfo.id,
            }
          })
        )
        setloading(false)
      })
      .catch(err => console.log(err.response))
  }

  useEffect(() => {
    getList()
    $(function () {
      $(".wrapper1").scroll(function () {
        $(".wrapper2").scrollLeft($(".wrapper1").scrollLeft())
      })
      $(".wrapper2").scroll(function () {
        $(".wrapper1").scrollLeft($(".wrapper2").scrollLeft())
      })
    })
    document.title = "Ticket"
  }, [])

  const cols = [
    {
      dataField: "id_position",
      text: "Id order",
      sort: true,
    },
    {
      dataField: "action",
      text: "Action",
      sort: true,
    },
    {
      dataField: "state",
      text: "Statut",
      sort: true,
    },

    {
      dataField: "cheffile",
      text: "Bureau de change",
      sort: true,
    },
    {
      dataField: "currency",
      text: "Devise",
      sort: true,
    },
    {
      dataField: "price",
      text: "Prix",
      sort: true,
    },
    {
      dataField: "cible",
      text: "Prix négocié",
      sort: true,
    },
    {
      dataField: "amount",
      text: "Valeur",
      sort: true,
    },
    {
      dataField: "agencyChef",
      text: "Agence de livraison",
      sort: true,
    },
    {
      dataField: "payment",
      text: "Mode de livraison",
      sort: true,
    },
    {
      dataField: "cut",
      text: "Coupure",
      sort: true,
    },
    {
      dataField: "billLines",
      text: "Coupure détails",
      sort: true,
    },

    {
      dataField: "contrevalue",
      text: "Contre valeur",
      sort: true,
    },
    {
      dataField: "cvcible",
      text: "Contre valeur négociée",
      sort: true,
    },
    {
      dataField: "net",
      text: "Ticket price",
      sort: true,
    },
    {
      dataField: "cvnet",
      text: "Contre valeur ticket",
      sort: true,
    },
    {
      dataField: "ticketnumber",
      text: "Numéro tickt",
      sort: true,
    },
    {
      dataField: "ticket",
      text: "Ticket",
      sort: true,
    },
    {
      dataField: "dateorder",
      text: "Date ",
      sort: true,
    },
    {
      dataField: "hour",
      text: "Heure ",
      sort: true,
    },
    {
      dataField: "Type",
      text: "Type",
      sort: true,
    },
    {
      dataField: "purchasename",
      text: "Acheteur",
      sort: true,
    },
    {
      dataField: "user",
      text: "Utilisateur acheteur",
      sort: true,
    },
    {
      dataField: "bayers",
      text: "Id acheteur",
      sort: true,
    },

    {
      dataField: "iduser",
      text: "Id utilisateur acheteur",
      sort: true,
    },

    {
      dataField: "typeposition",
      text: "Type position",
      sort: true,
    },

    {
      dataField: "region",
      text: "Gouvernorat",
      sort: true,
    },
    {
      dataField: "zone",
      text: "Zone",
      sort: true,
    },

    // {
    //   dataField: "contract",
    //   text: "Contrat de vente",
    //   sort: true,
    // },
    {
      dataField: "catseller",
      text: "Catégorie vendeur",
      sort: true,
    },
    {
      dataField: "range",
      text: "Catégorie acheteur",
      sort: true,
    },

   
  ]

  return (
    <React.Fragment>
      <Col md="3" className="d-flex">
        <Filter
          regions
          zone
          bankAgency
          handleSubmit={handleFilter}
          filters={[
            {
              label: "Devise",
              type: "select",
              options: currency.map(e => ({ label: e, value: e })),
              name: "currency",
            },
          ]}
        />
      </Col>
      <TableCustom cols={cols} items={list} />{" "}
      <div className="d-flex justify-content-end">
        <PaginationCustom
          page={page}
          onPageChange={onPageChange}
          totalSize={totalSize}
        />
      </div>
      <div className="d-flex ">
        <div className="col-lg-11">
          <h4>
            Total contre valeur :{" "}
            <NumericFormat
              displayType="text"
              thousandSeparator=" "
              value={Number(totalEquivalent).toFixed(3)}
            />
            {" TND"}
          </h4>
          {!!totalAmount && (
            <h4>
              Total valeur :{" "}
              <NumericFormat
                displayType="text"
                thousandSeparator=" "
                value={Number(totalAmount).toFixed(pattern)}
              />
              {` ${selectedCurr}`}
            </h4>
          )}
        </div>
      </div>
    </React.Fragment>
  )
}

TicketTable.propTypes = {
  onGetOrders: PropTypes.func,
}

export default withRouter(TicketTable)
