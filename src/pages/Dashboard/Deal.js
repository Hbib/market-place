import React, { Component, useState } from "react"
import MetaTags from "react-meta-tags"
import SalesAnalytics from "./sales-analytics"
import { Link } from "react-router-dom"

import avatar1 from "../../assets/images/users/avatar-1.jpg"
import profileImg from "../../assets/images/profile-img.png"
import ReactApexChart from "react-apexcharts"
import Earning from "./earning"
import Sector from "./Sector"
import Office from "./Office"
import DealTable from "./DealTable"
import { Breadcrumbs, Typography } from "@mui/material"
import ProposalDraftTable from "../proposalDraft/ProposalDraftTable"
import WalletBalance from "./wallet-balance"
import currency from "../../assets/currency"
import { Container, Row, Col } from "reactstrap"

var options = {
  chart: { sparkline: { enabled: !0 } },
  dataLabels: { enabled: !1 },
  colors: ["#556ee6"],
  plotOptions: {
    radialBar: {
      hollow: { margin: 0, size: "60%" },
      track: { margin: 0 },
      dataLabels: { show: !1 },
    },
  },
}

class Deal extends Component {
  constructor(props) {
    super(props)

    this.state = {
      options: {
        plotOptions: {
          radialBar: {
            startAngle: -135,
            endAngle: 135,
            dataLabels: {
              name: {
                fontSize: "13px",
                color: void 0,
                offsetY: 60,
              },
              value: {
                offsetY: 22,
                fontSize: "16px",
                color: void 0,
                formatter: function (e) {
                  return e + "%"
                },
              },
            },
          },
        },
        colors: ["#556ee6"],
        fill: {
          type: "gradient",
          gradient: {
            shade: "dark",
            shadeIntensity: 0.15,
            inverseColors: !1,
            opacityFrom: 1,
            opacityTo: 1,
            stops: [0, 50, 65, 91],
          },
        },
        stroke: {
          dashArray: 4,
        },
        labels: ["Secteur"],
      },
      series: [67],
    }
  }

  render() {
    return (
      <React.Fragment>
        <div className="page-content">
          <Container fluid>
            <Row>
              <Col lg={12}>
                <DealTable />
              </Col>
            </Row>
          </Container>
        </div>
      </React.Fragment>
    )
  }
}

export default Deal
