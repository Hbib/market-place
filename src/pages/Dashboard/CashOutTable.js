import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { withRouter, Link } from "react-router-dom"
import { useSelector, useDispatch } from "react-redux"
import moment from "moment"

import BootstrapTable from "react-bootstrap-table-next"
import paginationFactory, {
  PaginationListStandalone,
  PaginationProvider,
} from "react-bootstrap-table2-paginator"
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit"
import currency from "assets/currency"
import { Col, Row } from "reactstrap"

import Filter from "../../components/Common/Filter"
import { del, get, post, put } from "../../helpers/api_helper"

import $ from "jquery"
import PaginationCustom from "components/Common/PaginationCustom"
import TableCustom from "components/Common/TableCustom"
//redux

const CashOutTable = props => {
  const [list, setList] = useState([])
  const [page, setpage] = useState(1)
  const [totalSize, settotalSize] = useState(20)
  const [filter, setfilter] = useState()
  const [loading, setloading] = useState(false)
  const [listTotals, setListTotals] = useState([])

  const { user } = useSelector(state => {
    return state.user
  })

  function handleFilter(form) {
    getList(form)
    getTotals(form)
    setfilter(form)
    setPage(1)
  }

  function onPageChange(p) {
    setpage(p)
    getList(filter)
  }

  function getTotals(filter) {
    let url = "/api/orders/by-district/" + user.company + "?status[]=10"
    if (filter) {
      for (let key in filter) {
        url += `&${key}=${filter[key]}`
      }
    }

    get(url)
      .then(res => {
        setListTotals(res.data)
        console.log(res, "totals")
      })
      .catch(err => console.log(err.response))
  }

  function getList(filter) {
    setloading(true)
    let url =
      "/api/orders/filter/" +
      user.id +
      "?status[]=10&pageSize=10&offset=" +
      page
    let startDate, endDate
    if (filter) {
      for (let key in filter) {
        if (filter[key]) {
          url += `&${key}=${filter[key]}`
        }
      }
    }
    get(url)
      .then(res => {
        console.log(res)
        settotalSize(res.data.count)
        if (filter && filter.agency) {
          res.data = res.data.filter(
            e => e.syndMember[0].ex_op.box_agency == filter.agency
          )
        }
        if (filter && filter.chef) {
          res.data = res.data.filter(
            e => e.syndMember[0].ex_op.district == filter.chef
          )
        }
        let arr = res.data.data.map(e => ({
          id: e.ref,
          domicialiation: e.bank,
          iduser: e.user.id,
          user: e.user.username,
          produit: e.productType,
          currency: e.currency.isoCode,
          sens: "Achat",
          position: e.sessionId,
          bayers: e.user.company && e.user.company.id,
          purchasename: e.user.company && e.user.company.name,
          amount: Number(e.amount).toFixed(e.currency.pattern),
          cible: Number(e.negotationRate).toFixed(3) || "",
          marge_platform: e.profit && Number(e.profit.platformShare).toFixed(3),
          synd: e.syndMember.map(d => d.percentage),
          profitI: e.profit && e.profit.insightSharePercent,
          profitP: e.profit && e.profit.partnerSharePercent,
          marge_transaction:
            e.profit && Number(e.profit.platformShareValue).toFixed(3),
          net: e.profit && Number(e.profit.finalPrice).toFixed(3),
          cvnet: e.profit && Number(e.profit.finalEquivelent).toFixed(3),
          cvbrut: Number(e.equivalent).toFixed(3),
          margeI: e.profit && Number(e.profit.insightShare).toFixed(3),
          margeP: e.profit && Number(e.profit.partnerShare).toFixed(3),
          cvcible: e.negotationRate
            ? Number((e.negotationRate * e.amount) / e.currency?.unit).toFixed(
                3
              )
            : null,
          ticketnumber: e.ticket && e.ticket.id,
          ticket: e.ticket && (
            <a
              target="_blank"
              rel="noreferrer"
              href={e.ticket.file}
              className="btn btn-primary"
            >
              <i className="fas fa-arrow-up-right-from-square" /> Voir pdf
            </a>
          ),
          price: Number(e.originalRate).toFixed(3),
          contract: e.contrat,
          brute: Number(e.rate).toFixed(3),
          cvbrute: Number(e.equivalent).toFixed(3),
          contrevalue: Number(
            (e.amount * e.originalRate) / e.currency?.unit
          ).toFixed(3),
          agencyChef: e.leaderBoxInfo.ex_op && e.leaderBoxInfo.ex_op.district,

          benefet: e.name,
          dateorder: moment(e.createdAt).format("L"),
          hour: moment(e.createdAt).format("LT"),
          chef: e.leaderBoxInfo.ex_op.id,
          valuedate: "12:00",
          catseller: "A+",
          range: "A",
          typeposition:
            e.syndMember.length == 1 ? "Single position" : "Syndication",
          agency: e.leaderBoxInfo.ex_op.box_agency,

          state: e.status.name,
          Type: e.type == "order" ? "Marché" : "Course",
          contractB: "contrat",
          contractS: "contrat",
          cheffile: e.leaderBoxInfo.id,
        }))
        // if (startDate && endDate) {
        //   arr = arr.filter(
        //     e =>
        //       new Date(e.dateorder).getDate() >=
        //         new Date(startDate).getDate() &&
        //       new Date(e.dateorder).getDate() <= new Date(endDate).getDate()
        //   )
        // } else if (startDate) {
        //   arr = arr.filter(
        //     e =>
        //       new Date(e.dateorder).getDate() >= new Date(startDate).getDate()
        //   )
        // } else if (endDate) {
        //   arr = arr.filter(
        //     e => new Date(e.dateorder).getDate() <= new Date(endDate).getDate()
        //   )
        // }
        // if (filter && filter.currency) {
        //   arr = arr.filter(e => e.currency == filter.currency)
        // }
        // if (filter && filter.ref) {
        //   arr = arr.filter(e => e.id == filter.ref)
        // }

        setList(arr)
        setloading(false)
      })
      .catch(err => console.log(err.response))
  }

  useEffect(() => {
    getList()
    getTotals()
    $(function () {
      $(".wrapper1").scroll(function () {
        $(".wrapper2").scrollLeft($(".wrapper1").scrollLeft())
      })
      $(".wrapper2").scroll(function () {
        $(".wrapper1").scrollLeft($(".wrapper2").scrollLeft())
      })
    })
    document.title = "Cash Out"
  }, [])

  const cols = [
    {
      dataField: "id",
      text: "ID order",
      sort: true,
    },
    {
      dataField: "Type",
      text: "Type",
      sort: true,
    },
    {
      dataField: "produit",
      text: "Produit",
      sort: true,
    },
    {
      dataField: "action",
      text: "Action",
      sort: true,
    },
    {
      dataField: "state",
      text: "State",
      sort: true,
    },
    {
      dataField: "user",
      text: "Utilisateur",
      sort: true,
    },
    {
      dataField: "currency",
      text: "Devise",
      sort: true,
    },
    {
      dataField: "sens",
      text: "Sens",
      sort: true,
    },
    {
      dataField: "position",
      text: "Id position",
      sort: true,
    },

    {
      dataField: "synd",
      text: "Exposition",
      sort: true,
    },
    {
      dataField: "typeposition",
      text: "Type position",
      sort: true,
    },
    {
      dataField: "chef",
      text: "Chef lieu",
      sort: true,
    },

    {
      dataField: "cheffile",
      text: "Chef file",
      sort: true,
    },
    {
      dataField: "bayers",
      text: "ID acheteur",
      sort: true,
    },
    {
      dataField: "purchasename",
      text: "Nom acheterur",
      sort: true,
    },
    {
      dataField: "amount",
      text: "Montant",
      sort: true,
    },
    {
      dataField: "price",
      text: "Deal price",
      sort: true,
    },
    {
      dataField: "cible",
      text: "Prix négociation",
      sort: true,
    },
    {
      dataField: "cvbrut",
      text: "Prix brut",
      sort: true,
    },
    {
      dataField: "profitI",
      text: "Profit partagé Insight",
      sort: true,
    },
    {
      dataField: "profitP",
      text: "Profit partagé partenaire",
      sort: true,
    },
    {
      dataField: "margeI",
      text: "Marge sur transaction Insight",
      sort: true,
    },
    {
      dataField: "margeP",
      text: "Marge sur transaction Partenaire",
      sort: true,
    },
    {
      dataField: "net",
      text: "Prix net",
      sort: true,
    },

    {
      dataField: "contrevalue",
      text: "Contre valeur",
      sort: true,
    },
    {
      dataField: "cvcible",
      text: "Contre valeur négociation",
      sort: true,
    },
    {
      dataField: "cvbrute",
      text: "Contre valeur brute",
      sort: true,
    },
    {
      dataField: "cvnet",
      text: "Contre valeur net",
      sort: true,
    },
    {
      dataField: "domicialiation",
      text: "Domiciliation",
      sort: true,
    },
    {
      dataField: "agency",
      text: "Agence",
      sort: true,
    },
    {
      dataField: "dateorder",
      text: "Date ",
      sort: true,
    },
    {
      dataField: "hour",
      text: "Heure ",
      sort: true,
    },
    {
      dataField: "ticketnumber",
      text: "Numéro ticket",
      sort: true,
    },
    {
      dataField: "ticket",
      text: "Ticket",
      sort: true,
    },
    {
      dataField: "catseller",
      text: "Catégorie vendeur",
      sort: true,
    },
    {
      dataField: "range",
      text: "Catégorie acheteur",
      sort: true,
    },
  ]

  return (
    <React.Fragment>
      <Col md="3" className="d-flex">
        <Filter
          handleSubmit={handleFilter}
          agency
          chef
          filters={[
            {
              label: "Date début",
              type: "date",
              name: "startDate",
            },
            {
              label: "Date fin",
              type: "date",
              name: "endDate",
            },
            {
              label: "Devise",
              type: "select",
              options: currency.map(e => ({ label: e, value: e })),
              name: "currency",
            },
            {
              label: "Référence",
              type: "text",
              name: "ref",
            },
          ]}
        />{" "}
        <a
          className="btn btn-primary mx-2"
          target="_blank"
          rel="noreferrer"
          href={`${process.env.REACT_APP_APIKEY}/api/orders/exportOrder/${user.id}?status[]=10`}
        >
          <i className="fas fa-file-excel" /> Excel
        </a>
      </Col>
      <TableCustom cols={cols} items={list} loading={loading} />

      <div className="d-flex justify-content-end">
        <PaginationCustom
          page={page}
          onPageChange={onPageChange}
          totalSize={totalSize}
        />
      </div>
      <Row>
        <Col lg={6}>
          <h3>Total CV par chef lieu</h3>
          <table className="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Code agence</th>
                <th>Chef lieu</th>
                <th>Total contre valeur</th>
              </tr>
            </thead>
            <tbody>
              {Object.keys(listTotals).map(e => (
                <tr key={e}>
                  <td>{e}</td>
                  <td></td>
                  <td>{listTotals[e]}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </Col>
      </Row>
    </React.Fragment>
  )
}

CashOutTable.propTypes = {
  onGetOrders: PropTypes.func,
}

export default withRouter(CashOutTable)
