import React, { useState, useEffect } from "react"
import { Link } from "react-router-dom"
// import currency from "assets/currency"
import Earning from "./Dashboard/earning"
import { del, get, post, put } from "../helpers/api_helper"
import { FullScreen, useFullScreenHandle } from "react-full-screen"
import Select from "react-select"
import { NumericFormat } from "react-number-format"
import moment from "moment"
// import { Tooltip } from "react-tooltip"
import CountUp from "react-countup"
import { Controller, useForm } from "react-hook-form"
import {
  Container,
  Row,
  Col,
  table,
  thead,
  Button,
  Card,
  CardBody,
  CardTitle,
  Input,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Spinner,
  Alert,
  InputGroup,
  InputGroupText,
  Collapse,
} from "reactstrap"
import { AvForm, AvField } from "availity-reactstrap-validation"
import { useSelector, useDispatch } from "react-redux"

const Home = () => {
  const {
    register,
    formState: { errors },
    handleSubmit,
    watch,
    control,
    setValue,
    trigger,
  } = useForm()
  const { user } = useSelector(state => {
    return state.user
  })
  const [filtertype, setFiltertype] = useState("market")
  const [list, setList] = useState([])
  const [district, setDistrict] = useState([])
  const [agency, setagency] = useState()
  const [orderErr, setaOrderErr] = useState("")
  const [agencies, setAgencies] = useState([])
  const [loading, setLoading] = useState(true)
  const [loadingOrder, setloadingOrder] = useState(false)
  const [err, setErr] = useState("")
  const [selectedCurr, setSelectedCurr] = useState("EUR")
  const [open, setOpen] = useState(false)
  const [selectedOrder, setselectedOrder] = useState(null)
  const [targetRate, settargetRate] = useState(null)
  const [modal_xlarge, setmodal_xlarge] = useState(false)
  const [amount, setamount] = useState(null)
  const [filter, setFilter] = useState()
  const [loadedData, setloadedData] = useState(null)
  const [ordererr, setordererr] = useState()
  const [racecount, setracecount] = useState("")
  const [poscount, setposcount] = useState("")
  const [isExpanded, setisExpanded] = useState(false)
  const [sort, setSort] = useState("date-desc")
  const [ticket_avg, setticket_avg] = useState()
  const [total_sum_deal, settotal_sum_deal] = useState()
  const [total_count_deals, settotal_count_deals] = useState()
  const [currency, setcurrency] = useState([])

  const [total_position, settotal_position] = useState()
  const [total_positionHistory, settotal_positionHistory] = useState(0)
  const [total_ticketHistory, settotal_ticketHistory] = useState(0)
  const [total_purchase, settotal_purchase] = useState(0)
  const [total_purchaseHistory, settotal_purchaseHistory] = useState(0)
  const [total_count_deals_history, settotal_count_deals_history] = useState(0)

  const [total_sum_deals_History, settotal_sum_deals_History] = useState(0)
  const [total_saleHistory, settotal_saleHistory] = useState(0)
  const [total_sale, settotal_sale] = useState()
  const [total_sum_deals, settotal_sum_deals] = useState()
  const [positions_chart, setpositions_chart] = useState([])
  const [ticket_avg_chart, setticket_avg_chart] = useState([])
  const [regions, setregions] = useState([])
  const [counter, setCounter] = useState(0)
  const [intervalId, setIntervalId] = useState()

  const [KPIcollapse, setKPIcollapse] = useState(false)
  const handle = useFullScreenHandle()

  async function getAgencies() {
    const { data } = await get(`/api/agence?company=${user.company}`)
    setAgencies(data)
  }

  async function getracecount() {
    try {
      const { data } = await get(
        `/api/main-screen/course-data?currency=${selectedCurr}&isOpen=1&expired=0`
      )
      setracecount(data.kpi.count)
    } catch (e) {}
  }

  useEffect(() => {
    console.log(errors)
  }, [errors])

  async function getposcount() {
    try {
      const { data } = await get(
        `/api/main-screen/offer-data?currency.isoCode=${selectedCurr}`
      )
      setposcount(data.data.kpi.count)
    } catch (e) {}
  }

  async function getCurrency() {
    const { data } = await get("/api/currency")
    setcurrency(data)
  }

  useEffect(() => {
    // getracecount()
    // getposcount()
    getCurrency()
    getAgencies()
  }, [])

  function fetchList() {
    setLoading(true)
    let url =
      filtertype == "market"
        ? `/api/main-screen/market-data?currency=${selectedCurr}&bank=${user.company}`
        : `/api/main-screen/course-data?currency=${selectedCurr}&isOpen=1&expired=0`

    if (filter) {
      url += `&members.exOp.region=${filter}`
    }
    get(url)
      .then(res => {
        let data = filtertype == "market" ? res.data.data.data : res.data.data

        setloadedData(filtertype == "market" ? res.data.data : res.data)
        setticket_avg(res.data.data?.kpi?.ticket_avg)
        settotal_sum_deal(res.data.data?.kpi?.total_sum_deal)
        settotal_count_deals(res.data.data?.kpi?.total_count_deals)
        settotal_position(Number(res.data.data?.kpi?.total_position).toFixed(0))

        settotal_purchase(Number(res.data.data?.kpi?.total_purchase).toFixed(0))
        settotal_sale(Number(res.data.data?.kpi?.total_sale).toFixed(0))
        settotal_sum_deals(res.data.data?.kpi?.total_sum_deals)
        setpositions_chart(res.data.data?.charts?.position)
        setticket_avg_chart(res.data.data?.charts?.ticket_avg)

        if (filter && filter != "all") {
          data = data.filter(e => e.members[0].ex_op?.region?.id == filter)
        }

        if (sort != "date-desc") {
          sortList(data)
        } else {
          setList(data)
        }
        setErr("")
        setLoading(false)
      })
      .catch(err => {
        setErr("Veuillez patienter svp, une erreur système s'est produite")
        console.log(err)
      })
  }

  async function getRegions() {
    const { data } = await get("/api/hive/hive-zone")
    setregions(data)
  }

  useEffect(() => {
    // getBankList()
    document.onkeydown = function (evt) {
      evt = evt || window.event
      var isEscape = false
      if ("key" in evt) {
        isEscape = evt.key === "Escape" || evt.key === "Esc"
      } else {
        isEscape = evt.keyCode === 27
      }
      if (isEscape) {
        setmodal_xlarge(false)
      }
    }
    getRegions()
    document.title = "MainScreen"
  }, [])

  useEffect(() => {
    const eventSource = new EventSource(
      `${process.env.REACT_APP_MERCURE}/.well-known/mercure?topic=/market-pipe&topic=/market/kpi&topic=/clock`,
      { withCredentials: true }
    )

    eventSource.onmessage = event => {
      const { data, subject } = JSON.parse(event.data)

      if (subject == "/market-pipe") {
        console.log(selectedCurr)
        if (selectedCurr == data?.context?.currency?.iso_code) {
          if (data?.action?.toUpperCase() == "ADD") {
            if (list.filter(e => e.id == data?.data?.id)[0]) {
              return
            }
            setList(list => [data?.data, ...list])
          } else {
            setList(list => {
              let newList = []
              list.forEach(e => {
                if (e.id != data?.data?.id) {
                  newList.push(e)
                }
              })
              return newList
            })
          }
        }
      } else if (subject == "/market/kpi") {
        settotal_purchaseHistory(total_purchase)
        settotal_saleHistory(total_sale)
        settotal_positionHistory(total_position)
        settotal_ticketHistory(ticket_avg)
        settotal_sum_deals_History(total_sum_deals)
        settotal_count_deals_history(total_count_deals)
        if (selectedCurr == data?.context?.currency?.iso_code) {
          setticket_avg(data?.ticket_avg)
          settotal_sum_deal(data?.total_sum_deal)
          settotal_count_deals(data?.total_count_deals)
          settotal_position(data?.total_position)

          settotal_purchase(data?.total_purchase)
          settotal_sale(data?.total_sale)
          settotal_sum_deals(data?.total_sum_deals)
        }
      } else if (subject == "/clock") {
        setticket_avg_chart(old => [
          ...old,
          { time: data?.time, value: ticket_avg },
        ])
        setpositions_chart(old => [
          ...old,
          { time: data?.time, value: total_position },
        ])
      }
    }

    return () => {
      eventSource.close()
    }
  }, [
    selectedCurr,
    total_purchase,
    total_sale,
    total_position,
    ticket_avg,
    total_sum_deals,
    total_count_deals,
  ])

  useEffect(() => {
    fetchList()
    clearInterval(intervalId)
    setIntervalId(
      setInterval(() => {
        fetchList()
      }, 600000)
    )
  }, [selectedCurr, filtertype])

  function sortList(list) {
    if (list.length) {
      if (sort == "date-desc") {
        fetchList()
      } else if (sort == "date-asc") {
        setList([
          ...list.sort(
            (a, b) =>
              new Date(a.created_at).getTime() -
              new Date(b.created_at).getTime()
          ),
        ])
      } else if (sort == "rate-desc") {
        setList([...list.sort((a, b) => b.rate - a.rate)])
      } else if (sort == "rate-asc") {
        setList([...list.sort((a, b) => a.rate - b.rate)])
      } else if (sort == "amount-desc") {
        setList([...list.sort((a, b) => b.amount - a.amount)])
      } else {
        setList([...list.sort((a, b) => a.amount - b.amount)])
      }
    }
  }

  async function getAgency() {
    setloadingOrder(true)
    setordererr("")
    try {
      const { data } = await get(
        `/api/box_agence/get/by_company?company=${user.company}&exOp=${selectedOrder.members[0].ex_op.uuid}`
      )
      if (!data.length) {
        setordererr("Aucune agence disponible")
        setloadingOrder(false)
        return
      }

      setagency(data[0])
      setloadingOrder(false)
      // try {
      //   const { data } = await get(
      //     `/api/orders/check-session/${selectedOrder.id}`
      //   )
      //   if (!data.is_free) setordererr("Position n'est plus libre")
      //   setloadingOrder(false)
      // } catch (e) {
      //   setordererr(
      //     "Une erreur s'est produite, réessayer ou contacter l'administration"
      //   )
      //   setloadingOrder(false)
      // }
    } catch (e) {
      setordererr(
        "Une erreur s'est produite, réessayer ou contacter l'administration"
      )
      setloadingOrder(false)
    }
  }

  useEffect(() => {
    trigger("targetRate")
  }, [targetRate])

  useEffect(() => {
    trigger("amount")
  }, [amount])

  async function createOrder(v) {
    try {
      if (
        Number(amount) > Number(selectedOrder.amount) ||
        !agency ||
        (v.targetRate && Number(v.targetRate) > Number(selectedOrder.rate))
      ) {
        console.log("here")
        return
      }
      setloadingOrder(true)
      // selectedOrder.members.forEach(e => {
      //   if (e.admin == true) {
      //     e.ex_op.box_agency = agency.id || "1"
      //     e.ex_op.district = district.id || "1"
      //   }
      // })
      const obj = {
        currency: selectedCurr,
        product_type: "bbe to compte",
        type: "order",
        amount: Number(v.amount),
        rate: selectedOrder.rate,
        negotation_rate: v.targetRate ? Number(v.targetRate) : null,
        bank: v.bank || 1,
        user: user.id,
        synd_member: selectedOrder.members,
        session_id: selectedOrder.id,
        agence: agency,
        cashOutType: v.cashOutType,
      }
      await post("/api/orders/add", obj)
      setmodal_xlarge(false)
      setloadingOrder(false)
    } catch (err) {
      if (err.response?.status == 400) {
        setordererr("Position non disponible.")
        fetchList()
      } else {
        setordererr("Une erreur systeme c'est produite.")
      }
      setloadingOrder(false)
    }
  }

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <button className="btn btn-primary mb-3">valider</button>
          <div className="d-flex justify-content-between">
            <div className="d-flex">
              <div className="btn-group" style={{ height: 38.64 }} role="group">
                <button
                  type="button"
                  className={
                    filtertype == "market"
                      ? "btn btn-primary"
                      : "btn btn-outline-primary"
                  }
                  onClick={() => setFiltertype("market")}
                >
                  Marché
                </button>
                {/* <Tooltip
                  style={{
                    width: 90,
                    height: 20,
                    fontSize: 11,
                    padding: 0,
                    paddingLeft: 5,
                  }}
                  anchorId="coming-soon"
                  content="Available soon"
                  place="top"
                  isOpen={true}
                  variant="error"
                /> */}
                <button
                  id="coming-soon"
                  type="button"
                  disabled
                  className={
                    filtertype == "race"
                      ? "btn btn-primary"
                      : "btn btn-outline-primary"
                  }
                  // onClick={() => setFiltertype("race")}
                >
                  Course
                </button>
              </div>
              <div className="input-group mb-4 mx-3" style={{ width: 150 }}>
                <label className="input-group-text">Devise</label>
                <select
                  onChange={e => setSelectedCurr(e.currentTarget.value)}
                  className="form-select "
                >
                  {currency.map((j, k) => {
                    if (j.isoCode != "TND")
                      return <option key={k}>{j.isoCode}</option>
                  })}
                </select>
              </div>
            </div>
            <div className="d-flex">
              {/*  <div
                title="Nombre total de positions"
                className="mt-2 pt-1 mx-2 d-flex text-primary"
              >
                <span
                  id="position-count"
                  className=" fw-bolder"
                  style={{ fontSize: "20px", marginRight: "12px" }}
                >
                  {poscount}
                </span>
                <img
                  id="money"
                  style={{ width: 23, height: 23 }}
                  src={require("../assets/money.png").default}
                />
              </div>
              <div
                title="Nombre total de courses"
                className="mt-2 pt-1 mx-4 d-flex text-primary"
              >
                <span
                  id="race-count"
                  className="fw-bolder"
                  style={{ fontSize: "20px", marginRight: "12px" }}
                >
                  {racecount}
                </span>
                <img
                  id="running-man"
                  style={{ width: 23, height: 23, marginTop: 2 }}
                  src={require("../assets/running.png").default}
                />
              </div> */}
            </div>
          </div>
          <div className="mb-2" style={{ overflow: "auto" }}>
            <h3 className="text-nowrap mb-3">Vue globale</h3>
            <table className="table table-bordered op-table">
              <thead>
                <tr>
                  <th></th>
                  {currency
                    .filter(
                      e =>
                        e.isoCode != "TND" &&
                        e.isoCode != "CNY" &&
                        e.isoCode != "LYD"
                    )
                    .map(e => (
                      <th key={e.isoCode}>
                        <img
                          src={
                            require("../assets/flags/" + e.isoCode + ".png")
                              .default
                          }
                          style={{ width: 20 }}
                          alt={e.isoCode}
                        />{" "}
                        {e.isoCode}
                      </th>
                    ))}
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <b>Nb des positions</b>
                  </td>
                  {currency
                    .filter(
                      e =>
                        e.isoCode != "TND" &&
                        e.isoCode != "CNY" &&
                        e.isoCode != "LYD"
                    )
                    .map(e => (
                      <td key={e.isoCode}>
                        <NumericFormat
                          displayType="text"
                          thousandSeparator=" "
                          className="text-nowrap"
                          value={loadedData?.positions[e.isoCode]?.count || 0}
                        />
                      </td>
                    ))}
                </tr>
                <tr>
                  <td>
                    <b>Valeur</b>
                  </td>
                  {currency
                    .filter(
                      e =>
                        e.isoCode != "TND" &&
                        e.isoCode != "CNY" &&
                        e.isoCode != "LYD"
                    )
                    .map(e => (
                      <td key={e.isoCode}>
                        <NumericFormat
                          displayType="text"
                          thousandSeparator=" "
                          className="text-nowrap"
                          value={
                            loadedData?.positions[e.isoCode]?.position || 0
                          }
                        />
                      </td>
                    ))}
                </tr>

                {/* <tr>
                  <td>
                    <b>Contre valeur</b>
                  </td>
                  {currency
                    .filter(
                      e =>
                        e.isoCode != "TND" &&
                        e.isoCode != "CNY" &&
                        e.isoCode != "LYD"
                    )
                    .map(e => (
                      <td key={e.isoCode}>
                        <NumericFormat
                          displayType="text"
                          thousandSeparator=" "
                          className="text-nowrap"
                          value={loadedData?.positions[e.isoCode]?.count || 0}
                        />
                      </td>
                    ))}
                </tr> */}
              </tbody>
            </table>
          </div>
          {/* <div className="d-flex mb-2">
            <h3 className="text-nowrap">KPI générale</h3>{" "}
            <div
              title={KPIcollapse ? "Fermer" : "Ouvrir"}
              onClick={() => setKPIcollapse(!KPIcollapse)}
              style={{
                border: "2px solid",
                borderRadius: "50%",
                width: 30,
                height: 30,
                cursor: "pointer",
              }}
              className="border-primary bg-white d-flex justify-content-center align-items-center text-center mx-2 text-primary"
            >
              <i
                className={
                  KPIcollapse ? "fas fa-chevron-up" : "fas fa-chevron-down"
                }
              />
            </div>
          </div> */}

          <Collapse isOpen={KPIcollapse}>
            <Row>
              <Col lg={4}>
                <Card
                  className="mb-4 p-1 border border-primary rounded home-card bg-white text-center"
                  style={{ background: "white !important", height: "75%" }}
                >
                  <h4>Positions</h4>
                  <b style={{ fontSize: 20, letterSpacing: 2 }}>
                    <CountUp
                      className="account-balance"
                      start={Number(total_positionHistory).toFixed(0)}
                      end={Number(total_position).toFixed(0)}
                      duration={1}
                      useEasing={true}
                      useGrouping={true}
                      separator=" "
                    />
                  </b>
                </Card>
              </Col>
              {/* <Col lg={3}>
                <Card
                  className="mb-4 p-1 border border-primary rounded home-card bg-white text-center"
                  style={{ background: "white !important", height: "75%" }}
                >
                  <h4>Ticket moyen</h4>
                  <b style={{ fontSize: 20, letterSpacing: 2 }}>
                    <CountUp
                      className="account-balance"
                      start={0}
                      end={Number(ticket_avg)}
                      duration={1}
                      useEasing={true}
                      useGrouping={true}
                      separator=" "
                      decimals={3}
                      decimal=","
                    />
                  </b>
                </Card>
              </Col> */}
              <Col lg={4}>
                <Card
                  className="mb-4 p-1 border border-primary rounded home-card bg-white text-center"
                  style={{ background: "white !important", height: "75%" }}
                >
                  <h4>Total achat</h4>
                  <b style={{ fontSize: 20, letterSpacing: 2 }}>
                    <CountUp
                      className="account-balance"
                      start={Number(total_purchaseHistory).toFixed(0)}
                      end={Number(total_purchase).toFixed(0)}
                      duration={1}
                      useEasing={true}
                      useGrouping={true}
                      separator=" "
                    />
                  </b>
                </Card>
              </Col>
              <Col lg={4}>
                <Card
                  className="mb-4 p-1 border border-primary rounded home-card bg-white text-center"
                  style={{ background: "white !important", height: "75%" }}
                >
                  <h4>Totale vente</h4>
                  <b style={{ fontSize: 20, letterSpacing: 2 }}>
                    <CountUp
                      className="account-balance"
                      start={Number(total_saleHistory).toFixed(0)}
                      end={Number(total_sale).toFixed(0)}
                      duration={1}
                      useEasing={true}
                      useGrouping={true}
                      separator=" "
                    />
                  </b>
                </Card>
              </Col>
            </Row>
            <h3>Volume deals</h3>
            <Row>
              <Col lg={4}>
                <Card
                  className="mb-4 p-1 border border-primary rounded home-card bg-white text-center"
                  style={{ background: "white !important", height: "75%" }}
                >
                  <h4>Ticket moyen</h4>
                  <b style={{ fontSize: 20, letterSpacing: 2 }}>
                    <CountUp
                      className="account-balance"
                      start={0}
                      end={Number(ticket_avg)}
                      duration={1}
                      useEasing={true}
                      useGrouping={true}
                      separator=" "
                      decimals={3}
                      decimal=","
                    />
                  </b>
                </Card>
              </Col>
              <Col lg={4}>
                <Card
                  className="mb-4 p-1 border border-primary rounded home-card bg-white text-center"
                  style={{ background: "white !important", height: "75%" }}
                >
                  <h4>Total volume</h4>
                  <b style={{ fontSize: 20, letterSpacing: 2 }}>
                    <CountUp
                      className="account-balance"
                      start={Number(total_sum_deals_History).toFixed(0)}
                      end={Number(total_sum_deals).toFixed(0)}
                      duration={1}
                      useEasing={true}
                      useGrouping={true}
                      separator=" "
                    />
                  </b>
                </Card>
              </Col>
              <Col lg={4}>
                <Card
                  className="mb-4 p-1 border border-primary rounded home-card bg-white text-center"
                  style={{ background: "white !important", height: "75%" }}
                >
                  <h4>Total nombre</h4>
                  <b style={{ fontSize: 20, letterSpacing: 2 }}>
                    <CountUp
                      className="account-balance"
                      start={Number(total_count_deals_history)}
                      end={Number(total_count_deals)}
                      duration={1}
                      useEasing={true}
                      useGrouping={true}
                      separator=" "
                    />
                  </b>
                </Card>
              </Col>
            </Row>
            {/* <Row>
                    <Col lg="4" cols="12" className="text-center">
                      <h4>Total</h4>
                      <b style={{ fontSize: 20, letterSpacing: 2 }}>
                        {/* {} 
                        <CountUp
                          className="account-balance"
                          start={Number(total_sum_deals_History).toFixed(0)}
                          end={Number(total_sum_deals).toFixed(0)}
                          duration={1}
                          useEasing={true}
                          useGrouping={true}
                          separator=" "
                        />
                      </b>
                    </Col>
                    <Col
                      lg="4"
                      style={{
                        borderLeft: "1px solid gray",
                        borderRight: "1px solid gray",
                      }}
                      cols="12"
                      className="text-center"
                    >
                      <h4>{user.companyName}</h4>
                      <b style={{ fontSize: 20, letterSpacing: 2 }}>
                        <CountUp
                          className="account-balance"
                          start={0}
                          end={Number(loadedData?.kpi?.bank_sum_deals).toFixed(
                            0
                          )}
                          duration={1}
                          useEasing={true}
                          useGrouping={true}
                          separator=" "
                        />
                      </b>
                    </Col>
                    <Col lg="4" cols="12" className="text-center">
                      <h4>Part du marché</h4>
                      <b style={{ fontSize: 20, letterSpacing: 2 }}>
                        <CountUp
                          className="account-balance"
                          start={0}
                          end={Number(
                            loadedData && loadedData.kpi?.bank_sum_percent
                          ).toFixed(2)}
                          duration={1}
                          useEasing={true}
                          useGrouping={true}
                          separator=" "
                          decimals={2}
                          decimal=","
                        />
                        %
                      </b>
                    </Col>
                  </Row>
                  {/* <hr />

                <ReactApexChart
                  width={"100%"}
                  height={300}
                  className="d-flex justify-content-center"
                  options={{
                    labels: ["STB", "Totale"],
                    chart: {
                      type: "donut",
                    },
                    plotOptions: {
                      pie: {
                        startAngle: -90,
                        endAngle: 90,
                        offsetY: 10,
                      },
                    },
                    responsive: [
                      {
                        breakpoint: 480,
                        options: {
                          chart: {
                            width: 200,
                          },
                          legend: {
                            position: "bottom",
                          },
                        },
                      },
                    ],
                  }}
                  series={[30, 70]}
                  type="donut"
                /> 
                </Card>
              </Col>
              <Col lg={6}>
                <h3>Nombre deals</h3>
                <Card
                  className="mb-4 p-2 border border-primary rounded home-card bg-white"
                  style={{ background: "white !important" }}
                >
                  <Row>
                    <Col lg="4" cols="12" className="text-center">
                      <h4>Total</h4>
                      <b style={{ fontSize: 20, letterSpacing: 2 }}>
                        <CountUp
                          className="account-balance"
                          start={Number(total_count_deals_history)}
                          end={Number(total_count_deals)}
                          duration={1}
                          useEasing={true}
                          useGrouping={true}
                          separator=" "
                        />
                      </b>
                    </Col>
                    <Col
                      style={{
                        borderLeft: "1px solid gray",
                        borderRight: "1px solid gray",
                      }}
                      lg="4"
                      cols="12"
                      className="text-center"
                    >
                      <h4>{user.companyName}</h4>
                      <b style={{ fontSize: 20, letterSpacing: 2 }}>
                        <CountUp
                          className="account-balance"
                          start={0}
                          end={loadedData?.kpi?.bank_count_deals}
                          duration={1}
                          useEasing={true}
                          useGrouping={true}
                          separator=" "
                        />
                      </b>
                    </Col>
                    <Col lg="4" cols="12" className="text-center">
                      <h4>Part du marché</h4>
                      <b style={{ fontSize: 20, letterSpacing: 2 }}>
                        <CountUp
                          className="account-balance"
                          start={0}
                          end={Number(
                            loadedData && loadedData.kpi?.bank_count_percent
                          ).toFixed(2)}
                          duration={1}
                          useEasing={true}
                          useGrouping={true}
                          separator=" "
                          decimals={2}
                          decimal=","
                        />
                        %
                      </b>
                    </Col>
                  </Row> */}
          </Collapse>

          <Row>
            <Col lg={6} style={{ paddingRight: "1.2rem" }}>
              <h3>Tendance</h3>
              {filtertype == "market" ? (
                <div>
                  <h4 className="mt-3">
                    Position <small>({selectedCurr})</small>
                  </h4>
                  <Earning name={"Position"} data={positions_chart} />
                </div>
              ) : (
                <div>
                  <h4 className="mt-3">
                    Volume des demandes <small>({selectedCurr})</small>
                  </h4>
                  <Earning
                    name="Course"
                    data={loadedData && loadedData.charts.amount}
                  />
                </div>
              )}

              {filtertype == "market" ? (
                <div>
                  <h4 className="mt-3">
                    Ticket moyen <small>({selectedCurr})</small>
                  </h4>
                  <Earning name="Ticket moyen" data={ticket_avg_chart} />
                </div>
              ) : (
                <div>
                  <h4 className="mt-3">
                    Prix de course <small>({selectedCurr})</small>
                  </h4>
                  <Earning
                    name="Prix"
                    data={loadedData && loadedData.charts.rate}
                  />
                </div>
              )}
            </Col>

            <Col lg={6} style={{ paddingLeft: "1.2rem" }}>
              <h3>PIPE</h3>
              <button
                onClick={() => {
                  setisExpanded(!isExpanded)
                }}
                className="btn btn-sm btn-primary text-nowrap mb-2"
              >
                <i className="fas fa-expand" /> Agrandir
              </button>
              <div className="row mb-3">
                <Col lg="12" xl="6">
                  <InputGroup
                    size="sm"
                    className="mb-2"
                    style={{ height: "32px", flexWrap: "nowrap" }}
                  >
                    <InputGroupText>Trier par</InputGroupText>
                    <select
                      onChange={e => setSort(e.currentTarget.value)}
                      value={sort}
                      className="form-select-sm"
                    >
                      <option value="date-desc">Le plus recent</option>
                      <option value="date-asc">Le plus ancien</option>
                      <option value="rate-desc">Prix décroissant</option>
                      <option value="rate-asc">Prix croissant</option>
                      <option value="amount-desc">Valeur décroissante</option>
                      <option value="amount-asc">Valeur croissante</option>
                    </select>
                    <button
                      onClick={() => sortList(list)}
                      className="btn btn-sm btn-primary input-group-button"
                    >
                      Valider
                    </button>
                  </InputGroup>
                </Col>
                <Col lg="12" xl="6">
                  <InputGroup
                    size="sm"
                    style={{ height: "32px", flexWrap: "nowrap" }}
                  >
                    <InputGroupText>Filtrer par</InputGroupText>
                    <select
                      onChange={e => setFilter(e.currentTarget.value)}
                      className="form-select-sm"
                      value={filter}
                    >
                      <option value="all">Tout</option>
                      {regions.map(e => (
                        <option value={e.id} key={e.id}>
                          {e.name}
                        </option>
                      ))}
                    </select>
                    <button
                      onClick={() => fetchList()}
                      className="btn btn-sm btn-primary input-group-button"
                    >
                      Valider
                    </button>
                  </InputGroup>
                </Col>
              </div>
              {filtertype == "market" ? (
                <div
                  className="conteneur"
                  style={{
                    width: "100%",
                  }}
                >
                  <table
                    className={
                      handle.active
                        ? "table-home table table-striped expanded"
                        : "table-home table table-striped"
                    }
                  >
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Valeur</th>
                        <th>Prix</th>
                        <th>Gouvernorat</th>
                        <th>Zone cible</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    {loading || err ? (
                      <div
                        style={{
                          height: "523px",
                          width: "100%",
                          justifyContent: "center",
                          alignItems: "center",
                        }}
                        className="d-flex"
                      >
                        {loading && !err ? (
                          <Spinner
                            style={{
                              height: "3rem",
                              width: "3rem",
                              borderWidth: "5px",
                            }}
                            color="primary"
                          >
                            Loading...
                          </Spinner>
                        ) : (
                          <div>{err}</div>
                        )}
                      </div>
                    ) : (
                      <tbody
                        style={{
                          height: 523,
                        }}
                        id="pointsTable"
                      >
                        {list.map((e, i) => (
                          <tr key={i}>
                            <td>{e.id}</td>
                            <td>
                              <NumericFormat
                                displayType="text"
                                thousandSeparator=" "
                                value={Number(e.amount).toFixed(
                                  e.currency.pattern
                                )}
                              />
                            </td>
                            <td>{Number(e.rate).toFixed(4)}</td>
                            <td>{e.members[0].ex_op?.region?.name}</td>
                            <td>{e.members[0].ex_op?.geo_zone?.name}</td>
                            <td>
                              {user.roles[0] != "ROLE_ROOT" && (
                                <button
                                  onClick={() => {
                                    if (
                                      filtertype == "race" ||
                                      e.status == "ENGAGED"
                                    ) {
                                      return
                                    }
                                    setselectedOrder(e)
                                    setordererr("")
                                    setmodal_xlarge(true)
                                  }}
                                  className={
                                    e.status == "ENGAGED"
                                      ? "btn btn-danger"
                                      : "btn btn-success"
                                  }
                                  style={
                                    e.status == "ENGAGED"
                                      ? { height: 35 }
                                      : { height: 35, background: "green" }
                                  }
                                >
                                  {e.status == "ENGAGED" ? "Engaged" : "Go"}
                                </button>
                              )}
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    )}
                  </table>
                </div>
              ) : (
                <div
                  className="conteneur"
                  style={isExpanded ? { position: "absolute" } : {}}
                >
                  <table className="table-home table table-striped">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Valeur</th>
                        <th>Prix</th>
                        <th>Date d'expiration</th>
                        <th>Pourcentage closing</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    {loading || err ? (
                      <div
                        style={{
                          height: "523px",
                          width: "100%",
                          justifyContent: "center",
                          alignItems: "center",
                        }}
                        className="d-flex"
                      >
                        {loading && !err ? (
                          <Spinner
                            style={{
                              height: "3rem",
                              width: "3rem",
                              borderWidth: "5px",
                            }}
                            color="primary"
                          >
                            Loading...
                          </Spinner>
                        ) : (
                          <div>{err}</div>
                        )}
                      </div>
                    ) : (
                      <tbody id="pointsTable" className="rounded">
                        {list.map((e, i) => (
                          <tr key={i}>
                            <td>{e.id}</td>
                            <td>
                              {Number(e.amount).toFixed(e.currency.pattern)}
                            </td>
                            <td>{Number(e.rate).toFixed(3)}</td>
                            <td>{e.members[0].ex_op?.region?.name}</td>
                            <td>
                              <button
                                disabled
                                className="btn btn-primary"
                                style={{ height: 35 }}
                              >
                                Go
                              </button>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    )}
                  </table>
                </div>
              )}

              <Modal size="xl" isOpen={isExpanded}>
                <div className="d-flex justify-content-between mb-3">
                  <h4 className="mt-2">PIPE</h4>
                  <i
                    onClick={() => setisExpanded(false)}
                    className="btn pt-0 pb-0 bx bx-x-circle fs-2"
                  />
                </div>
                <ModalBody>
                  <div className="d-flex mb-2">
                    <InputGroup size="sm" style={{ height: "32px" }}>
                      <InputGroupText>Trier par</InputGroupText>
                      <select
                        onChange={e => setSort(e.currentTarget.value)}
                        value={sort}
                        className="form-select-sm"
                      >
                        <option value="date-desc">Le plus recent</option>
                        <option value="date-asc">Le plus ancien</option>
                        <option value="rate-desc">Prix décroissant</option>
                        <option value="rate-asc">Prix croissant</option>
                        <option value="amount-desc">Valeur décroissante</option>
                        <option value="amount-asc">Valeur croissante</option>
                      </select>
                      <button
                        onClick={() => sortList(list)}
                        className="btn btn-sm btn-primary input-group-button"
                      >
                        Valider
                      </button>
                    </InputGroup>
                    <InputGroup size="sm" style={{ height: "32px" }}>
                      <InputGroupText>Filtrer par</InputGroupText>
                      <select
                        onChange={e => setFilter(e.currentTarget.value)}
                        className="form-select-sm"
                        value={filter}
                      >
                        <option value="all">Tout</option>
                        {regions.map(e => (
                          <option value={e.id} key={e.id}>
                            {e.name}
                          </option>
                        ))}
                      </select>
                      <button
                        onClick={() => fetchList(list)}
                        className="btn btn-sm btn-primary input-group-button"
                      >
                        Valider
                      </button>
                    </InputGroup>
                  </div>
                  {filtertype == "market" ? (
                    <div
                      className="conteneur"
                      style={{
                        width: "100%",
                      }}
                    >
                      <table className="table-home expanded table table-striped">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Valeur</th>
                            <th>Prix</th>
                            <th>Gouvernorat</th>
                            <th>Zone cible</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        {loading || err ? (
                          <div
                            style={{
                              height: 700,
                              width: "100%",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                            className="d-flex"
                          >
                            {loading && !err ? (
                              <Spinner
                                style={{
                                  height: "3rem",
                                  width: "3rem",
                                  borderWidth: "5px",
                                }}
                                color="primary"
                              >
                                Loading...
                              </Spinner>
                            ) : (
                              <div>{err}</div>
                            )}
                          </div>
                        ) : (
                          <tbody id="pointsTable" style={{ height: 700 }}>
                            {list.map((e, i) => (
                              <tr key={i}>
                                <td>{e.id}</td>
                                <td>
                                  {Number(e.amount).toFixed(e.currency.pattern)}
                                </td>
                                <td>
                                  {Number(e.rate).toFixed(e.currency.pattern)}
                                </td>
                                <td>{e.members[0].ex_op?.region?.name}</td>
                                <td>{e.members[0].ex_op?.geo_zone?.name}</td>
                                <td>
                                  {user.roles[0] != "ROLE_ROOT" && (
                                    <button
                                      onClick={() => {
                                        if (
                                          filtertype == "race" ||
                                          e.status == "ENGAGED"
                                        ) {
                                          return
                                        }
                                        setselectedOrder(e)
                                        setordererr("")
                                        setmodal_xlarge(true)
                                      }}
                                      className={
                                        e.status == "ENGAGED"
                                          ? "btn btn-danger"
                                          : "btn btn-success"
                                      }
                                      style={
                                        e.status == "ENGAGED"
                                          ? { height: 35 }
                                          : { height: 35, background: "green" }
                                      }
                                    >
                                      {e.status == "ENGAGED" ? "Engaged" : "Go"}
                                    </button>
                                  )}
                                </td>
                              </tr>
                            ))}
                          </tbody>
                        )}
                      </table>
                    </div>
                  ) : (
                    <div
                      className="conteneur"
                      style={isExpanded ? { position: "absolute" } : {}}
                    >
                      <table className="table-home table table-striped">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Valeur</th>
                            <th>Prix</th>
                            <th>Gouvernorat</th>
                            <th>Zone cible</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        {loading || err ? (
                          <div
                            style={{
                              height: "523px",
                              width: "100%",
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                            className="d-flex"
                          >
                            {loading && !err ? (
                              <Spinner
                                style={{
                                  height: "3rem",
                                  width: "3rem",
                                  borderWidth: "5px",
                                }}
                                color="primary"
                              >
                                Loading...
                              </Spinner>
                            ) : (
                              <div>{err}</div>
                            )}
                          </div>
                        ) : (
                          <tbody id="pointsTable" className="rounded">
                            {list.map((e, i) => (
                              <tr key={i}>
                                <td>{e.id}</td>
                                <td>
                                  {Number(e.amount).toFixed(e.currency.pattern)}
                                </td>
                                <td>{Number(e.rate).toFixed(3)}</td>
                                <td>{moment(e.expireDate).format("L")}</td>
                                <td>{Number(e.coursePercent).toFixed(3)}</td>
                                <td></td>
                                <td>
                                  <button
                                    disabled
                                    className="btn btn-primary"
                                    style={{ height: 35 }}
                                  >
                                    Go
                                  </button>
                                </td>
                              </tr>
                            ))}
                          </tbody>
                        )}
                      </table>
                    </div>
                  )}
                </ModalBody>
              </Modal>
            </Col>
          </Row>
          <Modal onClosed={() => setamount(0)} size="lg" isOpen={modal_xlarge}>
            <div className="d-flex justify-content-between mb-3">
              <h4 className="mt-2">Nouveau ordre</h4>
              <i
                onClick={() => setmodal_xlarge(false)}
                className="btn pt-0 pb-0 bx bx-x-circle fs-2"
              />
            </div>
            <ModalBody>
              {!!orderErr && <Alert color="danger">{orderErr}</Alert>}
              {loadingOrder ? (
                <div
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    height: 500,
                  }}
                  className="d-flex"
                >
                  <Spinner
                    style={{
                      height: "3rem",
                      width: "3rem",
                      borderWidth: "5px",
                    }}
                    color="primary"
                  >
                    Loading...
                  </Spinner>
                </div>
              ) : ordererr ? (
                <div
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    height: 500,
                  }}
                  className="d-flex"
                >
                  <h4>
                    <i className="fas fa-exclamation-circle text-danger" />{" "}
                    {ordererr}
                  </h4>
                </div>
              ) : (
                selectedOrder && (
                  <form onSubmit={handleSubmit(createOrder)}>
                    <Row>
                      <h4>
                        Valeur de la position:{" "}
                        <b>
                          <NumericFormat
                            displayType="text"
                            thousandSeparator=" "
                            value={selectedOrder?.amount}
                          />
                        </b>
                      </h4>
                      <Col lg={6}>
                        <div className="mb-2">
                          <label>Devise</label>
                          <select
                            disabled
                            className="form-select"
                            value={selectedCurr}
                          >
                            {currency.map(e =>
                              e.isoCode != "TND" ? (
                                <option value={e.isoCode} key={e.isoCode}>
                                  {e.isoCode}
                                </option>
                              ) : null
                            )}
                          </select>
                        </div>

                        <div className="mb-2">
                          <label>Montant</label>
                          <input
                            {...register("amount", {
                              required: true,
                              pattern: /^[0-9]*$/g,
                            })}
                            className={
                              errors?.amount || selectedOrder.amount < amount
                                ? "form-control is-invalid"
                                : "form-control"
                            }
                            onInput={e => {
                              setamount(e.currentTarget.value)
                            }}
                          />
                          {(errors.amount || selectedOrder.amount < amount) && (
                            <div className="invalid-feedback">
                              Champ invalide
                            </div>
                          )}
                        </div>
                        <div className="mb-2">
                          <label>Prix</label>
                          <input
                            value={selectedOrder && selectedOrder.rate}
                            disabled
                            className="form-control"
                          />
                        </div>
                        <div className="mb-2">
                          <label>Prix négocié</label>
                          <input
                            {...register("targetRate", {
                              required: false,
                              pattern: /^[0-9]*$/g,
                            })}
                            className={
                              targetRate > selectedOrder?.rate
                                ? "form-control is-invalid"
                                : "form-control"
                            }
                            onInput={e => {
                              settargetRate(e.currentTarget.value)
                            }}
                          />
                          {(errors.targetRate ||
                            targetRate > selectedOrder?.rate) && (
                            <div className="invalid-feedback">
                              Champ invalide
                            </div>
                          )}
                        </div>
                        <div className="mb-2">
                          <label>Contre valeur</label>
                          <Input
                            disabled
                            name="counterValue"
                            value={
                              selectedOrder &&
                              Number(
                                (amount * selectedOrder.rate) /
                                  currency.filter(
                                    e => e.isoCode == selectedCurr
                                  )[0].unit
                              ).toFixed(3)
                            }
                            className="form-control"
                          />
                        </div>
                        <div className="mb-2">
                          <label>Contre valeur négocié</label>
                          <Input
                            value={Number(
                              (amount * targetRate) /
                                currency.filter(
                                  e => e.isoCode == selectedCurr
                                )[0].unit
                            ).toFixed(3)}
                            disabled
                            name="targetCounterValue"
                            className="form-control"
                          />
                        </div>
                      </Col>
                      <Col lg={6}>
                        <div className="mb-2">
                          <label>Gouvernorat</label>
                          <Input
                            type="text"
                            disabled
                            className="form-control"
                            name="region"
                            value={
                              selectedOrder &&
                              selectedOrder.members[0].ex_op?.region?.name
                            }
                          />
                        </div>
                        <div className="mb-2">
                          <label>Zone cible</label>
                          <Input
                            type="text"
                            disabled
                            className="form-control"
                            name="zone"
                            value={
                              selectedOrder &&
                              selectedOrder.members[0].ex_op?.geo_zone?.name
                            }
                          />
                        </div>
                        <div className="mb-2">
                          <label>Agence de livraison</label>
                          <Select
                            options={agencies.map(e => ({
                              label: `${e.codeBCT} ${e.name}`,
                              value: e.id,
                            }))}
                            required
                            onChange={e => setagency(e.value)}
                            className="is-invalid"
                            name="agence"
                            styles={{
                              control: (baseStyles, state) => {
                                return {
                                  ...baseStyles,
                                  borderColor: state.hasValue
                                    ? "grey"
                                    : "#f46a6a",
                                }
                              },
                            }}
                          ></Select>
                          {!agency && (
                            <div className="invalid-feedback">
                              Champ obligatoire
                            </div>
                          )}
                        </div>

                        <div className="mb-2">
                          <label>Mode de paiement</label>
                          <select
                            type="select"
                            name="cashOutType"
                            className={
                              errors?.cashOutType
                                ? "form-select is-invalid"
                                : "form-select"
                            }
                            {...register("cashOutType", { required: true })}
                          >
                            <option></option>
                            <option value="CASH_PAYMENT">Cash</option>
                            <option value="BANK_TRANSFER_PAYMENT">
                              Virement
                            </option>
                          </select>
                        </div>
                      </Col>
                    </Row>
                    <div className="mt-3 float-end">
                      <button type="submit" className="btn btn-primary">
                        Valider
                      </button>
                    </div>
                  </form>
                )
              )}
            </ModalBody>
          </Modal>
        </Container>
      </div>
    </React.Fragment>
  )
}

export default Home
