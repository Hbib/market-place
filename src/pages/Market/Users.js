import React, { useEffect, useState } from "react"

import PropTypes from "prop-types"
import { Link } from "react-router-dom"
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit"
import { del, get, post, put } from "../../helpers/api_helper"
import { useSelector, useDispatch } from "react-redux"
import {
  Col,
  Container,
  Row,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
} from "reactstrap"

import { AvForm, AvField } from "availity-reactstrap-validation"
import TableCustom from "components/Common/TableCustom"
const Users = ({ location }) => {
  const route = location.pathname
  const { user } = useSelector(state => {
    return state.user
  })

  useEffect(() => {
    document.title = "Liste utilisateurs"
    getList()
  }, [])
  const [modal, setModal] = useState(false)
  const [list, setList] = useState([])
  const [totalSize, settotalSize] = useState(10)
  const [page, setpage] = useState(1)
  const [loading, setLoading] = useState(false)

  async function handleValidSubmit(e, v) {
    try {
      await put("/api/users/update-password/" + user.id, {
        old_password: v.password,
        new_password: v.newpassword,
      })
      setModal(false)
    } catch (e) {}
  }

  function getList() {
    setLoading(true)
    get("/api/users?company=" + user.company).then(res => {
      console.log(res.data)
      setList(
        res.data.map(e => ({
          ref: e.id,
          username: e.username,
          fullName: e.fullName,
          cin: e.cin,
          email: e.email,
          // address:e.address,
          role: e.roles[0],
          action: (
            <div className="d-flex justify-content-center">
              <Link
                to={"/users/edit/" + e.id}
                className="btn btn-sm btn-primary mx-1"
              >
                Editer
              </Link>
              {
                <button
                  className="btn btn-sm btn-warning mx-1"
                  onClick={() => {
                    setModal(true)
                  }}
                >
                  Mot de passe
                </button>
              }
            </div>
          ),
        }))
      )
      setLoading(false)
    })
  }

  function deleteUser(Id) {
    var url = `/api/users/delete/${Id}`

    del(url)
      .then(res => {
        getList()
      })
      .catch(err => console.log(err.response))
  }

  const columns = [
    {
      text: "Code ",
      dataField: "ref",
      sort: true,
    },
    {
      text: "Nom utilisateur",
      dataField: "username",
      sort: true,
    },
    {
      text: "Nom et prenom",
      dataField: "fullName",
      sort: true,
    },

    {
      text: "Email",
      dataField: "email",
      sort: true,
    },
    {
      text: "CIN",
      dataField: "cin",
      sort: true,
    },

    {
      text: "Role",
      dataField: "role",
      sort: true,
    },
    {
      text: "Actions",
      dataField: "action",
      sort: true,
    },
  ]

  useEffect(() => {
    document.onkeydown = function (evt) {
      evt = evt || window.event
      var isEscape = false
      if ("key" in evt) {
        isEscape = evt.key === "Escape" || evt.key === "Esc"
      } else {
        isEscape = evt.keyCode === 27
      }
      if (isEscape) {
        setModal(false)
      }
    }
  }, [])
  return (
    <React.Fragment>
      <div className="page-content">
        <Modal
          contentClassName="card-bg-gray"
          isOpen={modal}
          size="md"
          id="modal-pass-change"
          centered={true}
        >
          <AvForm
            className="form-horizontal"
            onValidSubmit={(e, v) => handleValidSubmit(e, v)}
          >
            <div className="d-flex justify-content-between mb-3">
              <h4 className="mt-2">Modifier le mot de passe</h4>
              <i
                onClick={() => setModal(false)}
                className="btn pt-0 pb-0 bx bx-x-circle fs-2"
              />
            </div>
            <ModalBody>
              <div className="mb-3">
                <AvField
                  name="password"
                  label="Mot de passe"
                  className="form-control"
                  type="password"
                  required
                />
              </div>
              <div className="mb-3">
                <AvField
                  name="newpassword"
                  label="Nouveau mot de passe"
                  className="form-control"
                  type="password"
                  required
                />
              </div>
            </ModalBody>
            <ModalFooter>
              <Button type="submit" color="primary">
                Enregistrer
              </Button>
            </ModalFooter>
          </AvForm>
        </Modal>
        <Container fluid={true}>
          {/* <Link to="/users/new" className="btn btn-primary mb-3">
            Nouveau
          </Link> */}
          <TableCustom loading={loading} cols={columns} items={list} />
        </Container>
      </div>
    </React.Fragment>
  )
}

Users.propTypes = {
  location: PropTypes.object,
}

export default Users
