import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { withRouter, Link } from "react-router-dom"

import { del, get, post, put } from "../../helpers/api_helper"
import { NumericFormat } from "react-number-format"
import moment from "moment"
import Filter from "../../components/Common/Filter"
import PaginationCustom from "components/Common/PaginationCustom"
import { Col, Row, Table } from "reactstrap"
import $ from "jquery"
//redux
import { useSelector, useDispatch } from "react-redux"
import currency from "assets/currency.json"
import TableCustom from "components/Common/TableCustom"
import { toast, ToastContainer } from "react-toastify"

const ProposalDraftTable = props => {
  const [list, setList] = useState([])
  const [loading, setloading] = useState(true)
  const route = window.location.pathname
  const today = new Date()
  const date = today.setDate(today.getDate())
  const token = localStorage.getItem("authToken")
  const [agencies, setAgencies] = useState({})

  const [filter, setfilter] = useState()
  const [page, setPage] = useState(1)
  const [actionloading, setactionloading] = useState(false)
  const [totalSize, settotalSize] = useState(0)
  const [totalEquivalent, settotalEquivalent] = useState(0)
  const [totalAmount, settotalAmount] = useState(0)
  const [selectedCurr, setSelectedCurr] = useState()
  const [pattern, setPattern] = useState(0)
  const { user } = useSelector(state => {
    return state.user
  })

  function handleFilter(form) {
    if (form?.currency && form?.currency != "All") {
      setSelectedCurr(form.currency)
    }
    getList(form)
    setfilter(form)
    setPage(1)
  }

  function onPageChange(p) {
    setPage(p)
    if (filter) {
      getList({ ...filter, offset: p })
    } else {
      getList({ offset: p })
    }
  }

  function confirmOffer(orderId) {
    setactionloading(true)
    put(
      `/api/orders/half-confirm-client/${orderId}`,
      {},
      {
        headers: { Authorization: `Bearer ${token}` },
      }
    )
      .catch(err => {
        if (err.response?.data?.message) {
          toast["error"](err.response.data?.errors)
        } else {
          toast["error"](
            "Veuillez patienter svp, une erreur système s'est produite"
          )
        }
      })
      .finally(() => {
        setactionloading(false)
        getList(filter)
      })
  }

  function rejectOrder(orderId) {
    setactionloading(true)
    put(`/api/orders/reject/${orderId}`, { from: "mp", status: 8 })
      .catch(err => {
        if (err.response?.data?.message) {
          toast["error"](err.response.data?.errors)
        } else {
          toast["error"](
            "Veuillez patienter svp, une erreur système s'est produite"
          )
        }
      })
      .finally(() => {
        setactionloading(false)
        getList(filter)
      })
  }

  function checkExpire(list) {
    if (list.length) {
      const arr = []
      list.forEach(e => {
        if (moment().isBefore(e.expireDate)) {
          arr.push(e)
        }
      })
      setList(arr)
    }
  }

  // let int
  // useEffect(() => {
  //   if (int) clearInterval(int)
  //   int = setInterval(() => {
  //     checkExpire(list)
  //   }, 1000)
  // }, [list])

  function getList(filter) {
    setloading(true)
    let url =
      "/api/orders/status/" +
      user.company +
      "?pageSize=10&status[]=11&status[]=12&status[]=13&status[]=14&status[]=700&status[]=10&status[]=9&status[]=8&status[]=7&status[]=6&status[]=5&status[]=2&startDate=" +
      moment(date).format("yyyy-MM-DD") +
      " 00:00:00"
    if (!filter || !filter.offset) {
      url += "&offset=1"
    }
    if (filter) {
      for (let key in filter) {
        if (filter[key] != "All") {
          url += `&${key}=${filter[key]}`
        }
      }
    }
    get(url)
      .then(res => {
        console.log(res.data)
        settotalSize(res?.data.count)
        settotalEquivalent(res?.data.total_equivalent)
        settotalAmount(res?.data.total_amount)
        setPattern(res?.data.data[0]?.currency.pattern)
        setList(
          res.data.data.map(e => {
            return {
              id_position: e.sessionId,
              id: e.id,
              expireDate: e.expireDate,
              domicialiation: e.bank,
              user: e.user.name,
              produit: e.productType,
              currency: e.currency.isoCode,

              sens: "Achat",
              position: e.sessionId,
              bayers: e.user?.company && e.user?.company?.id,
              purchasename: e.user?.company?.name,
              agencyChoice: e.leaderBoxInfo.favoriteLocation,
              agencyChef: e.agence?.name,
              amount: (
                <NumericFormat
                  displayType="text"
                  thousandSeparator=" "
                  value={Number(e.amount).toFixed(e.currency.pattern)}
                />
              ),
              iduser: e.user?.id,
              user: e.user?.username,
              cible: Number(e.negotationRate).toFixed(3) || "",
              payment:
                e.cashOutType == "CASH_PAYMENT" ? "Cash" : "Virement bancaire",
              brute: Number(e.rate).toFixed(3),
              cvcible: e.negotationRate ? (
                <NumericFormat
                  displayType="text"
                  thousandSeparator=" "
                  value={Number(
                    (e.negotationRate * e.amount) / e.currency?.unit
                  ).toFixed(3)}
                />
              ) : null,
              cvbrut: Number(e.equivalent).toFixed(3),
              region: e.members[0].region,
              zone: e.members[0].zone,
              // contract: e.contrat,
              price: Number(e.originalRate).toFixed(3),
              contrevalue: (
                <NumericFormat
                  displayType="text"
                  thousandSeparator=" "
                  value={Number(
                    (e.amount * e.originalRate) / e.currency?.unit
                  ).toFixed(3)}
                />
              ),
              cheffile: e.leaderBoxInfo && e.leaderBoxInfo.name,
              benefet: "Hbib",
              //synd: e.syndMember.map(d => d.percentage),
              dateorder: moment(e.createdAt).format("DD-MM-yyyy"),
              hour: moment(e.createdAt).format("HH:mm"),

              valuedate: "12:00",
              // catseller: "A+",
              // range: "A",
              typeposition: "Single position",
              // e.syndMember.length == 1 ? "Single position" : "Syndication",
              //agency: e.leaderBoxInfo.ex_op.box_agency,
              // domicialiation: "1587",
              state: e.status.name,
              Type: e.type == "order" ? "Marché" : "Course",
              // contractB: "contrat",
              // contractS: "contrat",
              // cheffile: e.leaderBoxInfo.id,
              statusCode: e.status?.code,
              action: (e.status.code == 2 || e.status.code == 5) && (
                <div className="d-flex justify-content-around">
                  <button
                    className="btn btn-sm btn-success mx-1"
                    onClick={() => confirmOffer(e.id)}
                    disabled={actionloading}
                  >
                    Confirmer
                  </button>
                  <button
                    className="btn btn-sm btn-danger mx-1"
                    onClick={() => rejectOrder(e.id)}
                    disabled={actionloading}
                  >
                    Rejeter
                  </button>
                </div>
              ),
            }
          })
        )
        setloading(false)
      })
      .catch(err => console.log(err))
  }
  useEffect(() => {
    getList()
    $(function () {
      $(".wrapper1").scroll(function () {
        $(".wrapper2").scrollLeft($(".wrapper1").scrollLeft())
      })
      $(".wrapper2").scroll(function () {
        $(".wrapper1").scrollLeft($(".wrapper2").scrollLeft())
      })
    })

    document.title = "Confirmation"
  }, [])

  const EcommerceOrderColumns = [
    {
      dataField: "id_position",
      text: "Id order",
      sort: true,
    },
    {
      dataField: "action",
      text: "Action",
      sort: true,
    },
    {
      dataField: "state",
      text: "Statut",
      sort: true,
    },
    {
      dataField: "cheffile",
      text: "Bureau de change",
      sort: true,
    },
    {
      dataField: "currency",
      text: "Devise",
      sort: true,
    },
    {
      dataField: "price",
      text: "Prix",
      sort: true,
    },
    {
      dataField: "cible",
      text: "Prix négocié",
      sort: true,
    },
    {
      dataField: "amount",
      text: "Valeur",
      sort: true,
    },
    {
      dataField: "agencyChef",
      text: "Agence de livraison",
      sort: true,
    },
    {
      dataField: "payment",
      text: "Mode de livraison",
      sort: true,
    },

    {
      dataField: "cut",
      text: "Coupure",
      sort: true,
    },
    {
      dataField: "billLines",
      text: "Coupure détails",
      sort: true,
    },

    {
      dataField: "contrevalue",
      text: "Contre valeur",
      sort: true,
    },
    {
      dataField: "cvcible",
      text: "Contre valeur négociée",
      sort: true,
    },
    {
      dataField: "net",
      text: "Ticket price",
      sort: true,
    },
    {
      dataField: "cvnet",
      text: "Contre valeur ticket",
      sort: true,
    },

    {
      dataField: "dateorder",
      text: "Date ",
      sort: true,
    },
    {
      dataField: "hour",
      text: "Heure ",
      sort: true,
    },
    {
      dataField: "Type",
      text: "Type",
      sort: true,
    },

    {
      dataField: "purchasename",
      text: "Acheteur",
      sort: true,
    },
    {
      dataField: "user",
      text: "Utilisateur acheteur",
      sort: true,
    },
    {
      dataField: "bayers",
      text: "Id acheteur",
      sort: true,
    },

    {
      dataField: "iduser",
      text: "Id utilisateur acheteur",
      sort: true,
    },

    {
      dataField: "typeposition",
      text: "Type position",
      sort: true,
    },

    {
      dataField: "region",
      text: "Gouvernorat",
      sort: true,
    },
    {
      dataField: "zone",
      text: "Zone",
      sort: true,
    },

    // {
    //   dataField: "contract",
    //   text: "Contrat de vente",
    //   sort: true,
    // },
    {
      dataField: "catseller",
      text: "Catégorie vendeur",
      sort: true,
    },
    {
      dataField: "range",
      text: "Catégorie acheteur",
      sort: true,
    },

    {
      dataField: "ticketnumber",
      text: "Numéro tickt",
      sort: true,
    },
    {
      dataField: "ticket",
      text: "Ticket",
      sort: true,
    },
  ]

  return (
    <React.Fragment>
      <Col md="3" className="d-flex">
        <Filter
          regions
          zone
          bankAgency
          handleSubmit={handleFilter}
          filters={[
            {
              label: "Devise",
              type: "select",
              options: currency.map(e => ({ label: e, value: e })),
              name: "currency",
            },
          ]}
        />
      </Col>
      <TableCustom
        loading={loading}
        cols={EcommerceOrderColumns}
        items={list}
      />{" "}
      <div className="d-flex justify-content-end">
        <PaginationCustom
          page={page}
          onPageChange={onPageChange}
          totalSize={totalSize}
        />
      </div>
      <div className="d-flex ">
        <div className="col-lg-11">
          <h4>
            Total contre valeur :{" "}
            <NumericFormat
              displayType="text"
              thousandSeparator=" "
              value={Number(totalEquivalent).toFixed(3)}
            />
            {" TND"}
          </h4>
          {!!totalAmount && (
            <h4>
              Total valeur :{" "}
              <NumericFormat
                displayType="text"
                thousandSeparator=" "
                value={Number(totalAmount).toFixed(pattern)}
              />
              {` ${selectedCurr}`}
            </h4>
          )}
        </div>
      </div>
      <ToastContainer autoClose={5000} />
    </React.Fragment>
  )
}

ProposalDraftTable.propTypes = {
  onGetOrders: PropTypes.func,
}

export default withRouter(ProposalDraftTable)
