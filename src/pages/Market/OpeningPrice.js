import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import { withRouter, Link } from "react-router-dom"
import { del, get, post, put } from "../../helpers/api_helper"
import { isEmpty } from "lodash"
import BootstrapTable from "react-bootstrap-table-next"
import paginationFactory, {
  PaginationListStandalone,
  PaginationProvider,
} from "react-bootstrap-table2-paginator"
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit"
import Filter from "../../components/Common/Filter"

import { getOrders as onGetOrders } from "store/actions"
import currency from "../../assets/currency.json"
import $ from "jquery"
import logo from "assets/images/atb.jpg"
//redux
import bank from "assets/bank.json"
import { useSelector, useDispatch } from "react-redux"
import {
  Container,
  Row,
  Col,
  Button,
  Card,
  CardBody,
  CardTitle,
  Input,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Media,
  Table,
  CardHeader,
} from "reactstrap"

const OpeningPrice = props => {
  const [list, setList] = useState([])

  function getList() {
    get("/api/open-rate").then(res => {
      console.log(res.data)
      setList(
        res.data.map(e => ({
          id: e.id,
          rate: e.rate,
          status: e.status,
          currency: e.currency.isoCode,
        }))
      )
    })
  }
  useEffect(() => {
    $(function () {
      $(".wrapper1").scroll(function () {
        $(".wrapper2").scrollLeft($(".wrapper1").scrollLeft())
      })
      $(".wrapper2").scroll(function () {
        $(".wrapper1").scrollLeft($(".wrapper2").scrollLeft())
      })
    })
    document.title = "Prix d'ouverture"
    getList()
  }, [])
  function confirmPrice(PriceId) {
    var url = `/api/open-rate/update/${PriceId}`
    const data = {
      status: 1,
    }
    put(url, data)
      .then(res => {
        getList()
      })
      .catch(err => console.log(err.response))
  }
  function rejectPrice(PriceId) {
    var url = `/api/open-rate/update/${PriceId}`

    const data = {
      status: 2,
    }
    put(url, data)
      .then(res => {
        getList()
      })
      .catch(err => console.log(err.response))
  }

  return (
    <React.Fragment>
      <div className="page-content">
        {/* <MetaTags>
            <title>Market | Marketplace</title>
          </MetaTags> */}
        <Container fluid>
          <table className="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Devise</th>
                <th>Prix</th>

                <th>Status</th>
                <th className="w-10">Action</th>
              </tr>
            </thead>
            <tbody>
              {list.map(e => {
                return (
                  <tr key={e}>
                    <td>{e.currency}</td>
                    <td>{e.rate}</td>

                    <td>
                      {e.status == 0
                        ? "En attente"
                        : e.status == 1
                        ? "Validé"
                        : "Rejeté"}
                    </td>
                    <td>
                      <div className="d-flex justify-content-around">
                        <button
                          className="btn btn-sm btn-success mx-1"
                          onClick={() => confirmPrice(e.id)}
                          disabled={
                            e.status == 2 || e.status == 1 ? true : false
                          }
                        >
                          Valider
                        </button>
                        <button
                          className="btn btn-sm btn-danger mx-1"
                          onClick={() => rejectPrice(e.id)}
                          disabled={
                            e.status == 1 || e.status == 2 ? true : false
                          }
                        >
                          Rejeter
                        </button>
                      </div>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </Container>
      </div>
    </React.Fragment>
  )
}

OpeningPrice.propTypes = {
  onGetOrders: PropTypes.func,
}

export default withRouter(OpeningPrice)
