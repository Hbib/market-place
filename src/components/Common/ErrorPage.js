import React from "react"
import { useHistory } from "react-router-dom"
import { Container } from "reactstrap"

const ErrorPage = () => {
  const history = useHistory()
  return (
    <div className="bg-white h-100 w-100" style={{ position: "absolute" }}>
      <Container fluid>
        <div className="p-4">
          <img
            style={{ width: "170px", height: 70 }}
            src={require("../../assets/images/logo.png").default}
          />
        </div>
        <center>
          <img
            style={{ width: "60vh" }}
            src={require("../../assets/images/error.jpg").default}
          />

          <h2 className="my-5">
            Humm... une erreur s'est produite! Merci de contacter l'équipe
            support Insight Plus
          </h2>

          <button
            className="btn btn-primary"
            onClick={() => history.push("/cashbox")}
          >
            Retour à la plateforme
          </button>
        </center>
      </Container>
    </div>
  )
}

export default ErrorPage
