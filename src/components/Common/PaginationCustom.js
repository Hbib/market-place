import React, { useState, useRef, useEffect } from "react"
import PropTypes from "prop-types"
import { Pagination, PaginationLink, PaginationItem } from "reactstrap"
const ReactUltimatePagination = require("react-ultimate-pagination")

const PaginationCustom = ({
  totalSize = 0,
  onPageChange,
  sizePerPage = 10,
  page = 1,
}) => {
  const pagination = ReactUltimatePagination.createUltimatePagination({
    itemTypeToComponent: itemTypeToComponent,
    WrapperComponent: Wrapper,
  })
  return (
    <>
      {totalSize != 0 &&
        pagination({
          currentPage: page,
          totalPages: Math.ceil(totalSize / sizePerPage),
          onChange: onPageChange,
        })}
    </>
  )
}

function Page(props) {
  return (
    <PaginationItem
      active={props.isActive}
      onClick={props.onClick}
      disabled={props.disabled}
    >
      <PaginationLink>{props.value}</PaginationLink>
    </PaginationItem>
  )
}

function Ellipsis(props) {
  return (
    <PaginationItem onClick={props.onClick} disabled={props.disabled}>
      <PaginationLink>...</PaginationLink>
    </PaginationItem>
  )
}

function FirstPageLink(props) {
  return (
    <PaginationItem onClick={props.onClick} disabled={props.disabled}>
      <PaginationLink first />
    </PaginationItem>
  )
}

function PreviousPageLink(props) {
  return (
    <PaginationItem onClick={props.onClick} disabled={props.disabled}>
      <PaginationLink previous />
    </PaginationItem>
  )
}

function NextPageLink(props) {
  return (
    <PaginationItem onClick={props.onClick} disabled={props.disabled}>
      <PaginationLink next />
    </PaginationItem>
  )
}

function LastPageLink(props) {
  return (
    <PaginationItem onClick={props.onClick} disabled={props.disabled}>
      <PaginationLink last />
    </PaginationItem>
  )
}

function Wrapper(props) {
  return <Pagination>{props.children}</Pagination>
}

var itemTypeToComponent = {
  PAGE: Page,
  ELLIPSIS: Ellipsis,
  FIRST_PAGE_LINK: FirstPageLink,
  PREVIOUS_PAGE_LINK: PreviousPageLink,
  NEXT_PAGE_LINK: NextPageLink,
  LAST_PAGE_LINK: LastPageLink,
}

PaginationCustom.propTypes = {
  onPageChange: PropTypes.func,
  totalSize: PropTypes.number,
}

export default PaginationCustom
