import {
  Alert,
  Button,
  ButtonGroup,
  Card,
  CardContent,
  createTheme,
  FormLabel,
  TextField,
  ThemeProvider,
  Typography,
} from "@mui/material"

import { styled } from "@mui/material/styles"
import Tooltip, { tooltipClasses } from "@mui/material/Tooltip"
import Grid2 from "@mui/material/Unstable_Grid2"
import React, { useState } from "react"
import facebook from "../../assets/images/facebook.svg"
import whatsapp from "../../assets/images/whatsapp.svg"
import linkedin from "../../assets/images/linkedin-circled.svg"
import "./login2.scss"
import { AvField, AvForm } from "availity-reactstrap-validation"
import { get, post } from "helpers/api_helper"
// actions
import {
  loginUser,
  apiError,
  socialLogin,
  saveUser,
  saveBox,
  saveCurr,
} from "../../store/actions"
import { useSelector, useDispatch } from "react-redux"

import { useHistory } from "react-router-dom"

const MyLogin2 = () => {
  const history = useHistory()
  const dispatch = useDispatch()
  const [error, setError] = useState("_")
  const [loading, setLoading] = useState(false)

  const theme = createTheme({
    status: {
      danger: "#e53e3e",
    },
    palette: {
      primary: {
        main: "#2f609d",
        darker: "#053e85",
      },
      info: {
        main: "#ffffff",
        contrastText: "#2f609d",
      },
      neutral: {
        main: "white",
        contrastText: "#fff",
      },
    },
  })

  const handleValidSubmit = (event, values) => {
    setLoading(true)
    post("/api/login_check", values, {
      withCredentials: true,
    })
      .then(res => {
        dispatch(saveUser(res.data))
        localStorage.setItem("authToken", res.data.token)
        localStorage.setItem("stayOnline", JSON.stringify(true))
        console.log(res.data)
        history.push("/home")
        setLoading(false)
      })
      .catch(err => {
        if (err.response.status == 401) {
          setError("Nom d'utilisateur ou mot de passe incorrecte")
        } else {
          setError("Erreur serveur")
        }
        setLoading(false)
      })

    localStorage.setItem("userEmail", values.email)
  }

  const HtmlTooltip = styled(({ className, ...props }) => (
    <Tooltip {...props} arrow classes={{ popper: className }} />
  ))(({ theme }) => ({
    [`& .${tooltipClasses.arrow}`]: {
      color: "#f5f5f9",
    },
    [`& .${tooltipClasses.tooltip}`]: {
      backgroundColor: "#f5f5f9",
      color: "rgba(0, 0, 0, 0.87)",
      maxWidth: 220,
      fontSize: theme.typography.pxToRem(12),
      border: "1px solid #dadde9",
    },
  }))

  return (
    <ThemeProvider theme={theme}>
      <div className="login-background">
        <div className="login-page-header p-5">
          <div className="d-flex justify-content-end">
            <ButtonGroup
              sx={{ height: 32, borderTop: "3px solid #ff7600" }}
              color="info"
              variant="contained"
            >
              <HtmlTooltip
                title={
                  <React.Fragment>
                    <Typography color={"grey"} fontWeight={600}>
                      Coming soon !
                    </Typography>
                  </React.Fragment>
                }
              >
                <Button
                  sx={{
                    textTransform: "unset",
                    fontFamily: "Lato, sans-serif",
                  }}
                >
                  <b>DirectFX rates ©</b>
                </Button>
              </HtmlTooltip>
              <HtmlTooltip
                title={
                  <React.Fragment>
                    <Typography color={"grey"} fontWeight={600}>
                      Coming soon !
                    </Typography>
                  </React.Fragment>
                }
              >
                <Button
                  sx={{
                    textTransform: "unset",
                    fontFamily: "Lato, sans-serif",
                  }}
                >
                  <b>Maps</b>
                </Button>
              </HtmlTooltip>
            </ButtonGroup>
          </div>
        </div>
        <div className="login-page-content">
          <Grid2
            justifyContent={"space-around"}
            alignItems="center"
            container
            spacing={0}
          >
            <Grid2 lg={3}>
              <Card className="login-card" style={{ borderRadius: 10 }}>
                <CardContent>
                  <Typography
                    fontWeight={"100"}
                    sx={{
                      mb: 1,
                      fontSize: "4.5vh",
                      fontFamily: "Lato, sans-serif",
                    }}
                  >
                    Hello!
                  </Typography>
                  <Typography
                    sx={{
                      fontSize: "2.5vh",
                      mb: "4vh",
                      fontFamily: "Lato, sans-serif",
                    }}
                    variant="body2"
                    color="text.secondary"
                  >
                    you're a click away
                  </Typography>

                  {/* {error ? (
                        <Alert severity="error" variant="filled">
                          {error}
                        </Alert>
                      ) : null} */}
                  <AvForm onValidSubmit={handleValidSubmit}>
                    <FormLabel
                      sx={{
                        fontFamily: "Lato, sans-serif",
                      }}
                    >
                      Username
                    </FormLabel>
                    <AvField
                      name="username"
                      className="login-input"
                      style={{ width: "100%" }}
                      variant="standard"
                    />
                    <small
                      className={error == "_" ? "text-white" : "text-danger"}
                    >
                      {error}
                    </small>

                    <br />

                    <FormLabel
                      sx={{
                        fontFamily: "Lato, sans-serif",
                      }}
                    >
                      Password
                    </FormLabel>
                    <AvField
                      name="password"
                      className="login-input"
                      style={{ width: "100%" }}
                      type="password"
                      required
                    />
                    <small
                      className={error == "_" ? "text-white" : "text-danger"}
                    >
                      {error}
                    </small>

                    <Button
                      sx={{ mt: 1.5, width: "100%" }}
                      variant="contained"
                      type="submit"
                    >
                      {loading ? (
                        <div
                          className="spinner-border text-light"
                          style={{ width: "1.5rem", height: "1.5rem" }}
                          role="status"
                        >
                          <span className="sr-only">Loading...</span>
                        </div>
                      ) : (
                        <span>Login</span>
                      )}
                    </Button>
                  </AvForm>

                  {/* <Typography
                    sx={{
                      fontSize: "1em",
                      fontFamily: "Lato, sans-serif",
                    }}
                    variant="body2"
                    color="text.secondary"
                  >
                    This product is protected by Insight Plus ©{" "}
                    {new Date().getFullYear()}.
                  </Typography> */}
                </CardContent>
              </Card>
            </Grid2>
            <Grid2 lg={7} className="info">
              <div
                className="d-flex justify-content-center"
                style={{ alignItems: "center" }}
              >
                <Typography
                  color={"white"}
                  sx={{
                    fontSize: "8.75vh",
                    fontFamily: "Lato, sans-serif",
                  }}
                >
                  We work on interconnecting the global currency exchange
                  ecosystem.
                </Typography>
              </div>
            </Grid2>
          </Grid2>
        </div>
        <div
          style={{ alignItems: "end" }}
          className="d-flex justify-content-between p-5 social-container"
        >
          <img
            style={{ width: "170px", height: 70 }}
            className="mb-1"
            src={require("../../assets/images/logo-white.png").default}
          />
          <div className="social-right">
            <div className="d-flex" style={{ alignItems: "center" }}>
              <Typography
                sx={{
                  fontSize: "2.4vh",
                  mt: 0.5,
                  ml: 1.5,
                  fontFamily: "Lato, sans-serif",
                  color: "white",
                }}
                className="text-nowrap"
                variant="body2"
                color="text.secondary"
              >
                hello@insights.tn
              </Typography>
            </div>
            <a
              href="https://www.linkedin.com/company/insightplustunisia/"
              rel="noreferrer"
              target="_blank"
            >
              <img className="social" src={linkedin} />
            </a>
            <a
              href="https://api.whatsapp.com/send?phone=%2B21699240153&data=AWDzXvM9KUHJ0ZZGiCJPY9K-kV9L-A7Z9-pKEjh_K34fG3vRHw-5V-Sd2SFgi-b-YRHUukskTn-EvW4KFXcGbmGrzq4qGPcA6CqEHHT_ZukAm66VSvAl0eqZdkLWO7rFmwYw0pdM7jZCYklCMqADPaJIO-0z0vjE5zfywvvP29tyblvv-fs0TDS_FN-dj_02re2_VTc94123o0Jcs5Zb-79SwCSlTxTyprhHRuYtWWHadss0fnLcdDFNoh7DM3k-T7JniwKjLcUlhyP0sX8v7Q-ZsSs3-VzMtuFu0HJ46-yAPDcrSj8&source=FB_Page&app=facebook&entry_point=page_cta&fbclid=IwAR0dXGsdb_h7mUCpaOWk1d98ZLnUVzbSjViZaeos5Pl7VF-1kgoKZMLDqoE"
              rel="noreferrer"
              target="_blank"
            >
              <img className="social" src={whatsapp} />
            </a>
            <a
              href="https://www.facebook.com/InsightplusFX"
              rel="noreferrer"
              target="_blank"
            >
              <img className="social" src={facebook} />
            </a>
          </div>
        </div>
      </div>
    </ThemeProvider>
  )
}

export default MyLogin2
