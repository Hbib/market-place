import React, { useEffect, useState } from "react"
import { AvForm, AvField } from "availity-reactstrap-validation"

import PropTypes from "prop-types"
import SweetAlert from "react-bootstrap-sweetalert"
import { del, get, post, put } from "../../helpers/api_helper"
import moment from "moment"
import { Card, Col, Container, Row, Label } from "reactstrap"
import currency from "../../assets/currency"
import { useSelector, useDispatch } from "react-redux"
import { useHistory } from "react-router-dom"
const newRace = ({ location }) => {
  let history = useHistory()
  const route = location.pathname
  const [success_msg, setsuccess_msg] = useState(false)
  const [depassement, setdepassement] = useState(0)
  const { user } = useSelector(state => {
    return state.user
  })
  const token = localStorage.getItem("authToken")
  function handleSubmit(e, v) {
    e.preventDefault()
    v.expireDate = new Date(v.expireDate)
    v.user = user.id
    console.log(v)
    post("/api/course/add", v, {
      headers: { Authorization: `Bearer ${token}` },
    })
      .then(res => {
        console.log(res)
        history.push("/list-race")
      })
      .catch(err => console.log(err.response))
  }
  useEffect(() => {
    document.title = "Nouvelle course"
  }, [])
  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid={true}>
          <AvForm onValidSubmit={handleSubmit}>
            <Row>
              <Col lg={4}>
                <div className="mb-3">
                  <label>Devise </label>
                  <AvField
                    type="select"
                    name="currency"
                    value="EUR"
                    className="form-select"
                    required
                  >
                    {currency.map(e =>
                      e != "TND" ? (
                        <option value={e} key={e}>
                          {e}
                        </option>
                      ) : null
                    )}
                  </AvField>
                </div>{" "}
              </Col>
            </Row>
            <Row>
              <Col lg={4}>
                <div className="mb-3">
                  <label>Valeur</label>
                  <AvField
                    required
                    name="amount"
                    type="number"
                    className="form-control"
                  />
                </div>{" "}
              </Col>
            </Row>
            <Row>
              <Col lg={4}>
                <div className="mb-3">
                  <label>Prix de vente</label>
                  <AvField
                    required
                    name="rate"
                    type="number"
                    className="form-control"
                  />
                </div>{" "}
              </Col>
            </Row>
            {/* <Row>
              <Col lg={4}>
                <div>
                  <Label>Date de maturité</Label>
                  <div className="mb-3">
                    <AvField
                      className="form-control"
                      type="date"
                      id="example-date-input"
                      name="expireDate"
                    />
                  </div>
                </div>
              </Col>
            </Row> */}
            {/* <Row>
            <Col lg={4}>
              <div>
                <Label htmlFor="example-datetime-local-input">
                  Dépassement
                </Label>
                <div className="mb-3">
                  <select
                    className="form-select"
                    onChange={e => {
                      setdepassement(e.target.value)
                    }}
                  >
                    <option default value={0}>
                      Non
                    </option>
                    <option value={1}>Oui</option>
                  </select>
                </div>
              </div>
            </Col>
            {depassement == 1 ? (
              <Col lg="4">
                <Label htmlFor="example-datetime-local-input">Montant</Label>
                <input className="form-control" />
              </Col>
            ) : null}
          </Row> */}
            <div className="d-flex mt-2">
              <button
                className="btn btn-primary w-md"
                style={{ marginTop: "2%" }}
              >
                Valider
              </button>
            </div>
          </AvForm>
          {success_msg && (
            <SweetAlert
              title="Validé!"
              success
              confirmBtnBsStyle="success"
              cancelBtnBsStyle="danger"
              onConfirm={() => {
                setsuccess_msg(false)
              }}
              onCancel={() => {
                setsuccess_msg(false)
              }}
            >
              Race créé avec succée!
            </SweetAlert>
          )}
        </Container>
      </div>
    </React.Fragment>
  )
}
newRace.propTypes = {
  location: PropTypes.object,
}
export default newRace
